<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 7/2/18
 * Time: 9:51 AM
 */

namespace App;
use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class ProjectModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'master_project';


      public function deleteData(Request $request, $id){
        $subtask=DB::table('master_sub_task')
        ->join('master_task','master_task.id','=','master_sub_task.id_task')
        ->where('master_task.id_project', '=',$id)
        ->delete();    

        $task=DB::table('master_task')
        ->where('id_project',$id)
        ->delete();

        $pm=DB::table('master_project')
            ->where('id', '=',$id)
            ->delete();
    }

      public function openproject(Request $request, $id){
          $real_lm = $this->find($id);
       	  $real_lm->close= 0;
          $real_lm->save();
    }
}