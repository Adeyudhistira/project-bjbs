<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReffTeam extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'ref_team';
}