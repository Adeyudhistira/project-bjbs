<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->is('refTask')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        if($request->is('refSubtask')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        if($request->is('client')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        if($request->is('company')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        if($request->is('paralel')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        if($request->is('performance')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        if($request->is('progressstatus')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        if($request->is('projecttype')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        if($request->is('quicklevel')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        if($request->is('status')){
            if(!Auth::user()->hasRole('Admin')){
                abort('403','Akses di Tolak');
            }else{
                return $next($request);
            }
        }

        return $next($request);
    }
}
