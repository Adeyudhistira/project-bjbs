<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\SubtaskModel;
use App\TaskModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
use View;

class SubtaskController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {

        $param['task'] = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY t.id) AS nomor_urut, t.id,t.task_name,t.created_at,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,p.project_name,t.id_task,t.id_project
            FROM master_task t join master_project p on p.id=t.id_project join users u on u.id=t.pic_id join ref_progress_status ps on ps.id=t.progress_id where t.status_id=1");

        if ($request->ajax()) {
            $view = view('task.index', $param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'task.index', $param);
    }


    public function table($id, Request $request)
    {
        /*
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY t.id) AS nomor_urut, t.id,t.task_name,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,p.project_name
            FROM master_task t join master_project p on p.id=t.id_project join users u on u.id=t.pic_id join ref_progress_status ps on ps.id=t.progress_id where t.status_id=1");


        $data = Datatables::of($query)->addColumn('action', function ($query){
            return "
            <i class='fa fa-pencil' style='color:blue;' title='Edit'  onclick='editshow(".$query->id.")'></i>
            <i style='color:red;' title='Hapus' onclick='hapus($query->id,\"task/delete\");' class='fa fa-trash'></i>";
            
        })->make(true);

        return $data;
        */
        $param['task'] = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY t.id) AS nomor_urut, t.id,t.task_name,t.created_at,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,p.project_name,t.id_task,t.id_project,p.is_parallel
            FROM master_task t join master_project p on p.id=t.id_project left join users u on u.id=t.pic_id left join ref_progress_status ps on ps.id=t.progress_id where t.status_id in (1,2) and t.id=:id", ['id' => $id]);

        $param['subtask'] = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY t.id) AS nomor_urut, t.id,t.sub_task_name,t.created_at,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,t.notes
            FROM master_sub_task t  join users u on u.id=t.pic_id join ref_progress_status ps on ps.id=t.progress_id where t.status_id in (1,2) and t.id_task=:id order by t.id", ['id' => $id]);

        if ($request->ajax()) {
            $view = view('subtask.index', $param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'subtask.index', $param);
    }

    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(), ['id_sub_task' => 'required', 'start_date' => 'required', 'target_date' => 'required', 'pic_id' => 'required',  'tasklevel_id' => 'required']);
        if ($validators->fails()) {
            return $this->sendResponse('0', 'Input Sub Task gagal', $validators);
        }
        $typeproject = DB::table('master_task')
                ->select('master_project.project_type_id')
                ->join('master_project','master_project.id','=','master_task.id_project')
                ->where('master_task.id', '=', $request->input('id_task'))
                ->first();
        $vtask = DB::table('master_sub_task')
                ->select('master_task.task_name')
                ->join('master_task','master_task.id','=','master_sub_task.id_task')
                ->where('master_sub_task.id_task', '=', $request->input('id_task'))
                ->where('master_sub_task.id_sub_task', '=', $request->input('id_sub_task'))
                ->whereNotIn('master_sub_task.status_id',[3])
                ->first();       
  
  if(empty($vtask)){

  if($request->hasFile('file')){
        $destination_path = public_path('support_docs_task');
        $files = $request->file('file');
        $filename = str_replace(' ', '', $files->getClientOriginalName());
        $upload_success = $files->move($destination_path, $filename);

        
        $task = new SubtaskModel();
        $task->id_task = $request->input('id_task');
        $task->id_sub_task = $request->input('id_sub_task');
        $task->sub_task_name = $request->input('sub_task_name');
        $task->start_date = $request->input('start_date');
        $task->target_date = $request->input('target_date');
        $task->note = $request->input('note');
        $task->subtask_doc = $filename;
        $task->status_id = 2;
        $task->pic_id = $request->input('pic_id');
        $task->task_type_id = $typeproject->project_type_id;
        $task->sub_task_level_id = $request->input('tasklevel_id');
        $task->quick_level_id = 1;
        $task->progress_id = 1;
        $task->rate_level_id = 0;
        $task->company_id = 1;
        $task->progress_pct = 0;
        $task->duration = 0;
        $task->save();
        }else{

        $task = new SubtaskModel();
        $task->id_task = $request->input('id_task');
        $task->id_sub_task = $request->input('id_sub_task');
        $task->sub_task_name = $request->input('sub_task_name');
        $task->start_date = $request->input('start_date');
        $task->target_date = $request->input('target_date');
        $task->note = $request->input('note');
        $task->status_id = 1;
        $task->pic_id = $request->input('pic_id');
        $task->task_type_id = $typeproject->project_type_id;
        $task->sub_task_level_id = $request->input('tasklevel_id');
        $task->quick_level_id = 1;
        $task->progress_id = 2;
        $task->rate_level_id = 0;
        $task->company_id = 1;
        $task->progress_pct = 0;
        $task->duration = 0;
        $task->save();
        
        } 

        return $this->sendResponse('1', 'Input Sub Task berhasil', $task);          
  }else{


        return $this->sendResponse('2',$vtask->task_name , $vtask);
  }     

        //hitung progress pct task

        /*$cekprog = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->whereIn('status_id', [1, 2])
            ->where('id_task', '=', $request->input('id_task'))
            ->first();

        $cekprog1 = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->where('progress_id', '=', 5)
            ->whereIn('status_id', [1, 2])
            ->whereNotNull('end_date')
            ->where('id_task', '=', $request->input('id_task'))
            ->first();


        if ($cekprog->nilai > 0) {
            $a = $cekprog->nilai;
            $b = 100;
            $c = $b / $a;
            $d = $c * $cekprog1->nilai;

            $aa = DB::table('master_task')
                ->where('id', $request->input('id_task'))
                ->update(['progress_pct' => $d]);

        }


        //hitung progress pct projek

        $idproject = DB::table('master_task')
            ->select('id_project')
            ->where('id', '=', $request->input('id_task'))
            ->first();


        $prog = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task', 'master_task.id', '=', 'master_sub_task.id_task')
            ->whereIn('master_sub_task.status_id', [1, 2])
            ->where('master_task.id_project', '=', $idproject->id_project)
            ->first();

        $prog1 = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task', 'master_task.id', '=', 'master_sub_task.id_task')
            ->where('master_sub_task.progress_id', '=', 5)
            ->whereIn('master_sub_task.status_id', [1, 2])
            ->whereNotNull('master_sub_task.end_date')
            ->where('master_task.id_project', '=', $idproject->id_project)
            ->first();


        if ($prog->nilai > 0) {
            $a1 = $prog->nilai;
            $b1 = 100;
            $c1 = $b1 / $a1;
            $d1 = $c1 * $prog1->nilai;

            $aa = DB::table('master_project')
                ->where('id', $idproject->id_project)
                ->update(['progress_pct' => $d1]);

        }*/


    }


    public function delete(Request $request, $id)
    {
        try {
            $model = new SubtaskModel();
            $model->deleteData($request, $id);
            
            $subtask = SubtaskModel::find($id);
        if($subtask->end_date < $subtask->target_date){
            $quick_level = 5;
        }else{
            $date_diff = $this->weekday_diff($subtask->target_date,$subtask->end_date)-1;
            switch ($date_diff){
                case 0:
                    $quick_level = 4;
                    break;
                case 1:
                    $quick_level = 3;
                    break;
                case 2:
                    $quick_level = 2;
                    break;
                default:
                    $quick_level = 1;
                    break;
            }
        }

        $count_all_subtask = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->whereIn('status_id',[1,2])
            ->where('id_task', '=',$subtask->id_task)
            ->first();

        $count_done_subtask = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->where('progress_id', '=',5)
            ->whereIn('status_id',[1,2])
            ->whereNotNull('end_date')
            ->where('id_task', '=',$subtask->id_task)
            ->first();

        if($count_all_subtask->nilai > 0){
            $default = 100;
            $progress_pct = ($default/$count_all_subtask->nilai) * $count_done_subtask->nilai;

            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_pct' => $progress_pct]);
        }

        $project_id = DB::table('master_task')
            ->select('id_project')
            ->where('id', '=',$subtask->id_task)
            ->first();

        $all = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task','master_task.id','=','master_sub_task.id_task')
            ->whereIn('master_sub_task.status_id',[1,2])
            ->where('master_task.id_project', '=',$project_id->id_project)
            ->first();

        $done = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task','master_task.id','=','master_sub_task.id_task')
            ->where('master_sub_task.progress_id', '=',5)
            ->whereIn('master_sub_task.status_id',[1,2])
            ->whereNotNull('master_sub_task.end_date')
            ->where('master_task.id_project', '=',$project_id->id_project)
            ->first();

        if($all->nilai>0){
            $default = 100;
            $progress_pct = ($default/$all->nilai) * $done->nilai;
            DB::table('master_project')
                ->where('id', $project_id->id_project)
                ->update(['progress_pct' => $progress_pct]);

        }

        if($count_all_subtask->nilai == $count_done_subtask->nilai){
            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_id' => 5]);
        }else{
            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_id' => 2]);
        }
       
    
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            } else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        } catch (CustomException $e) {
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }

    }

    public function findRealById($id)
    {
        $data = \DB::select("SELECT  id, sub_task_name,url_doc, notes,progress_id,id_task,target_date
            FROM master_sub_task WHERE id =" . $id);
        return json_encode($data);
    }

    public function update(Request $request)
    {

        $validators = \Validator::make($request->all(), ['sub_task_name' => 'required', 'progressstatus_id' => 'required', 'url_doc' => 'max:10000']);
        if ($validators->fails()) {
            return $this->sendResponse(0, 'Update Gagal', $validators);
        }

        $destination_path = public_path('uploadsubtask');
        $files = $request->file('url_doc');
        $filename = $files->getClientOriginalName();
        $upload_success = $files->move($destination_path, $filename);

        if ($request->input('duration') < 0) {
            $diquick = 5;
        } elseif ($request->input('duration') > 3) {
            $diquick = 1;
        } else {

            $q = DB::table('ref_quick_level')
                ->where('selisih', '=', $request->input('duration'))
                ->first();

            $diquick = $q->id;
        }


        if ($request->input('progressstatus_id') == 5) {

            $task = SubtaskModel::find($request->input('id'));
            $task->end_date = $request->input('end_date');
            $task->duration = $request->input('duration');
            $task->progress_id = $request->input('progressstatus_id');
            $task->status_id = 1;
            $task->progress_pct = 100;
            $task->is_approve=false;
            $task->quick_level_id = $diquick;
            $task->notes = $request->input('notes');
            $task->url_doc = $filename;
            $task->save();


            $data = \DB::select("SELECT  id_task
            FROM master_sub_task WHERE id =" . $request->input('id') ."limit 1");

            $seq=\DB::select("SELECT  id,progress_id
            FROM master_sub_task WHERE id >" . $request->input('id') . "and id_task=".$data[0]->id_task."limit 1");    

            if(!empty($seq)){
            $task = SubtaskModel::find($seq[0]->id);
            $task->start_date = $request->input('end_date');
            $task->save();
            }


        } else {

            $task = SubtaskModel::find($request->input('id'));
            $task->progress_id = $request->input('progressstatus_id');
            $task->status_id = 1;
            $task->notes = $request->input('notes');
            $task->end_date = null;
            $task->progress_pct = 0;
            $task->is_approve =false;
            $task->duration = null;
            $task->quick_level_id = 1;
            $task->url_doc = $filename;
            $task->save();

             $id_subtask = $request->input('id');
        $subtask = SubtaskModel::find($id_subtask);
        if($subtask->end_date < $subtask->target_date){
            $quick_level = 5;
        }else{
            $date_diff = $this->weekday_diff($subtask->target_date,$subtask->end_date)-1;
            switch ($date_diff){
                case 0:
                    $quick_level = 4;
                    break;
                case 1:
                    $quick_level = 3;
                    break;
                case 2:
                    $quick_level = 2;
                    break;
                default:
                    $quick_level = 1;
                    break;
            }
        }

        $count_all_subtask = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->whereIn('status_id',[1,2])
            ->where('id_task', '=',$subtask->id_task)
            ->first();

        $count_done_subtask = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->where('progress_id', '=',5)
            ->whereIn('status_id',[1,2])
            ->whereNotNull('end_date')
            ->where('id_task', '=',$subtask->id_task)
            ->first();

        if($count_all_subtask->nilai > 0){
            $default = 100;
            $progress_pct = ($default/$count_all_subtask->nilai) * $count_done_subtask->nilai;

            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_pct' => $progress_pct]);
        }

        $project_id = DB::table('master_task')
            ->select('id_project')
            ->where('id', '=',$subtask->id_task)
            ->first();

        $all = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task','master_task.id','=','master_sub_task.id_task')
            ->whereIn('master_sub_task.status_id',[1,2])
            ->where('master_task.id_project', '=',$project_id->id_project)
            ->first();

        $done = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task','master_task.id','=','master_sub_task.id_task')
            ->where('master_sub_task.progress_id', '=',5)
            ->whereIn('master_sub_task.status_id',[1,2])
            ->whereNotNull('master_sub_task.end_date')
            ->where('master_task.id_project', '=',$project_id->id_project)
            ->first();

        if($all->nilai>0){
            $default = 100;
            $progress_pct = ($default/$all->nilai) * $done->nilai;
            DB::table('master_project')
                ->where('id', $project_id->id_project)
                ->update(['progress_pct' => $progress_pct]);

        }

        if($count_all_subtask->nilai == $count_done_subtask->nilai){
            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_id' => 5]);
        }else{
            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_id' => 2]);
        }


        }

        $data = \DB::select("SELECT  id_task
            FROM master_sub_task WHERE id =" . $request->input('id') ."and status_id in (1,2) and progress_id=5 limit 1");
        if(!empty($data)){
        $seq=\DB::select("SELECT  id,progress_id,is_approve
            FROM master_sub_task WHERE id >" . $request->input('id') . "and status_id in (1,2)  and id_task=".$data[0]->id_task." order by id asc limit 1");
            DB::table('master_task')
                ->where('id', $data[0]->id_task)
                ->update(['progress_id' => 2,'status_id' => 1]);

        $updatestatusproject=\DB::select("SELECT id_project
            FROM master_task WHERE id=".$data[0]->id_task." order by id asc limit 1");
          if($updatestatusproject){
            DB::table('master_project')
                ->where('id', $updatestatusproject[0]->id_project)
                ->update(['status_id' => 1]);

          }          

        }
        if(!empty($seq)){
           DB::table('master_sub_task')
                ->where('id', $seq[0]->id)
                ->update(['progress_id' => 2,'status_id' => 1]);
            }     



        return $this->sendResponse(1, 'Berhasil Diupdate', $task);
    }

    public function get_file($filename)
    {
        $file_path = public_path('uploadsubtask') . "/" . $filename;
        return Response::download($file_path);
        // return json_encode($filename);
        //$path = storage_path($filename);

        //$headers = array('Content-Type' => File::mimeType($file_path));
        //$headers = array('Content-Type'=>'application/png');
        //return Response::download($file_path, 'logo.png',$headers);

        //return $img->response('jpg');

        //return response()->download($file_path, 'filename.png', $headers);

    }

     public function findseq($id)
    {
        $data = \DB::select("SELECT  id_task
            FROM master_sub_task WHERE id =" . $id ."and status_id in (1,2) limit 1");

        $seq=\DB::select("SELECT  id,progress_id,is_approve
            FROM master_sub_task WHERE id <" . $id . "and status_id in (1,2) and id_task=".$data[0]->id_task." order by id desc limit 1");    

        return json_encode($seq);
    }

}