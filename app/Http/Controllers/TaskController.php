<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\TaskModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
use View;
class TaskController extends Controller
{

     public function __construct()
    {
        $this->middleware(['auth','checkrole']);
    }


   public function index(Request $request, $id = null)
    {
        $team_id = \Auth::user()->getAttributes()['team_id'];
        

        if(is_null($id)){
        
        if(Auth::user()->hasRole('Admin')){
            $query = \DB::select("SELECT t.id_project,p.project_name, t.id,t.task_name,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,t.id_task,p.is_parallel
            FROM master_task t join master_project p on p.id=t.id_project left join users u on u.id=t.pic_id left join ref_progress_status ps on ps.id=t.progress_id where t.status_id in(1,2) ORDER BY t.id_project,t.id asc");

        
        }else{
           $query = \DB::select("SELECT t.id_project,p.project_name, t.id,t.task_name,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,t.id_task,p.is_parallel
            FROM master_task t join master_project p on p.id=t.id_project left join users u on u.id=t.pic_id left join ref_progress_status ps on ps.id=t.progress_id where t.status_id in(1,2) and p.team_id=:id  ORDER BY t.id_project,t.id asc",['id'=> $team_id ]);

             
        }    

        

        }
/*
        else{
            $query = \DB::select("SELECT t.id_project,p.project_name, t.id,t.task_name,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,t.id_task
            FROM master_task t join master_project p on p.id=t.id_project join users u on u.id=t.pic_id join ref_progress_status ps on ps.id=t.progress_id where t.status_id = 1 and t.id_project = :id_project
GROUP BY t.id_project, t.id,t.task_name,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,p.project_name,t.id_task                    
    ORDER BY t.id_project,t.id asc",['id_project' => $id]);
        }
*/

        $param['task'] = $query;

        if ($request->ajax()) {
            $view = view('task.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'task.index',$param);
    }


    public function table(Request $request){


             $query = \DB::select("
SELECT t.id_project, t.id,t.task_name,t.created_at,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,p.project_name,t.id_task
FROM master_task t join master_project p on p.id=t.id_project join users u on u.id=t.pic_id join ref_progress_status ps on ps.id=t.progress_id 
where t.status_id=1
group by t.id,t.task_name,t.created_at,t.progress_pct,ps.definition,t.progress_id,u.full_name,t.url_doc,p.project_name  order by to_char(t.created_at,'yyy-mm-dd') desc");

                  
        $data = Datatables::of($query)->addColumn('no', function ($query){
           return"<i class='icon-note'></i>";

        })->addColumn('taskname', function ($query){
           return"<a href='#' class='text-bold-600'>".$query->task_name."</a><p class='text-muted'>aaa</p>";

        })->addColumn('projek', function ($query){
           return"<h6 class='mb-0'>
                              <span class='text-bold-600'>".$query->project_name."</span> on
                              <em>".date('d-M-Y', strtotime($query->created_at))."</em>
                            </h6>";

        })->addColumn('progress', function ($query){
            switch ($query->progress_id){
                case 1:
                    return '<span class="badge badge-default badge-danger">'.$query->definition.'</span>';
                    break;
                case 2:
                    return '<span class="badge badge-default badge-warning">'.$query->definition.'</span>';
                    break;
                case 3:
                    return '<span class="badge badge-default badge-success">'.$query->definition.'</span>';
                    break;
            }
        })->addColumn('progress_pct', function ($query){
           return"<div class='progress progress-sm'>
                              <div aria-valuemin='".$query->progress_pct."' aria-valuemax='100' class='progress-bar bg-gradient-x-success'
                              role='progressbar' title='".$query->progress_pct."' style='width:".$query->progress_pct."' aria-valuenow='".$query->progress_pct."'></div>
                            </div>  ";

        })->addColumn('action', function ($query){
            return "
            <i class='fa fa-pencil' style='color:blue;' title='Edit'  onclick='editshow(".$query->id.")'></i>
            <i style='color:red;' title='Hapus' onclick='hapus($query->id,\"task/delete\");' class='fa fa-trash'></i>";
            
        })->rawColumns(['no','taskname','projek','progress','progress_pct','action'])->make(true);

        return $data;
    }

    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(),['project_id'=>'required','id_task'=>'required','start_date'=>'required','target_date'=>'required','pic_id'=>'required','tasklevel_id'=>'required']);
        if($validators->fails()){
            return $this->sendResponse('0','Input Task gagal',$validators);
        }

       
        if($request->input('tasktype_id')==1){
         $a=0;                   
        }else{
         $a=7;
        }
        $vtask = DB::table('master_task')
                ->where('id_project', '=', $request->input('project_id'))
                ->where('id_task', '=', $request->input('id_task'))
                ->first();
        $typeproject = DB::table('master_project')
                ->select('project_type_id','project_name')
                ->where('id', '=', $request->input('project_id'))
                ->first();        
if(empty($vtask)){
      
       
        $task = new TaskModel();
        $task->id_project = $request->input('project_id');
        $task->id_task = $request->input('id_task');
        $task->task_name = $request->input('task_name');
        $task->start_date = $request->input('start_date');
        $task->target_date = $request->input('target_date');
        $task->status_id = 2;
        $task->pic_id = $request->input('pic_id');
        $task->task_type_id = $typeproject->project_type_id;
        $task->task_level_id = $request->input('tasklevel_id');
        $task->quick_level_id = 1;
        $task->progress_id =1;
        $task->rate_level_id = 0;
        $task->company_id = 1;
        $task->tanggal = date('Y-m-d');
        $task->progress_pct = 0;
        $task->duration = 0;
        $task->save();
       return $this->sendResponse('1','Input Task berhasil',$task);
   }else{
    return $this->sendResponse('2',$typeproject->project_name,$vtask);
   }
      
    }
   

    public function delete(Request $request, $id){
        try{
            $model = new TaskModel();
            $model->deleteData($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            }else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT  id,task_name,target_date, id_project,progress_id,notes,url_doc
            FROM master_task WHERE id =".$id);
        return json_encode($data);
    }

    public function update(Request $request){

        $validators = \Validator::make($request->all(),['progressstatus_id'=>'required','url_doc'=>'required']);
        if($validators->fails()){
            return $this->sendResponse(0,'Update Gagal', $validators);
        }

       $destination_path = public_path('uploadtask');
         $files = $request->file('url_doc');
         $filename = $files->getClientOriginalName();
        $upload_success = $files->move($destination_path, $filename);

         if($request->input('duration')<0){
            $diquick=5;
         }elseif($request->input('duration')>3){
            $diquick=1;
         }else{

            $q=DB::table('ref_quick_level')
            ->where('selisih', '=', $request->input('duration'))
            ->first();

            $diquick=$q->id;
         }

         



       if($request->input('progressstatus_id')==5){
      
        $task = TaskModel::find($request->input('id'));
        $task->end_date = $request->input('end_date');
        $task->duration = $request->input('duration');
        $task->progress_id = $request->input('progressstatus_id');
        $task->progress_pct = 100;
        $task->quick_level_id = $diquick;
        $task->notes = $request->input('notes');
        $task->url_doc = $filename;
        $task->save();

        $cekprog=DB::table('master_task')
            ->select(DB::raw('count(*) as nilai'))
            //->where('progress_id', '=',5)
            ->whereIn('status_id',[1,2])
            ->where('id_project', '=',$request->input('id_project'))
            ->first();

        $cekprog1=DB::table('master_task')
            ->select(DB::raw('count(*) as nilai'))
            ->where('progress_id', '=',5)
            ->whereIn('status_id',[1,2])
            ->whereNotNull('end_date')
            ->where('id_project', '=',$request->input('id_project'))
            ->first();   

        if($cekprog->nilai>0){
         $a=$cekprog->nilai; 
         $b=100;  
         $c=$b/$a;
         $d=$c*$cekprog1->nilai;
        



        $aa=DB::table('master_project')
            ->where('id', $request->input('id_project'))
            ->update(['progress_pct' => $d]);
            
        }    


        }else{

        $task = TaskModel::find($request->input('id'));
        $task->progress_id = $request->input('progressstatus_id');
        $task->notes = $request->input('notes');
        $task->url_doc = $filename;
        $task->save();

        }


        return $this->sendResponse(1,'Berhasil Diupdate', $task);
    }

       public function get_file($filename)
  {
        $file_path = public_path('uploadtask') . "/" . $filename;
        return Response::download($file_path);
       // return json_encode($filename);
    
  }

    public function findseq($id)
    {
        $data = \DB::select("SELECT  id_project
            FROM master_task WHERE id =" . $id ." and status_id in (1,2) limit 1");

        $seq=\DB::select("SELECT  id,progress_id
            FROM master_task WHERE id <" . $id . "and status_id in (1,2) and id_project=".$data[0]->id_project."order by id desc limit 1");    

        return json_encode($seq);
    }


    public function detail(Request $request,$value)
    {
        

        if($value=='inactive'){
            $status="In Active";
            $idstatus=1;
            $query = \DB::select("select DISTINCT on (mp.id) mp.id,mp.project_name from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
                        join master_task mt on mt.id=mst.id_task
                        join master_project mp on mt.id_project=mp.id
            where mp.status_id =2");
        }elseif($value=='onprogress'){
            $status="On Progress";
            $idstatus=2;
            $query = \DB::select("select DISTINCT on (mp.id) mp.id,mp.project_name from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
                        join master_task mt on mt.id=mst.id_task
                        join master_project mp on mt.id_project=mp.id
            where mp.progress_id=2 and mp.status_id=1");
        }elseif ($value=='finish') {
            $status="Finished";
            $idstatus=3;
            $query = \DB::select("select DISTINCT on (mp.id) mp.id,mp.project_name from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
                        join master_task mt on mt.id=mst.id_task
                        join master_project mp on mt.id_project=mp.id
            where mp.progress_id=5 and mp.status_id=1");
        }else{
            $idstatus=4;
            $status="All";
             $query = \DB::select("select DISTINCT on (mp.id) mp.id,mp.project_name from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
                        join master_task mt on mt.id=mst.id_task
                        join master_project mp on mt.id_project=mp.id");
        }
       
     

        
        $param['status']=$status;
        $param['idstatus']=$idstatus;
        $param['task'] = $query;

        if ($request->ajax()) {
            $view = view('task.detail',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'task.detail',$param); 

    }
}