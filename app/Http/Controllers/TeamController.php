<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\RefTeam;
use App\MyFunc;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
class TeamController extends Controller
{

   public function team(){
      //  $branch_type = BranchType::all();
        $team = DB::table('ref_team')
            ->where('status', '=',1)
            ->get();
        return json_encode($team);
    }

    public function teamid($id){
      //  $branch_type = BranchType::all();
        $team = DB::table('ref_team')
            ->where('status', '=',1)
            ->where('client_id',$id)
            ->get();
        return json_encode($team);
    }
     public function teamid1(){
      //  $branch_type = BranchType::all();
        $team = DB::table('ref_team')
            ->where('status', '=',1)
            ->get();
        return json_encode($team);
    }
   public function index(Request $request)
    {
        if ($request->ajax()) {
            $view = view('team.index')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'team.index');
    }


    public function table(Request $request){
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY t.id) AS nomor_urut, t.id, t.definition, t.client_id,t.status,c.definition as client
            FROM ref_team t join ref_client c on (c.id = t.client_id) where t.status=1");

      $data = Datatables::of($query)->addColumn('action', function ($query){
            return "
            <i class='fa fa-pencil' style='color:blue;' title='Edit'  onclick='editshow(".$query->id.")'></i>
            <i style='color:red;' title='Hapus' onclick='hapus($query->id,\"team/delete\");' class='fa fa-trash'></i>";
        })->make(true);

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(),['definition'=>'required','client_id'=>'required']);
        if($validators->fails()){
            return $this->sendResponse('0','Input Team gagal',$validators);
        }
        
        $user = new RefTeam();
        $user->definition = $request->input('definition');
        $user->client_id = $request->input('client_id');
        $user->status = 1;
        $user->save();
       return $this->sendResponse('1','Input Team berhasil',$user);
    }
   
    


    public function delete(Request $request, $id){
         $st=DB::table('master_project')
            ->where('team_id', '=',$id)
            ->where('status_id',1)
            ->first();

       if(empty($st)){
        $real_lm = DB::table('ref_team')
            ->where('id', $id)
            ->update(['status' => 0]);

        return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
            }else{
           $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
                
            }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT id, definition, client_id
FROM ref_team WHERE id =".$id);
        return json_encode($data);
    }

    public function update(Request $request){

        $validators = \Validator::make($request->all(),['definition'=>'required','client_id'=>'required']);
        if($validators->fails()){
            return $this->sendResponse(0,'Update Gagal', $validators);
        }

      //  var_dump($request->all());

        $real_lm = RefTeam::find($request->input('id'));
        $real_lm ->definition= $request->input('definition');
        $real_lm ->client_id= $request->input('client_id');
        $real_lm->save();

        


        return $this->sendResponse(1,'Berhasil Diupdate', $real_lm);
    }

     public function detailtask($id,Request $request){

        $param['team'] = \DB::select("select rt.id,u.full_name,rt.definition as team,rc.definition as client
                        from users u 
                        join ref_team rt on rt.id=u.team_id 
                        join ref_client rc on rc.id=rt.client_id
                        where rt.id=:id and u.status=1",['id'=>$id]);

        $param['task'] = \DB::select("select mst.sub_task_name,mst.duration,ps.definition as progress,rs.definition as status from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
            where u.team_id=:id and mst.progress_id in (2,5)",['id'=>$id]);

         $param['project'] = \DB::select("select DISTINCT on (pm.id) pm.id,project_name,mt.pic_id from master_project pm
            join master_task mt on mt.id_project=pm.id
            where pm.team_id=:id",['id'=>$id]);

        $param['done'] = \DB::select("select mst.sub_task_name,mst.duration,ps.definition as progress,rs.definition as status from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
            where u.team_id=:id and mst.progress_id =5",['id'=>$id]);

        $param['onprogress'] = \DB::select("select mst.sub_task_name,mst.duration,ps.definition as progress,rs.definition as status from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
            where u.team_id=:id and mst.progress_id =2",['id'=>$id]);

         if ($request->ajax()) {
            $view = view('team.detail',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'team.detail',$param);
    }


     public function excel($id){
        //$fields=['noid'=>'No. ID','namalengkap'=>'Nama Siswa','kelamin'=>'Jenis Kelamin','agama'=>'Agama'];
         $fields=[];
         $query = \DB::select("select DISTINCT on (pm.id) pm.id,project_name,mt.pic_id,rt.definition from master_project pm
            join master_task mt on mt.id_project=pm.id
            join ref_team rt on rt.id=pm.team_id
            where pm.team_id=:id",['id'=>$id]);

        return MyFunc::printto($query,'Detail Task Team: '.$query[0]->definition, $fields, 0,['excel'=>'team.excel'],'landscape','a4');
    }

}