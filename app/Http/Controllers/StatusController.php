<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\StatusModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
class StatusController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','checkrole']);
    }

     public function status(){
      //  $branch_type = BranchType::all();
        $team = DB::table('ref_status')
            ->where('status', '=',1)
            ->get();
        return json_encode($team);
    }

    public function status1(){
      //  $branch_type = BranchType::all();
        $team = DB::table('ref_status')
            ->where('status', '=',1)
            ->where('id', '=',1)
            ->get();
        return json_encode($team);
    }

   public function index(Request $request)
    {
        if ($request->ajax()) {
            $view = view('status.index')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'status.index');
    }


    public function table(Request $request){
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY id) AS nomor_urut, id, definition, seq,status
            FROM ref_status where status=1");

        $data = Datatables::of($query)->addColumn('action', function ($query){
            return "
            <i class='fa fa-pencil' style='color:blue;' title='Edit'  onclick='editshow(".$query->id.")'></i>
            <i style='color:red;' title='Hapus' onclick='hapus($query->id,\"status/delete\");' class='fa fa-trash'></i>";
            
        })->make(true);

        return $data;
    }

    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(),['definition'=>'required','seq'=>'required']);
        if($validators->fails()){
            return $this->sendResponse('0','Input Status gagal',$validators);
        }
        
        $Client = new StatusModel();
        $Client->definition = $request->input('definition');
        $Client->seq = $request->input('seq');
        $Client->status = 1;
        $Client->save();
       return $this->sendResponse('1','Input Statu berhasil',$Client);
    }
   

    public function delete(Request $request, $id){
        try{
            $model = new StatusModel();
            $model->deleteData($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            }else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT id,definition,seq FROM ref_status WHERE id =".$id);
        return json_encode($data);
    }

    public function update(Request $request){

        $validators = \Validator::make($request->all(),['definition'=>'required','seq'=>'required']);
        if($validators->fails()){
            return $this->sendResponse(0,'Update Gagal', $validators);
        }

      //  var_dump($request->all());

        $Client = StatusModel::find($request->input('id'));
        $Client->definition = $request->input('definition');
        $Client->seq = $request->input('seq');
        $Client->save();


        return $this->sendResponse(1,'Berhasil Diupdate', $Client);
    }

   

}