<?php

namespace App\Http\Controllers;

use App\ProjectModel;
use App\SubtaskModel;
use App\TaskModel;
use App\MyFunc;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Response;
use DB;
use Hash;
use Auth;

class LaporanController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
    
     public function laporandetail(Request $request)
    {
        $param['team'] = \DB::select("select mp.id,project_name,rpt.definition,rt.definition as team,u.full_name,count(CASE WHEN mt.status_id in (1,2) THEN 1 END) as taskprog,count(CASE WHEN mst.status_id in (1,2) THEN 1 END)as subtask from master_project mp
join ref_project_type rpt on rpt.id=mp.project_type_id
join ref_team rt on rt.id=mp.team_id
join users u on u.id=mp.pm_id
join master_task mt on mt.id_project=mp.id
join master_sub_task mst on mst.id_task=mt.id
GROUP BY mp.id,project_name,rpt.definition,rt.definition,u.full_name");
       
        
        if ($request->ajax()) {
            $view = view('laporan.lapdetail',$param)->renderSections();
            return json_encode($view);
        }    
        return view('master.master')->nest('child', 'laporan.lapdetail',$param);
        
        
    }

     public function laporanperpic(Request $request)
    {
        $param['team'] = \DB::select("select * from ref_team where status=1");
       
        
        if ($request->ajax()) {
            $view = view('laporan.lappic',$param)->renderSections();
            return json_encode($view);
        }    
        return view('master.master')->nest('child', 'laporan.lappic',$param);
        
        
    }
    public function laporanproker(Request $request)
    {
        $param['team'] = \DB::select("select * from ref_team where status=1");
       
        
        if ($request->ajax()) {
            $view = view('laporan.lapproker',$param)->renderSections();
            return json_encode($view);
        }    
        return view('master.master')->nest('child', 'laporan.lapproker',$param);
        
        
    }
    
    public function laporanperunit(Request $request)
    {
        $param['team'] = \DB::select("select * from ref_team where status=1");
        
        if ($request->ajax()) {
            $view = view('laporan.lapunit',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'laporan.lapunit',$param);
    }


           
 
    public function excel(Request $request){
                // Your array of ids
        $id_list = $request->customCheck;
        $fields=[];
        // Make your array of ids into a comma separated string
        $id_str = implode(',',$id_list);
        $query = \DB::select("select rt.definition as unit,count(mp.*) as jml_proker,
        count(CASE WHEN mp.progress_id = 5 THEN 1 END) as done,
        count(CASE WHEN mp.progress_id = 2 and mp.status_id=1 THEN 1 END) as onprogress,
        count(CASE WHEN mp.status_id = 2 THEN 1 END) as inactive,
        round(sum(case when mp.progress_id = 5 THEN 1 else 0 end)/count(mp.*)::decimal*100,0) as Pdone,
        round(sum(case when mp.progress_id = 2 and mp.status_id=1 THEN 1 else 0 end)/count(mp.*)::decimal*100,0) as Ponprogress,
        round(sum(case when mp.status_id = 2 THEN 1 else 0 end)/count(mp.*)::decimal*100,0) as Pinactive
        from ref_team rt
         join master_project mp on rt.id=mp.team_id
        where rt.id = ANY (string_to_array(:id,',')::int[]) 
        GROUP BY rt.definition",['id'=>$id_str]);

        if($request->has('excel')){
        return MyFunc::printto($query,'Laporan : Progress Program Kerja Per Unit (Rekap)', $fields, 0,['excel'=>'laporan.excel'],'landscape','a4');      
        }
        if($request->has('pdf')){
        return myFunc::pdf($query,'Laporan : Progress Program Kerja Per Unit (Rekap)',$fields,'laporan.pdf');
        }

    }

   public function excel1(Request $request){
                // Your array of ids
        $id_list = $request->customCheck;
        $fields=[];
        // Make your array of ids into a comma separated string
        $id_str = implode(',',$id_list);
        $query = \DB::select("select rt.full_name,count(mst.*) as jml_proker,
        count(CASE WHEN mst.progress_id = 5 THEN 1 END) as done,
        count(CASE WHEN mst.progress_id = 2 and mst.status_id=1 THEN 1 END) as onprogress,
        count(CASE WHEN mst.status_id = 3 THEN 1 END) as inactive,
        round(sum(case when mst.progress_id = 5 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Pdone,
        round(sum(case when mst.progress_id = 2 and mst.status_id=1 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Ponprogress,
        round(sum(case when mst.status_id = 3 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Pinactive
        from users rt
        join master_sub_task mst on mst.pic_id=rt.id
        where rt.id = ANY (string_to_array(:id,',')::int[]) 
        GROUP BY rt.full_name",['id'=>$id_str]);

        if($request->has('excel')){
        return MyFunc::printto($query,'Laporan : Progress Program Kerja Per PIC (Rekap)', $fields, 0,['excel'=>'laporan.excel1'],'landscape','a4');      
        }
        if($request->has('pdf')){
        return myFunc::pdf($query,'Laporan : Progress Program Kerja Per PIC (Rekap)',$fields,'laporan.pdf1');
        }

    }

    public function excel2(Request $request){
                // Your array of ids
        $id_list = $request->customCheck;
        $fields=[];
        // Make your array of ids into a comma separated string
        $id_str = implode(',',$id_list);
        $query = \DB::select("select rt.definition, mp.project_name,rps.definition as status,count(CASE WHEN mt.status_id in (1,2) THEN 1 END) as taskprog,count(CASE WHEN mst.status_id in (1,2) THEN 1 END)as subtask,count(CASE WHEN mst.progress_id=5 THEN 1 END)as subtaskprog ,round(sum(case when mst.progress_id = 5 and mst.status_id=1 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Pdone,
            u.full_name
            from ref_team rt
            join master_project mp on mp.team_id=rt.id
             join ref_progress_status rps on rps.id=mp.progress_id
             join master_task mt on mt.id_project=mp.id
            join master_sub_task mst on mst.id_task=mt.id
            join users u on u.id=mp.pm_id
            where rt.id = ANY (string_to_array(:id,',')::int[]) 
            GROUP BY rt.definition, mp.project_name,rps.definition,u.full_name",['id'=>$id_str]);

        if($request->has('excel')){
        return MyFunc::printto($query,'Laporan : Progress Program Kerja', $fields, 0,['excel'=>'laporan.excel2'],'landscape','a4');      
        }
        if($request->has('pdf')){
        return myFunc::pdf($query,'Laporan : Progress Program Kerja',$fields,'laporan.pdf2');
        }

    }
    public function excel3(Request $request){
       $fields=[];
        $query = \DB::select("select count(*) as jml_proker,count(CASE WHEN mst.progress_id = 5 THEN 1 END) as done,
        count(CASE WHEN mst.progress_id = 2  and mst.status_id=1 THEN 1 END) as onprogress,
        count(CASE WHEN mst.status_id = 3 THEN 1 END) as inactive,
        round(sum(case when mst.progress_id = 5 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Pdone,
        round(sum(case when mst.progress_id = 2 and mst.status_id=1 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Ponprogress,
        round(sum(case when mst.status_id = 3 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Pinactive
                from master_sub_task mst");
        
        return MyFunc::printto($query,'Report : Progress (Consolidation)', $fields, 0,['excel'=>'laporan.excel3'],'landscape','a4');      
       

    }

    public function pdf3(Request $request){
        $fields=[];
        $query = \DB::select("select count(*) as jml_proker,count(CASE WHEN mst.progress_id = 5 THEN 1 END) as done,
        count(CASE WHEN mst.progress_id = 2 and mst.status_id=1 THEN 1 END) as onprogress,
        count(CASE WHEN mst.status_id = 3 THEN 1 END) as inactive,
        round(sum(case when mst.progress_id = 5 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Pdone,
        round(sum(case when mst.progress_id = 2 and mst.status_id=1 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Ponprogress,
        round(sum(case when mst.status_id = 3 THEN 1 else 0 end)/count(mst.*)::decimal*100,0) as Pinactive
                from master_sub_task mst");
        
        return myFunc::pdf($query,'Laporan : Progress Program Kerja',$fields,'laporan.pdf3');   
       

    }

    public function excel4(Request $request,$id){
       $fields=[];
        $query = \DB::select("select mp.id,project_name,rpt.definition,rt.definition as team,u.full_name,count(CASE WHEN mt.status_id in (1,2) THEN 1 END) as taskprog,count(CASE WHEN mst.status_id in (1,2) THEN 1 END)as subtask from master_project mp
join ref_project_type rpt on rpt.id=mp.project_type_id
join ref_team rt on rt.id=mp.team_id
join users u on u.id=mp.pm_id
join master_task mt on mt.id_project=mp.id
join master_sub_task mst on mst.id_task=mt.id
where mp.id=:id
GROUP BY mp.id,project_name,rpt.definition,rt.definition,u.full_name",['id'=>$id]);
        
       
        return MyFunc::printto($query,'Report : Detail Program Kerja', $fields, 0,['excel'=>'laporan.excel4'],'landscape','a4');      

    }

    public function pdf4(Request $request,$id){
        $fields=[];
        $query = \DB::select("select mp.id,project_name,rpt.definition,rt.definition as team,u.full_name,count(CASE WHEN mt.status_id in (1,2) THEN 1 END) as taskprog,count(CASE WHEN mst.status_id in (1,2) THEN 1 END)as subtask from master_project mp
join ref_project_type rpt on rpt.id=mp.project_type_id
join ref_team rt on rt.id=mp.team_id
join users u on u.id=mp.pm_id
join master_task mt on mt.id_project=mp.id
join master_sub_task mst on mst.id_task=mt.id
where mp.id=:id
GROUP BY mp.id,project_name,rpt.definition,rt.definition,u.full_name",['id'=>$id]);
        
        return myFunc::pdf($query,'Report : Detail Program Kerja',$fields,'laporan.pdf4');   
       

    }





}
