<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\RefTaskModel;
use App\RefSubtaskModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
class RefTaskController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','checkrole']);
    }


   public function index(Request $request)
    {
        if ($request->ajax()) {
            $view = view('reftask.index')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'reftask.index');
    }


    public function table(Request $request){
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY rt.id) AS nomor_urut, rt.id, rt.definition
            ,rt.status,rm.name_model
            FROM ref_task rt join ref_model rm on rm.id=rt.id_model where rt.status=1");

        $data = Datatables::of($query)->addColumn('action', function ($query){
            return "
            <i class='fa fa-pencil' style='color:blue;' title='Edit'  onclick='editshow(".$query->id.")'></i>
            <i style='color:red;' title='Hapus' onclick='hapus($query->id,\"refTask/delete\");' class='fa fa-trash'></i>";
            
        })->make(true);

        return $data;
    }

    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(),['definition'=>'required']);
        if($validators->fails()){
            return $this->sendResponse('0','Input Task gagal',$validators);
        }
        
        $Client = new RefTaskModel();
        $Client->definition = $request->input('definition');
        $Client->id_model = $request->input('id_model'); 
        $Client->status = 1;
        $Client->save();
       return $this->sendResponse('1','Input Task berhasil',$Client);
    }
   

    public function delete(Request $request, $id){
        $st=DB::table('master_task')
            ->where('id_task', '=',$id)
            ->where('status_id',1)
            ->first();

       if(empty($st)){
        $real_lm = DB::table('ref_task')
            ->where('id', $id)
            ->update(['status' => 0]);

        return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
            }else{
           $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
                
            }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT id,definition FROM ref_task WHERE id =".$id);
        return json_encode($data);
    }

    public function update(Request $request){

        $validators = \Validator::make($request->all(),['definition'=>'required']);
        if($validators->fails()){
            return $this->sendResponse(0,'Update Gagal', $validators);
        }

      //  var_dump($request->all());

        $Client = RefTaskModel::find($request->input('id'));
        $Client->definition = $request->input('definition');
        $Client->id_model = $request->input('id_model');
        $Client->save();


        return $this->sendResponse(1,'Berhasil Diupdate', $Client);
    }

    public function Reftask(){
      //  $branch_type = BranchType::all();
        $team = DB::table('ref_task')
            ->where('status', '=',1)
            ->get();
        return json_encode($team);
    }
    public function Reftask1($id){
      //  $branch_type = BranchType::all();
        $team = DB::table('ref_task')
            ->where('id_model',$id)
            ->where('status', '=',1)
            ->get();
        return json_encode($team);
    }

     public function taskid($id,$idproject){
      //  $branch_type = BranchType::all();
        $project = DB::table('master_project')
            ->where('id',$idproject)
            ->first();

        /*
        $team = DB::table('ref_task')
            ->where('id_model',$id)
            ->where('status', '=',1)
            ->get();
        */

        $team = \DB::select("select * from ref_task where id_model=:idmodel and id not in (
                SELECT id_task from master_task where id_project=:id
                )",['id'=>$idproject,'idmodel'=>$id]);           

        $branches = [];
        foreach ($team as $da){
            $branch = RefSubtaskModel::select('id','id_task','definition')->where('id_task','=',$da->id)->where('status','=',1)->get()->toArray();

            $pic = DB::table('users')
            ->select('users.id','users.full_name')
            ->join ('model_has_roles','model_has_roles.model_id','=','users.id') 
            ->where('model_has_roles.role_id',1)
            ->where('team_id',$project->team_id)
            ->where('status', '=',1)
            ->get()->toArray();

            $leveltask = DB::table('ref_task_level')
            ->where('status', '=',1)
            ->get()->toArray();

            array_push($branches,['idtask' => $da->id,'task' => $da->definition, 'branches' => $branch,'user' => $pic,'leveltask' => $leveltask,'startdate' => $project->start_date,'targetdate' => $project->target_date]);
        }
        return json_encode($branches);
    }

}