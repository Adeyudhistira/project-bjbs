<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
    {
        $team_id = \Auth::user()->getAttributes()['team_id'];
        /*
         $param['assigned'] = DB::table('master_task')
            ->select(DB::raw('count(*) as jml'))
            ->where('progress_id',1)
            ->first();
        */
         $param['iddash']=1;   
         $param['project__'] = \DB::select('select project_name, progress_pct, full_name pmo,definition
                from (
          SELECT project_name, mp.progress_pct, u.full_name,pt.definition
                from master_project mp JOIN users u ON mp.pm_id = u.id 
                                join master_task st on st.id_project=mp.id
                                join master_sub_task mst on mst.id_task=st.id
                join ref_project_type pt on pt.id=mp.project_type_id
                where mst.progress_id in (2,5) and mst.status_id in (1,2)  
             ) t
            GROUP BY project_name, progress_pct, full_name,definition'); 

        $param['project'] = DB::table('master_project')
                ->select(DB::raw('avg(master_project.progress_pct) as avg'),'master_project.id','master_project.project_name','master_project.progress_pct','users.full_name','ref_project_type.definition')
                ->join('users','users.id','=','master_project.pm_id')
                ->join('master_task','master_task.id_project','=','master_project.id')
                ->join('master_sub_task','master_sub_task.id_task','=','master_task.id')
                ->join('ref_project_type','ref_project_type.id','=','master_project.project_type_id')
                ->whereIn('master_sub_task.progress_id', [2,5])
                ->whereIn('master_sub_task.status_id',[1,2])
                ->groupBy('master_project.project_name')
                ->groupBy('master_project.progress_pct')
                ->groupBy('users.full_name')
                ->groupBy('ref_project_type.definition')
                ->groupBy('master_project.id')
                ->orderBy('master_project.progress_pct')->paginate(5);
                   
        $param['rbb'] = DB::table('master_project')
                ->where('project_type_id', 1)
                //->whereNotNull('end_date')
                ->avg('progress_pct');  

        $param['nrbb'] = DB::table('master_project')
                ->where('project_type_id', 2)
                ->avg('progress_pct');  

        $param['rbbteam'] = \DB::select('select rpt.definition,rt.definition as team ,floor(avg(mst.progress_pct)) as progress 
                    from master_project mst
            join ref_project_type rpt on rpt.id=mst.project_type_id
            join users u on u.id=mst.pm_id
            join ref_team rt on rt.id=mst.team_id
            GROUP BY rpt.definition,rt.definition');        
      

         $param['pegawai'] = \DB::select('select photo, full_name,(avgrate+avgquick)/2
                from (
            select u.photo,u.full_name, avg(mst.rate_level_id) as avgrate,
            avg(mst.quick_level_id) as avgquick
                from users u
                join master_sub_task mst on mst.pic_id=u.id
                where mst.progress_id=5
                  group by u.photo,u.full_name
             ) t
--group by photo, full_name
order by 3 desc limit 1');

        $param['workload'] = \DB::select("SELECT
              CASE
              WHEN sum(tl.hour) <= 14
                THEN 'Low'
              WHEN sum(tl.hour) <= 28
                THEN 'Medium'
              WHEN sum(tl.hour) <= 56
                THEN 'High'
              WHEN sum(tl.hour) > 56
                THEN 'Overload'
              END AS workload,
              u.full_name,u.id,
                 CASE
              WHEN  floor(avg(mst.rate_level_id)) = 1
                THEN 'Poor'
              WHEN  floor(avg(mst.rate_level_id)) = 2
                THEN 'Average'
              WHEN  floor(avg(mst.rate_level_id)) = 3
                THEN 'Good'
              WHEN  floor(avg(mst.rate_level_id)) = 4
                THEN 'Very Good'
                WHEN  floor(avg(mst.rate_level_id)) = 5
                THEN 'Excellence'
              END AS rate_level_id,
            floor(  avg(mst.rate_level_id) )as t
            FROM users u
              JOIN master_sub_task mst ON mst.pic_id = u.id
              JOIN ref_task_level tl ON tl.id = mst.sub_task_level_id
              join model_has_roles hr on hr.model_id=u.id
            WHERE mst.status_id IN (1, 2) and mst.progress_id IN (2, 5) and hr.role_id=2
            GROUP BY u.full_name,u.id");

        $param['workloadteam'] = \DB::select("SELECT
              CASE
              WHEN sum(tl.hour) <= 14
                THEN 'Low'
              WHEN sum(tl.hour) <= 28
                THEN 'Medium'
              WHEN sum(tl.hour) <= 56
                THEN 'High'
              WHEN sum(tl.hour) > 56
                THEN 'Overload'
              END AS workload,
              rt.definition,rt.id,
                 CASE
              WHEN  floor(avg(mst.rate_level_id)) = 1
                THEN 'Poor'
              WHEN  floor(avg(mst.rate_level_id)) = 2
                THEN 'Average'
              WHEN  floor(avg(mst.rate_level_id)) = 3
                THEN 'Good'
              WHEN  floor(avg(mst.rate_level_id)) = 4
                THEN 'Very Good'
                WHEN  floor(avg(mst.rate_level_id)) = 5
                THEN 'Excellence'
              END AS rate_level_id,
            floor(  avg(mst.rate_level_id) )as t
            FROM users u
                        join ref_team rt on rt.id=u.team_id
              JOIN master_sub_task mst ON mst.pic_id = u.id
              JOIN ref_task_level tl ON tl.id = mst.sub_task_level_id
              join model_has_roles hr on hr.model_id=u.id
            WHERE mst.status_id IN (1, 2) AND mst.progress_id IN (2, 5) and hr.role_id = 2
            GROUP BY rt.definition,rt.id");

        if ($request->ajax()) {
            $view = view('home',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'home',$param);
    }

    public function jmlfinish(){
       $team = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            ->where('status_id',1)
            ->where('progress_id',5)
            ->get();
        return json_encode($team);
    }

    public function jmlinactive(){
         $data = DB::table('master_project')->select(DB::raw('count(*) as jml'))
             //->where('is_approve','=', false)
             //->where('progress_id','=', 5)
             ->where('status_id',2)
             ->get();
         return json_encode($data);
    }

    public function jmlonprogress(){
       $team = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            ->where('status_id',1)
            ->where('progress_id',2)
            ->get();
        return json_encode($team);
    }
      public function jmlactive(){
       $team = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            //->where('progress_id',5)
            ->where('status_id',1)
            //->where('is_approve' ,'=', true)
            ->get();
        return json_encode($team);
    }
    public function jmlproject(){
       $team = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            ->whereIn('status_id',[1,2])
            ->get();
        return json_encode($team);
    }

      public function task(){
      $team = \DB::select('select count(case when ms.is_approve= true THEN 1 end) as done,count(case when ms.progress_id= 2 THEN 1 end) as onprogress
 from ref_progress_status ps 
 join master_sub_task ms on ms.progress_id=ps.id
 where ps.id in(1,2,5) and ms.status_id in (1,2) '); 

        return json_encode($team);
    }

      public function dashboard(Request $request)
    {

        if ($request->ajax()) {
            $view = view('dashboard')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'dashboard');
    }

}
