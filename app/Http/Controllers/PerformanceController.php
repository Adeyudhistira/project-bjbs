<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\PerformanceModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
class PerformanceController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','checkrole']);
    }


   public function index(Request $request)
    {
        if ($request->ajax()) {
            $view = view('performance.index')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'performance.index');
    }


    public function table(Request $request){
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY id) AS nomor_urut, id, definition,buffer ,seq,status
            FROM ref_performance where status=1");

        $data = Datatables::of($query)->addColumn('action', function ($query){
            return "
            <i class='fa fa-pencil' style='color:blue;' title='Edit'  onclick='editshow(".$query->id.")'></i>
            <i style='color:red;' title='Hapus' onclick='hapus($query->id,\"performance/delete\");' class='fa fa-trash'></i>";
            
        })->make(true);

        return $data;
    }

    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(),['definition'=>'required','buffer'=>'required','seq'=>'required']);
        if($validators->fails()){
            return $this->sendResponse('0','Input Performance gagal',$validators);
        }
        
        $Client = new PerformanceModel();
        $Client->definition = $request->input('definition');
        $Client->buffer = $request->input('buffer');
        $Client->seq = $request->input('seq');
        $Client->status = 1;
        $Client->save();
       return $this->sendResponse('1','Input Performance berhasil',$Client);
    }
   

    public function delete(Request $request, $id){
        try{
            $model = new PerformanceModel();
            $model->deleteData($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            }else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT id,definition,buffer,seq FROM ref_performance WHERE id =".$id);
        return json_encode($data);
    }

    public function update(Request $request){

        $validators = \Validator::make($request->all(),['definition'=>'required','buffer'=>'required','seq'=>'required']);
        if($validators->fails()){
            return $this->sendResponse(0,'Update Gagal', $validators);
        }

      //  var_dump($request->all());

        $Client = PerformanceModel::find($request->input('id'));
        $Client->definition = $request->input('definition');
        $Client->buffer = $request->input('buffer');
        $Client->seq = $request->input('seq');
        $Client->save();


        return $this->sendResponse(1,'Berhasil Diupdate', $Client);
    }

   

}