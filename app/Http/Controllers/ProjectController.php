<?php

namespace App\Http\Controllers;

use App\ProjectModel;
use App\TaskModel;
use App\SubtaskModel;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Response;
use DB;
use Hash;
use Auth;
use App\MyFunc;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $team_id = \Auth::user()->getAttributes()['team_id'];
        if(Auth::user()->hasRole('Admin')){
        $param['project'] = DB::table('master_project')->paginate(5);
        
        }else{
        $param['project'] = DB::table('master_project')->where('team_id',$team_id)->paginate(5);
             
        }
       
        $param['id']=null;
        $param['user']=null;
        $param['dis']='none';
        if ($request->ajax()) {
            $view = view('projects.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'projects.index',$param);
    }


    public function detail($id,Request $request)
    {

         $team_id = \Auth::user()->getAttributes()['team_id'];
        if(Auth::user()->hasRole('Admin')){
        $param['project'] = DB::table('master_project')->paginate(5);
        
        }else{
        $param['project'] = DB::table('master_project')->where('team_id',$team_id)->paginate(5);
             
        }
        $param['id']=$id;
        $param['dis']='show';
        $param['user'] = \DB::select("select mp.id,mp.close,mp.project_name,mp.start_date,mp.target_date,u.full_name,u.photo,mst.sub_task_name from 
            master_project mp join users u on u.id=mp.pm_id 
            left join master_task mt on mt.id_project=mp.id
            left join master_sub_task mst on mst.id_task=mt.id
            where mp.id=:id  
            order by mst.end_date desc",['id'=>$id]);
        
        $param['det'] = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY mp.id) AS id, mp.id id_project, u.id as iduser,project_name, start_date,target_date,url_doc, end_date, progress_pct, u.full_name pmo, duration, status_id,no_sk,tgl_sk,perihal_sk,upload_doc
from master_project mp JOIN users u ON mp.pm_id = u.id where mp.id=:id",['id'=>$id]);

        if ($request->ajax()) {
            $view = view('projects.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'projects.index',$param);
    }

    public function detailtask($id,Request $request)
    {

         $team_id = \Auth::user()->getAttributes()['team_id'];
        if(Auth::user()->hasRole('Admin')){
        $param['project'] = DB::table('master_project')->paginate(5);
        
        }else{
        $param['project'] = DB::table('master_project')->where('team_id',$team_id)->paginate(5);
             
        }
        $param['id']=$id;
        $param['dis']='show';
        $param['user'] = \DB::select("select mp.id, mp.project_name,mp.start_date,mp.target_date,u.full_name,u.photo from master_project mp join users u on u.id=mp.pm_id where mp.id=:id",['id'=>$id]);

        $param['userp'] = \DB::select("select mp.id, mp.project_name,mp.start_date,mp.target_date,u.full_name,u.photo
                        ,mp.end_date,rs.definition,mp.status_id
                        from master_project mp 
                        join users u on u.id=mp.pm_id 
                        join ref_status rs on rs.id=mp.status_id
                        where mp.id=:id",['id'=>$id]);
        $param['detailtask'] = \DB::select("SELECT t.id_project,t.tanggal,p.project_name, t.id,t.task_name,t.created_at,t.progress_pct,ps.definition,t.progress_id,u.full_name,p.url_doc,t.id_task,p.start_date,p.target_date,p.end_date,rs.definition as status,p.status_id
            FROM master_task t join master_project p on p.id=t.id_project join users u on u.id=p.pm_id join ref_progress_status ps on ps.id=t.progress_id join ref_status rs on rs.id=p.status_id where t.id_project = :id",['id'=>$id]);
        $param['onprogress'] = \DB::select("SELECT mst.* FROM master_sub_task mst 
                        join master_task t on t.id=mst.id_task
                        join master_project p on p.id=t.id_project 
                        join users u on u.id=mst.pic_id 
                        join ref_progress_status ps on ps.id=t.progress_id 
                        where mst.status_id in (1,2) and mst.progress_id=2 and t.id_project = :id",['id'=>$id]);
        $param['total'] = \DB::select("SELECT mst.* FROM master_sub_task mst 
                        join master_task t on t.id=mst.id_task
                        join master_project p on p.id=t.id_project 
                        join users u on u.id=mst.pic_id 
                        join ref_progress_status ps on ps.id=t.progress_id 
                        where mst.status_id in (1,2) and t.id_project = :id",['id'=>$id]);
        
        $param['done'] = \DB::select("SELECT mst.* FROM master_sub_task mst 
                        join master_task t on t.id=mst.id_task
                        join master_project p on p.id=t.id_project 
                        join users u on u.id=mst.pic_id 
                        join ref_progress_status ps on ps.id=t.progress_id 
                        where mst.status_id in (1,2) and mst.progress_id=5 and t.id_project = :id",['id'=>$id]);

        $param['task'] = \DB::select("SELECT mt.id,mt.id_project,mt.task_name,mt.duration,mt.status_id,rs.definition from master_task mt
                        join ref_status rs on rs.id=mt.status_id
                        where id_project=:id",['id'=>$id]);

       

        

        

        if ($request->ajax()) {
            $view = view('projects.detail',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'projects.detail',$param);
    }



     public function project(){
      //  $branch_type = BranchType::all();
        $team = DB::table('master_project')
            ->whereIn('status_id',[1,2])
            ->where('close', '=',0)
           // ->where('team_id', '=',Auth::user()->team_id)
            ->get();
        return json_encode($team);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $range = $request->input('range_date');
        $start = substr($range,0,10);
        $end = substr($range, 12,23);
        /*$validators = \Validator::make($request->all(),['project_id'=>'required','task_name'=>'required','start_date'=>'required','target_date'=>'required','status_id'=>'required','pic_id'=>'required','tasktype_id'=>'required','tasklevel_id'=>'required']);
        if($validators->fails()){
            return $this->sendResponse('0','Input project gagal',$validators);
        }*/

        if($request->hasFile('file')){
            if($request->input('is_paralel')){
            $paralel=1;    
            }else{
            $paralel=0;    
            }
            

            $now = date('Y-m-d');
            $project = new ProjectModel();
            $project->project_name = $request->input('project_name');
            $project->start_date = $start;
            //$project->end_date = $end;
            $project->target_date = $end ;
            $project->project_type_id = $request->input('project_type_id');
            $project->client_id = $request->input('client_id');
            $project->progress_pct = 0;

            /*
            $start_date = strtotime($start);
            if($start_date < $now )
                $status = 2;
            else
                $status = 1;
            */

            $project->status_id = 2;
            $project->team_id = $request->input('team_id');
            $project->pm_id = $request->input('pm_id');
            $project->duration = 0;
            $project->close = 0;

            //$file = $request->file('file');
            //$file_extension = $file->getClientOriginalExtension();
            //$new_file_name = rand(100000,1001238912).".".$file_extension;

            $destination_path = public_path('support_docs');
            $files = $request->file('file');
            $filename = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $filename);

            $project->url_doc = $filename;
            $project->project_budget = str_replace(".","",$request->input('project_budget'));
            $project->company_id = 1;
            $project->progress_id = 2;
            $project->is_parallel = $paralel;
            $project->save();
            //$file->move('public/support_docs',$new_file_name);
            return $this->sendResponse('1', 'Input project berhasil', $project);
        }else{
            return $this->sendResponse('2','Input project gagal');
        }

    }

    public function test(){
        return ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        /*$validators = \Validator::make($request->all(),['company_name'=>'required','address'=>'required','phone'=>'required','city'=>'required','province'=>'required','seq'=>'required']);
        if($validators->fails()){
            return $this->sendResponse(0,'Update Gagal', $validators);
        }*/

        if($request->hasFile('e_url_doc')){
         if($request->input('is_paralel')){
            $paralel=1;    
            }else{
            $paralel=0;    
            }
        $destination_path = public_path('support_docs');
        $files = $request->file('e_url_doc');
        $filename = $files->getClientOriginalName();
        $upload_success = $files->move($destination_path, $filename);

        $project = ProjectModel::find($request->input('id'));
        $project->project_name = $request->input('e_project_name');
        $project->start_date = $request->input('e_start_date');
        $project->target_date = $request->input('e_target_date');
        $project->pm_id = $request->input('e_pm_id');
         $project->url_doc = $filename;
        $project->status_id = $request->input('e_status_id');
        $project->is_parallel = $paralel;
        $project->save();     
        }else{
         if($request->input('is_paralel')){
            $paralel=1;    
            }else{
            $paralel=0;    
            }    
        $project = ProjectModel::find($request->input('id'));
        $project->project_name = $request->input('e_project_name');
        $project->start_date = $request->input('e_start_date');
        $project->target_date = $request->input('e_target_date');
        $project->pm_id = $request->input('e_pm_id');
        $project->status_id = $request->input('e_status_id');
        $project->is_parallel = $paralel;
        $project->save();     
        }
       

        return $this->sendResponse(1,'Update project berhasil', $project);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function table(Request $request){
        $query = \DB::select('SELECT ROW_NUMBER() OVER (ORDER BY mp.id) AS id, mp.id id_project, project_name, start_date,target_date, end_date, progress_pct, u.full_name pmo, duration, status_id
from master_project mp JOIN users u ON mp.pm_id = u.id');

        $data = Datatables::of($query)
            ->addColumn('project', function ($query){
                return '<a href="task/index/'.$query->id_project.'">'.$query->project_name.'</a>';
            })
            ->addColumn('status', function ($query){
            switch ($query->status_id){
                case 1:
                    return '<div class="badge badge-success">Active</div>';
                    break;
                case 2:
                    return '<div class="badge badge-secondary">Inactive</div>';
                    break;
                case 3:
                    return '<div class="badge badge-danger">Dropped</div>';
                    break;
            }})
            ->addColumn('action', function ($query){
            return '<button type="button" class="btn btn-info bg-blue" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" onclick="editshow('.$query->id_project.')">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-cyan bg-cyan" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Support Docs" onclick="download('.$query->id_project.')">
                        <i class="fa fa-download"></i>
                    </button>';
        })->rawColumns(['project','status','action'])->make(true);

        return $data;
    }

    public function findById($id){
        $project = DB::select('select * from master_project where id = :id',['id' => $id]);
        return json_encode($project);
    }

    public function findTaskByProjId($id){
        $task = DB::select('SELECT task_name, status_id FROM master_task WHERE id_project = :id_project',['id_project' => $id]);
        return json_encode($task);
    }

    public function excel($id){
        //$fields=['noid'=>'No. ID','namalengkap'=>'Nama Siswa','kelamin'=>'Jenis Kelamin','agama'=>'Agama'];
         $fields=[];
         $query = \DB::select('SELECT mp.id,mp.project_name ,mt.task_name,mst.sub_task_name,mst.start_date,mst.target_date,mst.end_date,rp.definition,u.full_name
                    from master_project mp 
                    join master_task mt on mt.id_project=mp.id
                    join master_sub_task mst on mst.id_task=mt.id
                    join ref_progress_status rp on rp.id=mst.progress_id
                    JOIN users u ON mp.pm_id = u.id 
                    where mp.id=:id',['id'=>$id]);

        return MyFunc::printto($query,'Project: '.$query[0]->project_name, $fields, 0,['excel'=>'projects.excel'],'landscape','a4');
    }
    

       public function get_file($filename)
  {
        $file_path = public_path('support_docs') . "/" . $filename;
        return Response::download($file_path);
       // return json_encode($filename);
        //$path = storage_path($filename);

        //$headers = array('Content-Type' => File::mimeType($file_path));
        //$headers = array('Content-Type'=>'application/png');
        //return Response::download($file_path, 'logo.png',$headers);

        //return $img->response('jpg');

        //return response()->download($file_path, 'filename.png', $headers);
    
  }
  public function projectid($id){
      //  $branch_type = BranchType::all();
        $team = DB::table('master_project')
            ->where('id', '=',$id)
            ->where('close', '=',0)
            ->get();
        return json_encode($team);
    }

    public function cektask(Request $request, $id){
      
      $team = DB::table('master_task')
            ->where('id_project', '=',$id)
            ->get();
        return json_encode($team);

    }
        public function delete(Request $request, $id){

             try{
            $model = new ProjectModel();
            $model->deleteData($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            }else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }
        

    }


     public function generate($id,Request $request)
    {
       

         $param['task'] = \DB::select("select * from master_task where id_project=:id",['id'=>$id]);
         $param['model'] = \DB::select("select * from ref_model where status=1");


        if ($request->ajax()) {
            $view = view('projects.generate',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'projects.generate',$param);
    }


    public function insertGenerate(Request $request)
    {
        
        //var_dump($request->all());
if(!empty($request->idtask)){

       $idproject=$request->input('id_project');
       $project = DB::table('master_project')
            ->where('id', '=',$idproject)
            ->first();


       $idtask=$request->idtask;
       $targetdate1=$request->target_date1;
       $picid1=$request->input('pic_id');
       $sub_task_level_id1=$request->input('sub_task_level_id');


       $idsubtask=$request->idsubtask;
       $targetdate=$request->target_date;
      // $picid=$request->pic_id;
       //$sub_task_level_id=$request->sub_task_level_id;
        $no1=0;
        $no=0;
       foreach ($idtask as $item ) {
            if(!is_null($item)){

       $idsubtask=$request->input('idsubtask_'.$item);
        
            
            $reftask = DB::table('ref_task')
            ->where('id', '=',$item)
            ->first();

            $task = new TaskModel();
            $task->id_project = $idproject;
            $task->id_task = $item;
            $task->task_name = $reftask->definition;
            $task->start_date = $project->start_date;
            foreach ($targetdate1 as $tgl ) {
            if(!is_null($tgl)){
            $task->target_date = $tgl;
                }
            }
            $task->status_id = 2;
           // foreach ($picid1 as $pic ) {
            //if(!is_null($pic)){
            $task->pic_id = $picid1;
              //  }
            //}
            $task->task_type_id = $project->project_type_id;
            //foreach ($sub_task_level_id1 as $lvl ) {
            //if(!is_null($lvl)){
            $task->task_level_id = $sub_task_level_id1;
              //  }
            //}
            $task->quick_level_id = 1;
            $task->progress_id = 1;
            $task->rate_level_id = 0;
            $task->company_id = 1;
            $task->tanggal = date('Y-m-d');
            $task->progress_pct = 0;
            $task->duration = 0;
            $task->save();

            
            $taskmaster = DB::table('master_task')
            ->where('id_task', '=',$item)
            ->where('id_project', '=',$idproject)
            ->first();
            
        foreach ($idsubtask as $item1) {
             
             if(!is_null($item1)){
            
            $targetdate=$request->input('target_date_'.$item.'_'.$item1);
            $picid=$request->input('pic_id_'.$item.'_'.$item1);
            $sub_task_level_id=$request->input('sub_task_level_id_'.$item.'_'.$item1);       

            $refsubtask = DB::table('ref_sub_task')
            ->where('id', '=',$item1)
            ->first();

            $subtask = new SubtaskModel();
            $subtask->id_task = $taskmaster->id;
            $subtask->id_sub_task = $item1;
            $subtask->sub_task_name = $refsubtask->definition;
            $subtask->start_date = $project->start_date;
            
            $subtask->target_date =$targetdate;
            $subtask->status_id = 2;
            $subtask->pic_id = $picid1;
            $subtask->task_type_id = $project->project_type_id;
            $subtask->sub_task_level_id = $sub_task_level_id1;
            $subtask->quick_level_id = 1;
            $subtask->progress_id = 1;
            $subtask->rate_level_id = 0;
            $subtask->company_id = 1;
            $subtask->progress_pct = 0;
            $subtask->duration = 0;
            $subtask->save();
           
            $no++;

            }
        }





            $no1++;
            
            }
        }
        return $this->sendResponse(1,'Generate Berhasil', $project);

    }else{
        return $this->sendResponse(2,'gagal', 'a');
    }

    }


     public function insertClose(Request $request)
    {

        $query = \DB::select('select * from master_task where id_project=:id and progress_id not in(5) and status_id in (1,2)',['id'=>$request->input('idproject')]);
        
        if(empty($query)){


        $destination_path = public_path('support_docs');
        $files = $request->file('upload_doc');
        $filename = $files->getClientOriginalName();
        $upload_success = $files->move($destination_path, $filename);

        $project = ProjectModel::find($request->input('idproject'));
        $project->no_sk = $request->input('no_sk');
        $project->tgl_sk = $request->input('tgl_sk');
        $project->perihal_sk = $request->input('perihal_sk');
        $project->upload_doc = $filename;
        $project->duration = $request->input('duration');
        $project->end_date = $request->input('end_date');
        $project->close = 1;
        $project->save();

        return $this->sendResponse(1,'Close project Berhasil', $project);
        }else{

        return $this->sendResponse(2,'Gagal Close Project', $query);
        }    



    }

    public function open(Request $request,$id)
    {
         try{
            $model = new ProjectModel();
            $model->openproject($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            }else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }
    }

}
