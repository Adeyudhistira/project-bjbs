<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\User;
use App\MyFunc;
use App\ReffUser;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
class UserController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }


   public function index(Request $request)
    {
        if ($request->ajax()) {
            $view = view('user.index')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'user.index');
    }


    public function table(Request $request){
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY u.id) AS nomor_urut, u.id, u.full_name, u.user_name,ft.definition as team_id,u.status,u.password,r.name
            FROM users u 
            join ref_team ft on ft.id=u.team_id
            join model_has_roles mhr on mhr.model_id=u.id
            join roles r on r.id=mhr.role_id
            where u.status=1");

        $data = Datatables::of($query)->addColumn('action', function ($query){
            return "
            <i class='fa fa-pencil' style='color:blue;' title='Edit'  onclick='editshow(".$query->id.")'></i>
            <i style='color:red;' title='Hapus' onclick='hapus($query->id,\"user/delete\");' class='fa fa-trash'></i>
            <i style='color:green;' title='Reset' onclick=resetPassword(".$query->id."); class='fa fa-refresh'></i>";
            
        })->make(true);

        return $data;
    }

    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(),['full_name'=>'required','user_name'=>'required','team_id'=>'required']);
        if($validators->fails()){
            return $this->sendResponse('0','Input user gagal',$validators);
        }

        $query = DB::table('users')
            ->where('user_name', '=', $request->input('user_name'))
            ->first();

if(empty($query)){

        $destination_path = public_path('uploadfoto');
        $files = $request->file('photo');
        $filename = str_replace(' ', '', $files->getClientOriginalName());
        $upload_success = $files->move($destination_path, $filename);

        $user = new User();
        $user->full_name = $request->input('full_name');
        $user->user_name = $request->input('user_name');
        $user->team_id = $request->input('team_id');
        $user->status = 1;
        $user->photo = $filename;
        $user->password = bcrypt('123123');
        $user->save();

        $role = Role::findById($request->input('role_id'));
        $user->assignRole($role);



       return $this->sendResponse('1','Input user berhasil',$user);
    }else{
       return $this->sendResponse('2','Username Sudah Ada',$query); 
    }
}
   
    public function resetPassword($id){
        $user = User::find($id);

        $user->password = bcrypt('123123');

        $user->save();
        return $this->sendResponse('1','Reset password berhasil',$user);
    }

    public function seed(Request $request){
        $query = \DB::select("select ROW_NUMBER() OVER (ORDER BY id) AS nomor_urut,id,name,comp_id,note,status
FROM branch_type order by id asc" );

        $data = Datatables::of($query)->addColumn('action', function ($query){
            return "<div class='btn btn-primary' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i> Edit</div>
                    <div class='btn btn-danger' onclick=doDelete($query->id,'branchtype/delete');><i class='fa fa-trash'></i> Delete</div>
                    <div class='btn btn-info' onclick=resetPassword($query->id);><i class='fa fa-refresh'></i> Reset Password</div>";
        })->make(true);
        return $data;
    }

    public function getbranchtype($id){
        $query = DB::table('branch_type')
            ->select('id','name','note','comp_id','status')
            ->where('id', '=', $id)
            ->first();

        //$data = Datatables::of($query)->make(true);
        return \Response::json($query);
    }

    public function initUpdate($id,Request $request){
        try{
            $model = new BranchTypeModel();
            $model->saveData($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23505") {
                return response()->json([
                    'rc' => 1,
                    'rm' => 'Data sudah ada'
                ]);
            } else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }
        return view('master.master')->nest('child', 'branchtype.index');
    }

    public function delete(Request $request, $id){
         $st=DB::table('master_project')
            ->where('pm_id', '=',$id)
            ->where('status_id',1)
            ->first();

       if(empty($st)){
        $real_lm = DB::table('users')
            ->where('id', $id)
            ->update(['status' => 0]);

        return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
            }else{
           $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
                
            }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT id,full_name,user_name,team_id FROM users WHERE id =".$id);
        return json_encode($data);
    }

    public function update(Request $request){

        $validators = \Validator::make($request->all(),['level'=>'required','full_name'=>'required','user_name'=>'required','team_id'=>'required']);
        if($validators->fails()){
            return $this->sendResponse(0,'Update Gagal', $validators);
        }

      //  var_dump($request->all());

        $user = User::find($request->input('id'));
        $user->full_name = $request->input('full_name');
        $user->user_name = $request->input('user_name');
        $user->team_id = $request->input('team_id');
        $user->status = 1;
        $user->password = bcrypt('123123');
        $user->save();


        return $this->sendResponse(1,'Berhasil Diupdate', $user);
    }

    public function test(){
        $before = "1.000.000";
        $after = str_replace(".","",$before);
        return $before." ".$after;
    }

     
    public function update1(Request $request){
       /*
       $validators = \Validator::make($request->all(),['passwordnew'=>'required','cpasswordnew'=>'required|same:passwordnew','oldpassword'=>'required']);
        if($validators->fails()){
            return $this->sendResponse(0,'Update Gagal', $validators);
        }
      */
  if (!(Hash::check($request->input('oldpassword'), Auth::user()->password))) {
        $error = array('oldpassword' => 'Please enter correct current password');
        //return response()->json(array('error' => $error), 400);   
        return Redirect::back()->withErrors($error);
    //return $this->sendResponse(2,'Your current password does not matches with the password you provided. Please try again.');
        
        }elseif ($request->input('passwordnew')!=$request->input('cpasswordnew')) {
            $error = array('cpasswordnew' => 'The Confirm Password New and Password new must match!');
            return Redirect::back()->withErrors($error);
            //return response()->json(array('error' => $error), 400);
        }else{
        $real_lm = ReffUser::find($request->input('id'));
        //$real_lm ->user_name= $request->input('user_name');
        $real_lm ->password= bcrypt($request->input('passwordnew'));
        $real_lm->save();
        //$this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');

        //Auth->middleware('guest')->except('logout');
        //return redirect()->to('login');
        //return view('auth.login');
        //return $this->sendResponse(1,'Password changed successfully !', $real_lm);  

        }
        
    

    }

     public function pic(){
      //  $branch_type = BranchType::all();
        $team = DB::table('users')
            ->where('status', '=',1)
            //->where('team_id', '=',Auth::User()->team_id)
            ->get();
        return json_encode($team);
    }

    public function picid($id){
      //  $branch_type = BranchType::all();
        $team = DB::table('users')
            ->where('status', '=',1)
            ->join('model_has_roles','model_has_roles.model_id','=','users.id')
            ->where('model_has_roles.role_id','=',2)
            ->where('team_id', '=',$id)
            ->get();
        return json_encode($team);
    }


      public function picteam($id){
      //  $branch_type = BranchType::all();
        $projectteam = DB::table('master_project')
            ->where('id', '=',$id)
            ->first();

        $team = DB::table('users')
            ->where('status', '=',1)
            ->join('model_has_roles','model_has_roles.model_id','=','users.id')
            ->where('model_has_roles.role_id','=',2)
            ->where('team_id', '=',$projectteam->team_id)
            ->get();

        return json_encode($team);
    }


    public function detailtask($id,Request $request){

        $param['user'] = \DB::select("select u.id,u.full_name,u.photo,u.status,u.phone,u.status,u.email,rt.definition from users u join ref_team rt on rt.id=u.team_id where u.id=:id",['id'=>$id]);

        $param['task'] = \DB::select("select mst.sub_task_name,mst.duration,ps.definition as progress,rs.definition as status from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
            where u.id=:id and mst.progress_id in (2,5)",['id'=>$id]);

         $param['project'] = \DB::select("select DISTINCT on (pm.id) pm.id,project_name,mt.pic_id from master_project pm
            join master_task mt on mt.id_project=pm.id
            where mt.pic_id=:id",['id'=>$id]);

        $param['done'] = \DB::select("select mst.sub_task_name,mst.duration,ps.definition as progress,rs.definition as status from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
            where u.id=:id and mst.progress_id =5",['id'=>$id]);

        $param['onprogress'] = \DB::select("select mst.sub_task_name,mst.duration,ps.definition as progress,rs.definition as status from master_sub_task mst
            join users u on u.id=mst.pic_id
            join ref_progress_status ps on ps.id=mst.progress_id 
            join ref_status rs on rs.id=mst.status_id
            where u.id=:id and mst.progress_id =2",['id'=>$id]);

         if ($request->ajax()) {
            $view = view('user.detail',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'user.detail',$param);
    }


     public function excel($id){
        //$fields=['noid'=>'No. ID','namalengkap'=>'Nama Siswa','kelamin'=>'Jenis Kelamin','agama'=>'Agama'];
         $fields=[];
         $query = \DB::select('SELECT mp.project_name ,mt.task_name,mst.sub_task_name,mst.start_date,mst.target_date,mst.end_date,rp.definition,u.full_name
                    from master_project mp 
                    join master_task mt on mt.id_project=mp.id
                    join master_sub_task mst on mst.id_task=mt.id
                    join ref_progress_status rp on rp.id=mst.progress_id
                    JOIN users u ON mst.pic_id = u.id 
                    where u.id=:id and mst.progress_id in (2,5)',['id'=>$id]);

        return MyFunc::printto($query,'Detail Task: '.$query[0]->full_name, $fields, 0,['excel'=>'user.excel'],'landscape','a4');
    }

     public function change(Request $request)
    {
        
        $param['user'] = \DB::select("select id from users where id=:id",['id'=>Auth::user()->id]);
        if ($request->ajax()) {
            $view = view('user.changepassword',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'user.changepassword',$param);
    }

}