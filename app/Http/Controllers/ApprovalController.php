<?php

namespace App\Http\Controllers;

use App\ProjectModel;
use App\SubtaskModel;
use App\TaskModel;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Response;
use DB;
use Hash;
use Auth;

class ApprovalController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        if(Auth::user()->hasRole('Admin')){
            $param['project'] = \DB::select("SELECT DISTINCT on(mp.project_name) mp.project_name,mp.id,u.full_name pic
                            FROM master_project mp JOIN master_task mt ON mp.id = mt.id_project
                             JOIN master_sub_task mst ON mst.id_task = mt.id
                           left JOIN users u ON mt.pic_id = u.id where mp.status_id in (1)");
        }else{
            $param['project'] = \DB::select("SELECT DISTINCT on(mp.project_name) mp.project_name,mp.id,u.full_name pic
                            FROM master_project mp JOIN master_task mt ON mp.id = mt.id_project
                             JOIN master_sub_task mst ON mst.id_task = mt.id
                            left JOIN users u ON mt.pic_id = u.id where mp.status_id in (1) and mp.team_id = :team_id",["team_id" => Auth::user()->team_id]);
        }

       
        $param['id']=null;
        if ($request->ajax()) {
            $view = view('approve.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approve.index',$param);
    }

    public function task($id,Request $request)
    {

        if(Auth::user()->hasRole('Admin')){
            $param['project'] = \DB::select("SELECT DISTINCT on(mp.project_name) mp.project_name,mp.id,u.full_name pic
                            FROM master_project mp JOIN master_task mt ON mp.id = mt.id_project
                             JOIN master_sub_task mst ON mst.id_task = mt.id
                            left JOIN users u ON mt.pic_id = u.id where mp.status_id in (1)");
        }else{
            $param['project'] = \DB::select("SELECT DISTINCT on(mp.project_name) mp.project_name,mp.id,u.full_name pic
                            FROM master_project mp JOIN master_task mt ON mp.id = mt.id_project
                             JOIN master_sub_task mst ON mst.id_task = mt.id
                            left JOIN users u ON mt.pic_id = u.id where mp.status_id in (1) and mp.team_id = :team_id",["team_id" => Auth::user()->team_id]);
        }

        $param['id']=$id;
        $param['task'] = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY mst.id) AS id, mst.id mstid, mt.task_name ,mst.sub_task_name, mst.start_date, mst.end_date, mst.target_date, mst.duration, mst.url_doc,u.full_name pic
            FROM master_project mp JOIN master_task mt ON mp.id = mt.id_project
              JOIN master_sub_task mst ON mst.id_task = mt.id
            JOIN users u ON mst.pic_id = u.id
            where mt.id_project=:id and mst.is_approve = false AND mst.progress_id = 5 and mst.status_id in (1,2)",['id'=>$id]);

        if ($request->ajax()) {
            $view = view('approve.index2',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approve.index2',$param);
    }



    public function table(){
        $query = DB::select('SELECT ROW_NUMBER() OVER (ORDER BY mst.id) AS id, mst.id mstid, mp.project_name, mt.task_name ,mst.sub_task_name, mt.start_date, mt.end_date, mst.target_date, mt.duration, u.full_name pic
FROM master_project mp JOIN master_task mt ON mp.id = mt.id_project
  JOIN master_sub_task mst ON mst.id_task = mt.id
JOIN users u ON mt.pic_id = u.id');

        $data = Datatables::of($query)->addColumn('action', function ($query){
            return '<button type="button" class="btn btn-info bg-blue" data-toggle="tooltip" data-placement="top" title="" data-original-title="Approve" onclick="rating('.$query->mstid.','. $query->target_date .')">
                        <i class="fa fa-check"></i>
                    </button>
                    <button type="button" class="btn btn-danger bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Return" onclick="editshow('.$query->mstid.')">
                        <i class="fa fa-times"></i>
                    </button>';
        })->make(true);
        return $data;
    }

    public function rate(Request $request){
        $rate = $request->input('rate');
        $id_subtask = $request->input('id_subtask');
        $enddate = $request->input('end_date');
        $durasi = $request->input('duration');
       
        $subtask = SubtaskModel::find($id_subtask);
        if($subtask->end_date < $subtask->target_date){
            $quick_level = 5;
        }else{
            $date_diff = $this->weekday_diff($subtask->target_date,$subtask->end_date)-1;
            switch ($date_diff){
                case 0:
                    $quick_level = 4;
                    break;
                case 1:
                    $quick_level = 3;
                    break;
                case 2:
                    $quick_level = 2;
                    break;
                default:
                    $quick_level = 1;
                    break;
            }
        }

        $current_date = date('Y-m-d');

        $count_all_subtask = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->whereIn('status_id',[1,2])
            ->where('id_task', '=',$subtask->id_task)
            ->first();

        $count_done_subtask = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->where('progress_id', '=',5)
            ->whereIn('status_id',[1,2])
            ->whereNotNull('end_date')
            ->where('id_task', '=',$subtask->id_task)
            ->first();

        if($count_all_subtask->nilai > 0){
            $default = 100;
            $progress_pct = ($default/$count_all_subtask->nilai) * $count_done_subtask->nilai;

            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_pct' => $progress_pct]);
        }

        $project_id = DB::table('master_task')
            ->select('id_project')
            ->where('id', '=',$subtask->id_task)
            ->first();

        $all = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task','master_task.id','=','master_sub_task.id_task')
            ->whereIn('master_sub_task.status_id',[1,2])
            ->where('master_task.id_project', '=',$project_id->id_project)
            ->first();

        $done = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task','master_task.id','=','master_sub_task.id_task')
            ->where('master_sub_task.progress_id', '=',5)
            ->whereIn('master_sub_task.status_id',[1,2])
            ->whereNotNull('master_sub_task.end_date')
            ->where('master_task.id_project', '=',$project_id->id_project)
            ->first();

        if($all->nilai>0){
            $default = 100;
            $progress_pct = ($default/$all->nilai) * $done->nilai;
            DB::table('master_project')
                ->where('id', $project_id->id_project)
                ->update(['progress_pct' => $progress_pct]);

        }

        if($count_all_subtask->nilai == $count_done_subtask->nilai){
            /*DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_id' => 5]);*/
            //current date - target project
            $task = TaskModel::find($subtask->id_task);

            $duration = $this->weekday_diff($current_date,$task->target_date);
            $task->duration = $duration;
            $task->end_date = $current_date;
            $task->progress_id = 5;
            $task->save();

        }else{
            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_id' => 2]);
        }

        $alltask = DB::table('master_task')
            ->select(DB::raw('count(*) as nilai'))
            ->whereIn('progress_id',[2,5])
            ->where('id_project','=', $project_id->id_project)->first();
        $alltaskdone = DB::table('master_task')
            ->select(DB::raw('count(*) as nilai'))
            ->where('progress_id','=',5)
            ->where('id_project','=', $project_id->id_project)->first();

        if($alltask->nilai == $alltaskdone->nilai){
            $project = ProjectModel::find($project_id->id_project);
            $duration = $this->weekday_diff($current_date,$project->target_date);

            $project->duration = $duration;
            $project->end_date = $current_date;
            $project->progress_id = 5;

            $project->save();
        }

        $subtask = SubtaskModel::find($id_subtask);
        $subtask->rate_level_id = $rate;
        $subtask->quick_level_id = $quick_level;
        $subtask->is_approve = true;
        $subtask->duration = $durasi;
        $subtask->end_date = $enddate;
        $subtask->save();

        $data = \DB::select("SELECT  id_task
        FROM master_sub_task WHERE id =" . $id_subtask ."limit 1");

        $seq=\DB::select("SELECT  id,progress_id
        FROM master_sub_task WHERE id >" . $id_subtask . "and id_task=".$data[0]->id_task."limit 1");    

        if(!empty($seq)){
        $subtask = SubtaskModel::find($seq[0]->id);
        $subtask->start_date = $enddate;
        $subtask->save();
        }

        return $this->sendResponse(1,'Berhasil Rate',$subtask);
    }

    public function test(){

         //return $this->weekday_diff($a,$b)-1;
    }

    public function rollback(Request $request){
         $id_subtask = $request->input('id_subtask_return');
         $note = $request->input('note');
         $subtask = SubtaskModel::find($id_subtask);
         $subtask->progress_pct = 0;
         $subtask->progress_id = 2;
         $subtask->is_approve = false;
         $subtask->duration = 0;
         $subtask->end_date = null;
         $subtask->save();

        $count_all_subtask = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->whereIn('status_id', [1, 2])
            ->where('id_task', '=', $subtask->id_task)
            ->first();

        $count_done_subtask = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->where('progress_id', '=', 5)
            ->whereIn('status_id', [1, 2])
            ->whereNotNull('end_date')
            ->where('id_task', '=', $subtask->id_task)
            ->first();

        if($count_all_subtask->nilai > 0){
            $progress_pct = (100/$count_all_subtask->nilai) * $count_done_subtask->nilai;

            DB::table('master_task')
                ->where('id', $request->input('id_task'))
                ->update(['progress_pct' => $progress_pct]);
        }else{
            DB::table('master_task')
                ->where('id', $request->input('id_task'))
                ->update(['progress_pct' => 0]);
        }

        $project_id = DB::table('master_task')
            ->select('id_project')
            ->where('id', '=',$subtask->id_task)
            ->first();

        $all = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task','master_task.id','=','master_sub_task.id_task')
            ->whereIn('master_sub_task.status_id',[1,2])
            ->where('master_task.id_project', '=',$project_id->id_project)
            ->first();

        $done = DB::table('master_sub_task')
            ->select(DB::raw('count(*) as nilai'))
            ->join('master_task','master_task.id','=','master_sub_task.id_task')
            ->where('master_sub_task.progress_id', '=',5)
            ->whereIn('master_sub_task.status_id',[1,2])
            ->whereNotNull('master_sub_task.end_date')
            ->where('master_task.id_project', '=',$project_id->id_project)
            ->first();

        if($all->nilai>0){
            $default = 100;
            $progress_pct = ($default/$all->nilai) * $done->nilai;
            DB::table('master_project')
                ->where('id', $project_id->id_project)
                ->update(['progress_pct' => $progress_pct]);

        }else{
            DB::table('master_project')
                ->where('id', $project_id->id_project)
                ->update(['progress_pct' => 0]);
        }

        if($count_all_subtask->nilai == $count_done_subtask->nilai){
            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_id' => 5]);
        }else{
            DB::table('master_task')
                ->where('id', $subtask->id_task)
                ->update(['progress_id' => 2]);
        }

         return $this->sendResponse(1,'Berhasil return', $subtask);
    }

    public function findByIdSub(){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
