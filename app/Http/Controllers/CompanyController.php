<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\CompanyModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
class CompanyController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','checkrole']);
    }


   public function index(Request $request)
    {
        if ($request->ajax()) {
            $view = view('company.index')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'company.index');
    }


    public function table(Request $request){
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY id) AS nomor_urut, id, company_name, address,phone,city,province,seq,status
            FROM ref_company where status=1");

        $data = Datatables::of($query)->addColumn('action', function ($query){
            return
                '<button type="button" class="btn btn-info bg-blue" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" onclick="editshow('.$query->id.')">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-danger bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus" onclick=hapus('.$query->id.',"company/delete")>
                        <i class="fa fa-trash"></i>
                    </button>';
            
        })->make(true);

        return $data;
    }

    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(),['company_name'=>'required','address'=>'required','phone'=>'required','city'=>'required','province'=>'required','seq'=>'required']);
        if($validators->fails()){
            return $this->sendResponse('0','Input Company gagal',$validators);
        }
        
        $Company = new CompanyModel();
        $Company->company_name = $request->input('company_name');
        $Company->address = $request->input('address');
        $Company->phone = $request->input('phone');
        $Company->city = $request->input('city');
        $Company->province = $request->input('province');
        $Company->seq = $request->input('seq');
        $Company->company_code = 'bjbs01';
        $Company->status = 1;
        $Company->save();
       return $this->sendResponse('1','Input Company berhasil',$Company);
    }
   

    public function delete(Request $request, $id){
        try{
            $model = new CompanyModel();
            $model->deleteData($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            }else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT  id, company_name, address,phone,city,province,seq,status
            FROM ref_company WHERE id =".$id);
        return json_encode($data);
    }

    public function update(Request $request){

        $validators = \Validator::make($request->all(),['company_name'=>'required','address'=>'required','phone'=>'required','city'=>'required','province'=>'required','seq'=>'required']);
        if($validators->fails()){
            return $this->sendResponse(0,'Update Gagal', $validators);
        }

      //  var_dump($request->all());

        $Company = CompanyModel::find($request->input('id'));
        $Company->company_name = $request->input('company_name');
        $Company->address = $request->input('address');
        $Company->phone = $request->input('phone');
        $Company->city = $request->input('city');
        $Company->province = $request->input('province');
        $Company->seq = $request->input('seq');
        $Company->company_code = 'bjbs01';
        $Company->status = 1;
        $Company->save();


        return $this->sendResponse(1,'Berhasil Diupdate', $Company);
    }

   

}