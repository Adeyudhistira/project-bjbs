<?php

namespace App;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class RefTaskModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'ref_task';

     public function deleteData(Request $request, $id){
       // $bean = $this->find($id);
        //$bean->delete($id);
        $real_lm = $this->find($id);
        $real_lm->status= 0;
        $real_lm->save();
    }
}