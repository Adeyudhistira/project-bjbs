<?php

namespace App;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class TaskModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'master_task';

     public function deleteData(Request $request, $id){
       // $bean = $this->find($id);
        //$bean->delete($id);
        $real_lm = $this->find($id);
        $real_lm->status_id= 3;
        $real_lm->save();


        $data = \DB::select("SELECT  * FROM master_sub_task WHERE id_task =".$id);
        if(!empty($data)){
        	foreach ($data as $key) {
        	DB::table('master_sub_task')
            ->where('id', $key->id)
            ->update(['status_id' => 3]);
        	}
        }
    }
}