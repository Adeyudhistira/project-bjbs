<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
  Route::get('/home','HomeController@index')->name('home');
  Route::get( '/', ['as'=>'home', 'uses'=> 'HomeController@index']);

  Route::prefix('user')->group(function (){
        Route::get('table','UserController@table');
        Route::get('edit/{id}','UserController@findRealById');
        Route::post('update', 'UserController@update');
        Route::get('delete/{id}', 'UserController@delete');
        Route::get('detailtask/{id}', 'UserController@detailtask');
        Route::get('change', 'UserController@change');
    });
    Route::get('user/reset/{id}', 'UserController@resetPassword');

    Route::prefix('client')->group(function (){
        Route::get('table','ClientController@table');
        Route::get('edit/{id}','ClientController@findRealById');
        Route::post('update', 'ClientController@update');
        Route::get('delete/{id}', 'ClientController@delete');
    });

    Route::prefix('company')->group(function (){
        Route::get('table','CompanyController@table');
        Route::get('edit/{id}','CompanyController@findRealById');
        Route::post('update', 'CompanyController@update');
        Route::get('delete/{id}', 'CompanyController@delete');
    });

    Route::prefix('paralel')->group(function (){
        Route::get('table','ParalelController@table');
        Route::get('edit/{id}','ParalelController@findRealById');
        Route::post('update', 'ParalelController@update');
        Route::get('delete/{id}', 'ParalelController@delete');
    });

    Route::prefix('performance')->group(function (){
        Route::get('table','PerformanceController@table');
        Route::get('edit/{id}','PerformanceController@findRealById');
        Route::post('update', 'PerformanceController@update');
        Route::get('delete/{id}', 'PerformanceController@delete');
    });

    Route::prefix('progressstatus')->group(function (){
        Route::get('table','ProgressstatusController@table');
        Route::get('edit/{id}','ProgressstatusController@findRealById');
        Route::post('update', 'ProgressstatusController@update');
        Route::get('delete/{id}', 'ProgressstatusController@delete');
    });

    Route::prefix('projecttype')->group(function (){
        Route::get('table','ProjecttypeController@table');
        Route::get('edit/{id}','ProjecttypeController@findRealById');
        Route::post('update', 'ProjecttypeController@update');
        Route::get('delete/{id}', 'ProjecttypeController@delete');
    });

    Route::prefix('quicklevel')->group(function (){
        Route::get('table','QuicklevelController@table');
        Route::get('edit/{id}','QuicklevelController@findRealById');
        Route::post('update', 'QuicklevelController@update');
        Route::get('delete/{id}', 'QuicklevelController@delete');
    });

    Route::prefix('status')->group(function (){
        Route::get('table','StatusController@table');
        Route::get('edit/{id}','StatusController@findRealById');
        Route::post('update', 'StatusController@update');
        Route::get('delete/{id}', 'StatusController@delete');
    });

    Route::prefix('tasklevel')->group(function (){
        Route::get('table','TasklevelController@table');
        Route::get('edit/{id}','TasklevelController@findRealById');
        Route::post('update', 'TasklevelController@update');
        Route::get('delete/{id}', 'TasklevelController@delete');
    });

    Route::prefix('team')->group(function (){
        Route::get('table','TeamController@table');
        Route::get('edit/{id}','TeamController@findRealById');
        Route::post('update', 'TeamController@update');
        Route::get('delete/{id}', 'TeamController@delete');
        Route::get('detailtask/{id}', 'TeamController@detailtask');
    });

     Route::prefix('workload')->group(function (){
        Route::get('table','WorkloadController@table');
        Route::get('edit/{id}','WorkloadController@findRealById');
        Route::post('update', 'WorkloadController@update');
        Route::get('delete/{id}', 'WorkloadController@delete');
    });

    Route::prefix('task')->group(function (){
        Route::get('index','TaskController@index');
        Route::get('index/{id}','TaskController@index');
        Route::get('table','TaskController@table');
        Route::get('edit/{id}','TaskController@findRealById');
        Route::post('update', 'TaskController@update');
        Route::get('delete/{id}', 'TaskController@delete');
        Route::get('seq/{id}','TaskController@findseq');
        Route::get('detail/{id}','TaskController@detail');
    });

    Route::prefix('subtask')->group(function (){
        Route::get('table/{id}','SubtaskController@table');
        Route::get('edit/{id}','SubtaskController@findRealById');
        Route::post('update', 'SubtaskController@update');
        Route::get('delete/{id}', 'SubtaskController@delete');
        Route::get('seq/{id}','SubtaskController@findseq');

    });


    Route::prefix('refTask')->group(function (){
        Route::get('table','RefTaskController@table');
        Route::get('edit/{id}','RefTaskController@findRealById');
        Route::post('update', 'RefTaskController@update');
        Route::get('delete/{id}', 'RefTaskController@delete');
    });

     Route::prefix('model')->group(function (){
        Route::get('table','ModelController@table');
        Route::get('edit/{id}','ModelController@findRealById');
        Route::post('update', 'ModelController@update');
        Route::get('delete/{id}', 'ModelController@delete');
    });

    Route::prefix('refSubtask')->group(function (){
        Route::get('table','RefSubtaskController@table');
        Route::get('edit/{id}','RefSubtaskController@findRealById');
        Route::post('update', 'RefSubtaskController@update');
        Route::get('delete/{id}', 'RefSubtaskController@delete');
    });

    Route::prefix('select')->group(function (){
        Route::get('team','TeamController@team');
        Route::get('teamid/{id}','TeamController@teamid');
        Route::get('teamid1','TeamController@teamid1');
        Route::get('client','ClientController@client');
        Route::get('project','ProjectController@project');
        Route::get('projectid/{id}','ProjectController@projectid');
        Route::get('status','StatusController@status');
        Route::get('user','UserController@pic');
        Route::get('userid/{id}','UserController@picid');
        Route::get('projecttype','ProjecttypeController@projecttype');
        Route::get('tasklevel','TasklevelController@tasklevel');
        Route::get('quicklevel','QuicklevelController@quicklevel');
        Route::get('progress','ProgressstatusController@progressstatus');
        Route::get('status1','StatusController@status1');
        Route::get('Reftask','RefTaskController@Reftask');
        Route::get('Reftask1/{id}','RefTaskController@Reftask1');
        Route::get('RefSubtask/{id}','RefSubtaskController@RefSubtask');
         Route::get('userteam/{id}','UserController@picteam');
         Route::get('model','ModelController@model');
         Route::get('taskid/{id}/{id1}','RefTaskController@taskid');

    });
 

    Route::prefix('projects')->group(function (){
        Route::get('table', 'ProjectController@table');
        Route::get('edit/{id}', 'ProjectController@findById');
        Route::post('update', 'ProjectController@update');
        Route::get('task/{id}','ProjectController@findTaskByProjId');
        Route::get('test', 'ProjectController@test');
        Route::get('detail/{id}','ProjectController@detail');
        Route::get('detailtask/{id}','ProjectController@detailtask');
        Route::get('delete/{id}', 'ProjectController@delete');
        Route::get('cektask/{id}', 'ProjectController@cektask');
        Route::get('generate/{id}','ProjectController@generate');
        Route::post('insertGenerate','ProjectController@insertGenerate');
        Route::post('insertClose','ProjectController@insertClose');
        Route::get('open/{id}', 'ProjectController@open');
    });

    Route::prefix('approve')->group(function (){
        Route::get('table', 'ApprovalController@table');
        Route::post('rate','ApprovalController@rate');
        Route::get('findByIdSub/id');
        Route::get('task/{id}','ApprovalController@task');
        Route::get('test','ApprovalController@test');
        Route::post('rollback','ApprovalController@rollback');
    });

    Route::prefix('chart')->group(function (){
        Route::get('jmlfinish', 'HomeController@jmlfinish');
        Route::get('jmlinactive','HomeController@jmlinactive');
        Route::get('jmlonprogress', 'HomeController@jmlonprogress');
        Route::get('jmlactive', 'HomeController@jmlactive');
        Route::get('jmlproject', 'HomeController@jmlproject');
        Route::get('tableproject', 'HomeController@tableproject');
        Route::get('task', 'HomeController@task');
    });

    Route::get('excel1', ['as'=>'excel1', 'uses'=> 'LaporanController@excel']); 
    Route::get('excel2',['as'=>'excel2', 'uses'=> 'LaporanController@excel1']);
    Route::get('excel3',['as'=>'excel3', 'uses'=> 'LaporanController@excel2']);
    Route::get('excel4',['as'=>'excel4', 'uses'=> 'LaporanController@excel3']);
    Route::get('pdf4',['as'=>'pdf4', 'uses'=> 'LaporanController@pdf3']);

    Route::get('excel5/{id}',['as'=>'excel5', 'uses'=> 'LaporanController@excel4']);
    Route::get('pdf5/{id}',['as'=>'pdf5', 'uses'=> 'LaporanController@pdf4']);

    Route::get('laporanperunit',['as'=>'laporanperunit', 'uses'=> 'LaporanController@laporanperunit']);
    Route::get('laporanperpic',['as'=>'laporanperpic', 'uses'=> 'LaporanController@laporanperpic']);
    Route::get('laporanproker',['as'=>'laporanproker', 'uses'=> 'LaporanController@laporanproker']);
    Route::get('laporandetail',['as'=>'laporandetail', 'uses'=> 'LaporanController@laporandetail']);
    
    Route::resource('model','ModelController');
    Route::resource('projects', 'ProjectController');
    Route::resource('user','UserController');
    Route::resource('client','ClientController');
    Route::resource('company','CompanyController');
    Route::resource('paralel','ParalelController');
    Route::resource('performance','PerformanceController');
    Route::resource('progressstatus','ProgressstatusController');
    Route::resource('projecttype','ProjecttypeController');
    Route::resource('quicklevel','QuicklevelController');
    Route::resource('status','StatusController');
    Route::resource('tasklevel','TasklevelController');
    Route::resource('team','TeamController');
    Route::resource('workload','WorkloadController');
    Route::resource('task','TaskController');
    Route::resource('subtask','SubtaskController');
    Route::get('download/{id}',['as'=>'download', 'uses'=> 'SubtaskController@get_file']);
    Route::get('downloadp/{id}',['as'=>'downloadp', 'uses'=> 'ProjectController@get_file']);
    Route::get('downloadtask/{id}',['as'=>'downloadtask', 'uses'=> 'TaskController@get_file']);
    Route::get('excel/{id}',['as'=>'excel', 'uses'=> 'ProjectController@excel']);
    Route::get('exceltask/{id}',['as'=>'exceltask', 'uses'=> 'UserController@excel']);
    Route::get('excelteam/{id}',['as'=>'excelteam', 'uses'=> 'TeamController@excel']);
    
    //Route::post('update1',['as'=>'update1', 'uses'=> 'UserController@update1']);
    Route::post('update1',['as'=>'update1', 'uses'=> 'UserController@update1'], function () {
    return redirect('logout')->with('status', 'Ganti Password Berhasil!');
    });
    Route::resource('refTask','RefTaskController');
    Route::resource('refSubtask','RefSubtaskController');
    Route::resource('approve','ApprovalController');
});  
Route::get('dashboard',['as'=>'dashboard', 'uses'=> 'DashboardController@dashboard']);




