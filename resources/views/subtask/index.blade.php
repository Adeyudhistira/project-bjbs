@section('content')
 <section class="row">
          <div class="col-12">
            <div class="page-title mb-4 d-flex align-items-center">
                <div class="choose-form-tab d-inline-block">
                <ul class="nav nav-form-custom nav-form-sm float-right" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        @role('SPV')
                        <button type="button" class="btn btn-info" data-toggle="modal"
                         onclick="tambah({{$task[0]->id}},{{$task[0]->id_task}},{{$task[0]->id_project}})">Tambah Sub Task</button>
                        @endrole
                    </li>
                </ul>
            </div>
        </div>
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Daftar Sub Task  <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'task')"><span class="text-bold-600">{{$task[0]->task_name}}</span></a></h4>
                <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <!-- Task List table -->
                  <div class="table-responsive">
                    <table id="t_user" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Pekerjaan</th>
                          <th>Dates</th>
                          <th>Status Terakhir</th>
                          <th>Progress</th>
                          <th>PIC</th>
                          <th>Dokumen Pendukung</th>
                          <th>Catatan</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>

                        @foreach($subtask as $data)
                         <tr>
                          <td>
                            {{$data->nomor_urut}}
                          </td>
                          <td>
                            <p class="text-muted"> {{$data->sub_task_name}}</p>
                          </td>
                          <td>
                             <h6 class="mb-0">
                              <span class="text-bold-600">{{$task[0]->task_name}}</span> on
                              <em>{{date('d-M-Y', strtotime($task[0]->created_at))}}</em>
                            </h6>
                          </td>
                          <td>
                            @if($data->progress_id==1)
                              <span class="badge badge-default badge-danger">{{$data->definition}}</span>
                            @elseif($data->progress_id==2)
                              <span class="badge badge-default badge-warning">{{$data->definition}}</span>
                            @else
                              <span class="badge badge-default badge-success">{{$data->definition}}</span>
                            @endif
                            
                          </td>
                          <td>
                            <div class="progress progress-sm">
                              <div aria-valuemin="{{$data->progress_pct}}" aria-valuemax="100" class="progress-bar bg-gradient-x-success"
                              role="progressbar" title="{{$data->progress_pct}}%" style="width:{{$data->progress_pct}}%" aria-valuenow="{{$data->progress_pct}}"></div>
                            </div>
                          </td>
                          <td class="text-center">
                            {{$data->full_name}}
                          </td>
                          <td class="text-center">
                            <a href="{{route('download',$data->url_doc)}}" title="{{$data->url_doc}}">{{$data->url_doc}}</a>
                          </td>
                          <td>
                            {{$data->notes}}
                          </td>
                          <td>
                              @role('Admin')
                              <i class='fa fa-pencil' style='color:blue;' title='Edit'  onclick='editshow({{$data->id}},{{$task[0]->is_parallel}})'></i>
                              @endrole
                              @role('SPV')
                              <i style='color:red;' title='Hapus' onclick="hapus({{$data->id}},base_url+'/subtask/delete');" class='fa fa-trash'></i>
                              @endrole
                          </td>
                        </tr>
                  @endforeach
                      </tbody>
                    </table>
                  </div>
                  <!--/ Task List table -->
                </div>
              </div>
            </div>
          </div>
        </section>
@include('subtask.add')
@include('subtask.edit')
@include('subtask.addsubtask')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('assets/js/_subtask.js') }}"></script>

@endsection