 <div class="modal animated slideInRight text-left" id="_create" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-lg modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-blue">
                                  <h4 class="modal-title white" id="myModalLabel76">Tambah Sub Task</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <div id="subtask" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:40%;padding:2px;"><img src="{{asset('images/loader.gif')}}" width="110" height="110" /></div>
                                   <form id="_create_form">
                                      <table width="100%">
                                        <tr>
                                          <td>
                                            <table width="100%">
                                              <tr>
                                                <td>
                                                  <div class="form-group">
                                                    <label for="message-text" class="col-form-label">Nama Sub Task</label>
                                                    
                                                    <select name="id_sub_task" class="select2 form-control" data-parsley-required id="id_sub_task">
                                                       
                                                    </select>
                                                    <input type="hidden" class="form-control" id="id_task" name="id_task" data-parsley-required>
                                                    <input type="hidden" class="form-control" id="id_task1" name="id_task1" data-parsley-required>
                                                    <input type="hidden" class="form-control" id="sub_task_name" name="sub_task_name" data-parsley-required>
                                                </div>
                                                </td>
                                              </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="message-text" class="col-form-label">Catatan</label>
                                                            <textarea class="form-control" name="note" id="note"></textarea>
                                                        </div>
                                                    </td>
                                                </tr>

                                              <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label for="message-text" class="col-form-label">Tanggal Mulai</label>
                                                      <input type="date"  class="form-control" id="start_date" name="start_date" data-parsley-required>
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label for="message-text" class="col-form-label">Tanggal Target</label>
                                                      <input type="date" class="form-control" id="target_date" name="target_date" data-parsley-required>
                                                  </div>
                                                </td>
                                              </tr>

                                            </table>
                                          </td>
                                          <td>
                                            <table width="100%">
                                                <tr valign="top">
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <div style="background-color: #6495ED; width: 0px;  height: 400px; border: 0.5px #6495ED solid;">
                                                        </div>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                          <td valign="top">
                                            <table width="100%">
                                              <tr>
                                                <td>
                                                  <div class="form-group">
                                                      <label for="message-text" class="col-form-label">PIC</label>
                                                      
                                                      <select name="pic_id" class="select2 form-control" data-parsley-required id="pic_id">
                                                         
                                                      </select>
                                                 
                                                  </div>
                                                </td>
                                              </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="message-text" class="col-form-label">Dokumen Pendukung</label>
                                                            <input type="file" class="form-control" name="file"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                               
                                               <tr>
                                                <td>
                                                   <div class="form-group">
                                                        <label for="message-text" class="col-form-label">Level Task</label>
                                                        
                                                        <select name="tasklevel_id" class="select2 form-control" data-parsley-required id="tasklevel_id">
                                                           
                                                        </select>
                                                   
                                                    </div>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-outline-info"  onclick="processInsert()">Save</button>
                                </div>
                              </div>
                            </div>
                          </div>
                      