 <div class="modal animated slideInRight text-left" id="_edit" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-blue">
                                  <h4 class="modal-title white" id="myModalLabel76">Edit Sub Task</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <div id="subtasku" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:40%;padding:2px;"><img src="{{asset('images/loader.gif')}}" width="110" height="110" /></div>
                                   <form id="form_edit_lm"  name="formEdit" role="form" class="form-validation" data-parsley-validate="" enctype="multipart/form-data">
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Nama Sub Task</label>

                                          <input type="text" readonly class="form-control" id="sub_task_name1" name="sub_task_name" data-parsley-required>
                                          <input type="hidden" class="form-control" id="id" name="id" data-parsley-required>
                                          <input type="hidden" class="form-control" id="idtask" name="id_task">
                                          <input type="hidden" class="form-control" id="end_date1" name="end_date" data-parsley-required>
                                          <input type="hidden" class="form-control" id="duration1" name="duration" data-parsley-required>
                                      </div>
                                      @php
                                       $team = DB::table('ref_progress_status')
                                        ->where('status', '=',1)
                                        ->WhereIn('id',[2,5])
                                        ->get();
                                      @endphp
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Progress Task</label>
                                          
                                          <select style="width:470px" name="progressstatus_id" class=" form-control" data-parsley-required id="progressstatus_id1">
                                             <option value="">Pilih Progress</option>
                                             @foreach($team as $item)
                                             <option value="{{$item->id}}">{{$item->definition}}</option>
                                             @endforeach
                                          </select>
                                     
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Status Task</label>
                                          
                                          <select style="width:470px" name="status_id" class=" form-control" data-parsley-required id="status_id1">
                                             
                                          </select>
                                     
                                      </div>
                                      <div class="form-group">
                                        <label for="message-text" class="col-form-label">Dokumen Pendukung</label>
                                      
                                          <input type="file" required data-validation-required-message="Tidak Boleh Kosong"  class="form-control-file" id="url_doc1" name="url_doc">
                                        
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Keterangan</label>
                                         
                                         <textarea class="form-control" id="notes1" name="notes" rows="3"></textarea>
                                      </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-outline-info"  onclick="processUpdate()">Update</button>
                                </div>
                              </div>
                            </div>
                          </div>