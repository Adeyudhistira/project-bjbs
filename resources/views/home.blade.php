@section('content')
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-blue">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                &nbsp;
                                                <i class="ft-layers font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'task/detail/all')">
                                                <span class="font-large-2 text-bold-300 white float-right"
                                                      id="jmlproject"></span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">Total Project</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-red">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="ft-pause font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'task/detail/inactive')">
                                                <span class="font-large-2 text-bold-300 white float-right"
                                                      id="jmlinactive"></span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">InActive Project</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-warning">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="ft-clock font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'task/detail/onprogress')">
                                                <span class="font-large-2 text-bold-300 white float-right"
                                                      id="jmlonprogress"></span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">On Progress Project</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-green">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="icon-check font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'task/detail/finish')">
                                                <span class="font-large-2 text-bold-300 white float-right"
                                                      id="jmlfinish">5</span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">Finished Project</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        @php
        $query= \DB::select('select * from ref_team where status=1'); 
        @endphp

        @foreach($query as $item)
        @php
        $waitApproval = DB::table('master_project')->select(DB::raw('count(*) as jml'))
             ->leftJoin('users','users.id','=','master_project.pm_id')
             //->where('is_approve','=', false)
             //->where('progress_id','=', 5)
             ->where('master_project.team_id','=', $item->id)
             ->where('status_id',2)
             ->first();

        $onprogress = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            ->leftJoin('users','users.id','=','master_project.pm_id')
            ->whereIn('status_id',[1,2])
            ->where('master_project.team_id','=', $item->id)
            ->first();

        $finish = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            ->leftJoin('users','users.id','=','master_project.pm_id')
            ->where('progress_id',2)
            ->where('master_project.team_id','=', $item->id)
            ->where('status_id',1)
            //->where('is_approve' ,'=', true)
            ->first();

        $jmltask = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            ->leftJoin('users','users.id','=','master_project.pm_id')
            ->where('master_project.team_id','=', $item->id)
            ->where('status_id',1)
            ->where('progress_id',5)
            ->first();

        @endphp

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header bg-blue">
                  <h4 class="card-title text-white">{{$item->definition}}</h4>
                    <a class="heading-elements-toggle"><i class="ft-more-horizontal font-medium-3"></i></a>
                </div>
                  <div class="card-content">
                      
                      <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-blue">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="ft-layers font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" >
                                                <span class="font-large-2 text-bold-300 white float-right"
                                                      >{{$onprogress->jml}}</span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">Total Project</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-red">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="ft-pause font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" >
                                                <span class="font-large-2 text-bold-300 white default float-right"
                                                      >{{$waitApproval->jml}}</span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">InActive Project</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-warning">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="ft-clock font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" >
                                                <span class="font-large-2 text-bold-300  white float-right"
                                                      >{{$finish->jml }}</span>
                                                 </a>     
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">On Progress project</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 bg-green">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="icon-check font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" >
                                                <span class="font-large-2 text-bold-300 white float-right"
                                                      >{{$jmltask->jml}}</span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">Finished Project</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            
                  </div>
                </div>
              </div>
            </div>
          @endforeach  





            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-blue">
                            <h4 class="card-title text-white">Ongoing Projects</h4>
                            <a class="heading-elements-toggle"><i class="ft-more-horizontal font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <p class="m-0">Total ongoing projects {{ count($project__) }}
                                    <span class="float-right"><a href="#" onclick="ajaxMenu(event,'projects')"
                                                                 target="_blank">Project Summary <i
                                                    class="ft-arrow-right"></i></a></span>

                                </p>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover table-striped mb-0">
                                    <thead>
                                    <tr style="background-color: #5F9EA0;color: white;">
                                        <th>Project</th>
                                        <th>PIC</th>
                                        <th>Project Type</th>
                                        <th>Progress</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($project as $dt)
                                        <tr>
                                            <td class="text-truncate"><a href="#" class="text-bold-600" onclick="ajaxMenu(event,'projects/detailtask/{{$dt->id}}')">{{$dt->project_name}}</a></td>
                                            <td class="text-truncate">

                                                <span>{{$dt->full_name}}.</span>
                                            </td>
                                            <td class="text-truncate">
                                                <span class="tag tag-success">{{{$dt->definition}}}</span>
                                            </td>
                                            <td class="valign-middle">
                                                <div class="progress m-0" style="height: 7px;">
                                                    <div class="progress-bar bg-success" role="progressbar"
                                                         style="width: {{$dt->progress_pct}}%"
                                                         aria-valuenow="{{$dt->progress_pct}}" title=" {{round($dt->progress_pct,2)}}%"
                                                         aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                    </tbody>
                  </table>
                  <nav aria-label="Page navigation">
                            <ul class="pagination justify-content-center pagination-separate pagination-round pagination-flat pagination-sm">
                              <?php echo $project->render(); ?>
                            </ul>
                          </nav>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-xl-6 col-lg-12">
            <div class="card">
              <div class="card-header no-border bg-blue">
                <h4 class="card-title text-white">Performance & Workload</h4>
                <a class="heading-elements-toggle"><i class="ft-more-horizontal font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content">

                <div id="audience-list-scroll" class="table-responsive height-250 position-relative">
                  <div id="workload" style="height: 200px">
                  <table class="table mb-0 table-striped" >
                    <thead>
                    <tr style="background-color: #5F9EA0;color: white;">
                      <th>Staff</th>
                      <th>Performance Rating</th>
                      <th>Workload Rating</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($workload as $wk)
                    <tr>
                      <td><a href="#" class="text-bold-600" onclick="ajaxMenu(event,'user/detailtask/{{$wk->id}}')">{{$wk->full_name}}</a></td>
                      <td>{{ $wk->rate_level_id }}</td>
                      <td>
                        {{$wk->workload}}
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-6 col-lg-12">
              <div class="card">
                <div class="card-header bg-blue">
                  <h4 class="card-title text-white">Project Overall Progress</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content">
                  <!-- project progress -->
                  <div class="card-body">
                    <div class="insights">
                      <p>RBB
                     
                        <span class="float-right text-warning h3">{{floor($rbb)}}%</span>
                       
                       
                      </p>
                      <div class="progress progress-sm mt-1 mb-0">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{floor($rbb)}}%" aria-valuenow="{{floor($rbb)}}"
                             aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                  <!-- project progress -->
                </div>
              </div>
              <div class="card">
                <div class="card-header bg-blue">
                  <h4 class="card-title text-white">Project Overall Progress</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content">
                  <!-- project search -->
                  <!-- /project search -->
                  <!-- project progress -->
                  <div class="card-body">
                    <div class="insights">
                      <p>Non RBB
                        <span class="float-right text-warning h3">{{floor($nrbb)}}%</span>
                      </p>
                      <div class="progress progress-sm mt-1 mb-0">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{floor($nrbb)}}%" aria-valuenow="{{floor($nrbb)}}"
                             aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                  <!-- project progress -->
                </div>
              </div>
          </div>
        </div>

 <!-- Team -->
<div class="row">
          <div class="col-xl-6 col-lg-12">
            <div class="card">
              <div class="card-header no-border bg-blue">
                <h4 class="card-title text-white">Performance & Workload Team</h4>
                <a class="heading-elements-toggle"><i class="ft-more-horizontal font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content">

                <div id="audience-list-scroll" class="table-responsive height-250 position-relative">
              <div id="team" style="height: 200px">
                <table class="table mb-0 table-striped">
                    <thead>
                    <tr style="background-color: #5F9EA0;color: white;">
                      <th>Team</th>
                      <th>Performance Rating</th>
                      <th>Workload Rating</th>
                    </tr>
                    </thead>
                  
                    <tbody>
                    @foreach($workloadteam as $wk)
                    <tr>
                      <td><a href="#" class="text-bold-600" onclick="ajaxMenu(event,'team/detailtask/{{$wk->id}}')">{{$wk->definition}}</a></a></td>
                      <td>{{ $wk->rate_level_id }}</td>
                      <td>
                        {{$wk->workload}}
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-6 col-lg-12">
              <div class="card">
                <div class="card-header bg-blue">
                  <h4 class="card-title text-white">Project Overall Progress Team</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content">
                  <!-- project progress -->

                      <div id="rbbteam" style="height: 250px">
                  <div class="card-body">

                    <div class="insights">
                       
                      @foreach($rbbteam as $dt)
                      <p>{{$dt->team}}
                     
                        <span class="float-right text-warning h3">{{$dt->progress}}%</span>
                       
                       
                      </p>
                      <div class="progress progress-sm mt-1 mb-0">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{$dt->progress}}%" aria-valuenow="{{$dt->progress}}"
                             aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                      @endforeach
                   
                    </div>

                  </div>

                       </div>
                  <!-- project progress -->
                </div>
              </div>
          </div>
        </div>




        <div class="row">
          <div class="col-xl-6 col-lg-12">
            <div class="card">
              <div class="card-head">
                <div class="card-header bg-blue">
                  <h4 class="card-title text-white">Task Progress</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div id="basic-pie" class="height-400 echart-container"></div>
                </div>
              </div>
            </div>
          </div>
        

         <div class="col-xl-6 col-lg-12">
            <div class="card profile-card-with-cover border-grey border-lighten-2">
              <div class="card-img-top img-fluid bg-cover height-300" style="background: url('images/bg1.jpg');
    background-position: center;"></div>

              @if(!empty($pegawai[0]->photo))
              <div class="card-profile-image">
                <img src="{{asset('uploadfoto/')}}/{{ $pegawai[0]->photo }}" style="width: 150px" class="rounded-circle img-border"
                     alt="Card image">
                </div>
              @else
              <div class="card-profile-image">
                <img src="{{asset('images/portrait/small/avatar-s-9.png')}}" class="rounded-circle img-border"
                     alt="Card image">
              </div>
              @endif
              <div class="profile-card-with-cover-content text-center">
                <div class="card-body">
                  <h4 class="card-title">{{ $pegawai==null ? '' : $pegawai[0]->full_name }}</h4>
                  <p class="text-muted m-0">Pegawai yang memiliki rating kinerja terbaik</p>
                </div>
                <div class="card-body">
                </div>
              </div>
            </div>
          </div>
          </div>

  </div>
</div>

@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/_dashboard.js') }}"></script>
@stop

