 <div class="modal animated slideInRight text-left" id="_edit" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-blue">
                                  <h4 class="modal-title white" id="myModalLabel76">Edit Model</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  
                                   <form id="form_edit_lm"  name="formEdit" role="form" class="form-validation" data-parsley-validate="">
                                    <div class="form-group">
                                          <label for="message-text" class="col-form-label">Model</label>
                                          <input type="text" class="form-control" id="name_model1" name="name_model" data-parsley-required>
                                      </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-outline-info"  onclick="processUpdate()">Update</button>
                                </div>
                              </div>
                            </div>
                          </div>