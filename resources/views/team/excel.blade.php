<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>

<table  class="tg" border="1">
	<thead>
		<tr><th colspan="5"><h3></h3>{{$module[0]->definition}}</th></tr>
		<tr>
           <th>Nama</th>
           <th>Nama Sub Task</th>
           <th align="center">Durasi (hari)</th>
           <th>Status</th>
           <th>Progress</th>
        </tr>
	</thead>

	<tbody>
		@if(!empty($module))
			
             @foreach($module as $pm)
                         <tr>
                          <td style="font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;" colspan="5"><b>{{ $pm->project_name }}</b></td>
                        </tr>
                        @php
                           $task=DB::table('master_task')
                           ->select('id','task_name','id_task')
                           ->where('id_project',$pm->id)
                           ->get();
                        @endphp
                        @foreach($task as $ts)
                        <tr>
                          <td style="font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;" colspan="5"> &nbsp;&nbsp;&nbsp;&nbsp;{{$ts->task_name}}</td>
                        </tr>

                        @php
                         $subtask=\DB::select("select u.full_name,mst.sub_task_name,mst.duration,ps.definition as progress,rs.definition as status 
                          from master_sub_task mst
                          join users u on u.id=mst.pic_id
                          join ref_progress_status ps on ps.id=mst.progress_id 
                          join ref_status rs on rs.id=mst.status_id
                          WHERE  id_task=$ts->id");
                        @endphp


                        @foreach($subtask as $mst)
                         <tr>
                          <td>
                             {{$mst->full_name}}
                           </td>
                           <td>
                             {{$mst->sub_task_name}}
                           </td>
                           <td>
                             {{$mst->duration}}
                           </td>
                           <td>
                             {{$mst->status}}
                           </td>
                           <td>
                             {{$mst->progress}}
                           </td>
                         </tr>
                        @endforeach

                        @endforeach
                        
                        @endforeach
                      	@endif  
                       </tbody>
	
</table>