@section('content')

           <div class="card">
              <div class="card-head">
                  <div class="px-1">
                    <ul class="list-inline list-inline-pipe text-center p-1 border-bottom-grey border-bottom-lighten-3">
                      <li>Team Project:
                        <span class="text-muted">{{$team[0]->team}}</span>
                      </li>
                      <li>Client:
                        <span class="text-muted">{{$team[0]->client}}</span>
                      </li>
                      <li>Jumlah Anggota Team:
                        <span class="text-muted">{{count($team)}}</span>
                      </li>
                      <li><a href="{{route('excelteam',$team[0]->id)}}" class="text-muted" data-toggle="tooltip" data-placement="bottom"
                        title="Export as Excel"><i class="fa fa-file-excel-o"></i></a></li> 
                    </ul>
                  </div>
                </div>
                <!-- project-info -->
                <div id="project-info" class="card-body row">
                  <div class="project-info-count col-lg-4 col-md-12">
                    <div class="project-info-icon">
                      <h2>{{count($onprogress)}}</h2>
                      <div class="project-info-sub-icon">
                        <span class="fa fa-tasks"></span>
                      </div>
                    </div>
                    <div class="project-info-text pt-1">
                      <h5>On Progress Task</h5>
                    </div>
                  </div>
                  <div class="project-info-count col-lg-4 col-md-12">
                    <div class="project-info-icon">
                      <h2>{{count($done)}}</h2>
                      <div class="project-info-sub-icon">
                        <span class="fa fa-check-square-o"></span>
                      </div>
                    </div>
                    <div class="project-info-text pt-1">
                      <h5>Done Task</h5>
                    </div>
                  </div>
                  <div class="project-info-count col-lg-4 col-md-12">
                    <div class="project-info-icon">
                      <h2>{{count($task)}}</h2>
                      <div class="project-info-sub-icon">
                        <span class="fa fa-th"></span>
                      </div>
                    </div>
                    <div class="project-info-text pt-1">
                      <h5>Total Task</h5>
                    </div>
                  </div>
                </div>
                <!-- project-info -->
                <div class="card-body">
                  <div class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                    <span>SLA Monitoring</span>
                  </div>
                  <div class="row py-2">
                     <table class="table table-striped ">
                       <thead>
                        <tr>
                          <th>
                              Nama
                           </th>
                           <th>
                              Nama Sub Task
                           </th>
                           <th align="center">
                              Durasi (hari) 
                           </th>
                           <th>
                              Status 
                           </th>
                           <th>
                              Progress 
                           </th>
                       </tr>    
                       </thead>
                       <tbody>
                        @foreach($project as $pm)
                         <tr>
                          <td style="font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;" colspan="5"><b>{{ $pm->project_name }}</b></td>
                        </tr>
                        @php
                           $task=DB::table('master_task')
                           ->select('id','task_name','id_task')
                           ->where('id_project',$pm->id)
                           ->paginate(2);
                        @endphp
                        @foreach($task as $ts)
                        <tr>
                          <td style="font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;" colspan="5"> &nbsp;&nbsp;&nbsp;&nbsp;{{$ts->task_name}}</td>
                        </tr>

                        @php
                         $subtask=\DB::select("select u.full_name,mst.sub_task_name,mst.duration,ps.definition as progress,rs.definition as status 
                          from master_sub_task mst
                          join users u on u.id=mst.pic_id
                          join ref_progress_status ps on ps.id=mst.progress_id 
                          join ref_status rs on rs.id=mst.status_id
                          WHERE  id_task=$ts->id");
                        @endphp


                        @foreach($subtask as $mst)
                         <tr>
                          <td>
                             {{$mst->full_name}}
                           </td>
                           <td>
                             {{$mst->sub_task_name}}
                           </td>
                           <td>
                             {{$mst->duration}}
                           </td>
                           <td>
                             {{$mst->status}}
                           </td>
                           <td>
                             {{$mst->progress}}
                           </td>
                         </tr>
                        @endforeach

                        @endforeach
                        
                        @endforeach
                        
                       </tbody>
                   </table>
                   <nav aria-label="Page navigation">
                      <ul class="pagination justify-content-center pagination-separate pagination-round pagination-flat pagination-sm">
                              <?php echo $task->render(); ?>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>

 
@endsection
