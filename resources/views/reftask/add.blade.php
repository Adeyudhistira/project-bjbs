 <div class="modal animated slideInRight text-left" id="_create" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-blue">
                                  <h4 class="modal-title white" id="myModalLabel76">Tambah Task</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  
                                   <form id="_create_form">
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Definition</label>
                                          <input type="text" class="form-control" id="definition" name="definition" data-parsley-required>
                                      </div>
                                     <div class="form-group">
                                          <label for="message-text" class="col-form-label">Model</label>
                                          
                                          <select style="width:470px" name="id_model" class=" form-control" data-parsley-required id="id_model">
                                             
                                          </select>
                                     
                                      </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-outline-info"  onclick="processInsert()">Save</button>
                                </div>
                              </div>
                            </div>
                          </div>