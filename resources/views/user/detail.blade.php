@section('content')
 <div class="sidebar-detached sidebar-left">
     
           <div class="card">
                <!-- project-info -->
                <div id="project-info" class="card-body row">
                  <div class="project-info-count col-lg-4 col-md-12">
                    <div class="project-info-icon">
                      <h2>{{count($onprogress)}}</h2>
                      <div class="project-info-sub-icon">
                        <span class="fa fa-tasks"></span>
                      </div>
                    </div>
                    <div class="project-info-text pt-1">
                      <h5>On Progress Task</h5>
                    </div>
                  </div>
                  <div class="project-info-count col-lg-4 col-md-12">
                    <div class="project-info-icon">
                      <h2>{{count($done)}}</h2>
                      <div class="project-info-sub-icon">
                        <span class="fa fa-check-square-o"></span>
                      </div>
                    </div>
                    <div class="project-info-text pt-1">
                      <h5>Done Task</h5>
                    </div>
                  </div>
                  <div class="project-info-count col-lg-4 col-md-12">
                    <div class="project-info-icon">
                      <h2>{{count($task)}}</h2>
                      <div class="project-info-sub-icon">
                        <span class="fa fa-th"></span>
                      </div>
                    </div>
                    <div class="project-info-text pt-1">
                      <h5>Total Task</h5>
                    </div>
                  </div>
                </div>
                <!-- project-info -->
                <div class="card-body">
                  <div class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                    <span>SLA Monitoring</span>
                  </div>
                   <a href="{{route('exceltask',$user==null ? '-' : $user[0]->id)}}"
                        title="Export as Excel"><i class="fa fa-file-excel-o"> Export Excel</i></a>
                  <div class="row py-2">
                     <table class="table table-striped ">
                       <thead>
                        <tr>
                           <th>
                              Nama Sub Task
                           </th>
                           <th align="center">
                              Durasi (hari) 
                           </th>
                           <th>
                              Status 
                           </th>
                           <th>
                              Progress 
                           </th>
                       </tr>    
                       </thead>
                       <tbody>
                        @foreach($project as $pm)
                         <tr>
                          <td style="font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;" colspan="4"><b>{{ $pm->project_name }}</b></td>
                        </tr>
                        @php
                           $task=DB::table('master_task')
                           ->select('id','task_name','id_task')
                           ->where('pic_id',$pm->pic_id)
                           ->where('id_project',$pm->id)
                           ->paginate(2);
                        @endphp
                        @foreach($task as $ts)
                        <tr>
                          <td style="font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;" colspan="4"> &nbsp;&nbsp;&nbsp;&nbsp;{{$ts->task_name}}</td>
                        </tr>

                        @php
                         $subtask=\DB::select("select mst.sub_task_name,mst.duration,ps.definition as progress,rs.definition as status 
                          from master_sub_task mst
                          join ref_progress_status ps on ps.id=mst.progress_id 
                          join ref_status rs on rs.id=mst.status_id
                          WHERE pic_id=$pm->pic_id and id_task=$ts->id");
                        @endphp


                        @foreach($subtask as $mst)
                         <tr>
                           <td>
                             {{$mst->sub_task_name}}
                           </td>
                           <td>
                             {{$mst->duration}}
                           </td>
                           <td>
                             {{$mst->status}}
                           </td>
                           <td>
                             {{$mst->progress}}
                           </td>
                         </tr>
                        @endforeach

                        @endforeach
                        
                        @endforeach
                        
                       </tbody>
                   </table>
                   <nav aria-label="Page navigation">
                      <ul class="pagination justify-content-center pagination-separate pagination-round pagination-flat pagination-sm">
                              <?php echo $task->render(); ?>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
           
    </div>
       <div class="row">
        <div class="col-12">
           <div class="card">
              <div class="text-center">
                <div class="card-body">
                    @if($user==null)
                    @elseif($user[0]->photo==null)
                    <img src="{{asset('images/portrait/medium/avatar-m-4.png')}}" class="rounded-circle  height-150"
                  alt="Card image">
                    @else

                  <img  src="{{URL::asset('/uploadfoto/'.$user[0]->photo)}}" class="rounded-circle  height-150"
                  alt="Card image">
                    @endif
                </div>
                <div class="card-body">
                  <h4 class="card-title">{{$user==null ? '' : $user[0]->full_name}}</h4>
                  <h6 class="card-subtitle text-muted">{{$user==null ? '-' : $user[0]->definition}}</h6>
                </div>
              </div>
              <div class="list-group list-group-flush">
                <a class="text-info list-group-item""><i class="fa fa-briefcase"></i> 
                  @if($user[0]->status==1)
                  Active
                  @else
                  Non Active
                  @endif
                </a>
                <p class="list-group-item"><i class="ft-phone"></i> {{$user==null ? '-' : $user[0]->phone }}</p>
                  <p class="list-group-item"><i class="ft-mail"></i> {{$user==null ? '-' : $user[0]->email}}</p>
              </div>
            </div>
        </div>
    </div>
@endsection
