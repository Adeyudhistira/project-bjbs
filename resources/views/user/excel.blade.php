<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>

<table  class="tg" border="1">
	<thead>
		<tr><th colspan="7"><h3></h3>Sub Task List By PIC</th></tr>
		<tr><th colspan="7"><h3></h3>{{$module[0]->full_name}}</th></tr>
		<tr>
			<th>Project</th><th>Task</th><th>Sub Task</th><th>Progress Status</th>
			<th>Tanggal Mulai</th><th>Target Selesai</th><th>Tanggal Selesai</th>
		</tr>
	</thead>
	<tbody>
		@if(!empty($module))
			@foreach($module as $dt)
			<tr>
				<td>{{$dt->project_name}}</td>
				<td>{{$dt->task_name}}</td>
				<td>{{$dt->sub_task_name}}</td>
				<td>{{$dt->definition}}</td>
				<td>{{$dt->start_date}}</td>
				<td>{{$dt->target_date}}</td>
				<td>{{ $dt->end_date==null ? '-' : $dt->end_date}}</td>
			</tr>
			@endforeach
		@endif
	</tbody>
	
</table>