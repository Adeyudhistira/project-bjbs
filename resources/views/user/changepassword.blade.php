@section('content')
 <div class="row">
     <div class="col-12">
        <div class="card">
           <div class="card-header">
                <h4 class="card-title">Change Password</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
        <div class="card-content collpase show">
            <div class="card-body card-dashboard">
                  <div id="ch" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src="{{asset('images/loader.gif')}}" width="110" height="110" /></div>
            <form id="form_edit_lm" name="formEdit" role="form" class="form-validation" method="post" action="{{route('update1')}}" data-parsley-validate="">
			{{ csrf_field() }}    
			<div class="form-group">
			<label for="recipient-name" class="col-form-label">Current Password</label>
			<input type="password" class="form-control" required data-validation-required-message="Tidak Boleh Kosong"  id="oldpassword" name="oldpassword">
			@if ($errors->has('oldpassword'))
			    <div class="error">{{ $errors->first('oldpassword') }}</div>
			@endif
			<input type="hidden" value="{{Auth::user()->id}}" id="id1" name="id">	
			</div>
			<div class="form-group">
			<label for="message-text" class="col-form-label">New Password</label>
			<input type="password" required data-validation-required-message="Tidak Boleh Kosong"  class="form-control" id="passwordnew" name="passwordnew">
			</div>
			<div class="form-group">
			<label for="message-text" class="col-form-label">Confirm New Password</label>
			<input type="password"  required data-validation-required-message="Tidak Boleh Kosong"  id="cpasswordnew" name="cpasswordnew" class="form-control" />
			@if ($errors->has('cpasswordnew'))
			    <div class="error">{{ $errors->first('cpasswordnew') }}</div>
			@endif
			</div>
			<div class="registrationFormAlert" id="divCheckPasswordMatch"></div>

			<button type="submit"  class="btn btn-info pull-right">Confirm</button>

			</form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection