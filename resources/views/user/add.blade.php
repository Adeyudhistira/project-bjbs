 <div class="modal animated slideInRight text-left" id="_create" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-blue">
                                  <h4 class="modal-title white" id="myModalLabel76">Tambah Users</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                   <div id="us" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src="{{asset('images/loader.gif')}}" width="110" height="110" /></div>
                                   <form id="_create_form">
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Full Name</label>
                                          <input type="text" class="form-control" id="full_name" name="full_name" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Username</label>
                                          <input type="text" class="form-control" id="user_name" name="user_name" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Team</label>
                                          <select name="team_id" class="form-control" data-parsley-required id="team_id">
                                          </select>
                                      </div>
                                       <div class="form-group">
                                           <label for="message-text" class="col-form-label">Role</label>
                                           <select name="role_id" class="form-control" data-parsley-required id="role_id">
                                               <option value="1">Admin</option>
                                               <option value="2">Spv</option>
                                           </select>
                                       </div>
                                       <div class="form-group">
                                           <label for="message-text" class="col-form-label">Foto / Avatar</label>
                                           <input type="file" class="form-control" id="photo" name="photo" data-parsley-required>
                                       </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-outline-info"  onclick="processInsert()">Save</button>
                                </div>
                              </div>
                            </div>
                          </div>