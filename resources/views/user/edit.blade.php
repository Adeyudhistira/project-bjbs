 <div class="modal animated slideInRight text-left" id="_edit" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-blue">
                                  <h4 class="modal-title white" id="myModalLabel76">Edit Users</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  
                                   <form id="form_edit_lm"  name="formEdit" role="form" class="form-validation" data-parsley-validate="">
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Full Name</label>
                                          <input type="text" class="form-control" id="full_name1" name="full_name" data-parsley-required>
                                          <input type="hidden" class="form-control" id="id" name="id" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Username</label>
                                          <input type="text" class="form-control" id="user_name1" name="user_name" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Team</label>
                                          <select name="team_id" class="form-control" data-parsley-required id="team_id1">
                                             
                                          </select>
                                      </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-outline-info"  onclick="processUpdate()">Update</button>
                                </div>
                              </div>
                            </div>
                          </div>