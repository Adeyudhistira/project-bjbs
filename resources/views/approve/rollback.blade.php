<div class="modal animated slideInRight text-left" id="_return" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <h4 class="modal-title white" id="myModalLabel76">Return Pekerjaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="list_body">
                <form id="_return_form">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Masukan Keterangan</label>
                        <textarea name="note" class="form-control"></textarea>
                    </div>
                    <input type="hidden" id="id_subtask_return" name="id_subtask_return">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-blue" data-dismiss="modal" onclick="processReturn()">Simpan</button>
            </div>
        </div>
    </div>
</div>