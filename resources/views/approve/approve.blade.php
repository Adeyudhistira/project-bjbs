<div class="modal animated slideInRight text-left" id="_rating" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <h4 class="modal-title white" id="myModalLabel76">Rating Pekerjaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="list_body">
                <p>Mohon rating pekerjaan yang telah selesai dilakukan.</p>
                <div id="rate"></div>
                <input type="hidden" id="id_subtask">
                <input type="hidden" id="end_date1" name="end_date">
                <input type="hidden" id="duration1" name="duration">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-blue" data-dismiss="modal" onclick="refreshPage()">Simpan</button>
            </div>
        </div>
    </div>
</div>