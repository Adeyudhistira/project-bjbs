@section('content')

      <div class="sidebar-detached sidebar-left"="">
        <div class="sidebar">
          <div class="bug-list-sidebar-content">
            <!-- Predefined Views -->
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Daftar Project</h4>
                <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  </ul>
                </div>
              </div>
              <!-- bug-list search -->
              <div class="card-content">
             
                <!-- /bug-list search -->
                <!-- bug-list view -->
                <div class="card-body ">
                  <div class="list-group">
                        @foreach($project as $pj)
                       
                            <a href="#" onclick="ajaxMenu(event,'approve/task/{{$pj->id}}')" title="PIC ({{$pj->pic}})" class="list-group-item list-group-item-action {{ $pj->id == $id ? 'active' : '' }}">{{$pj->project_name}}</a>          
                            
                        @endforeach
                  </div>
                </div>
              </div>
            </div>
            <!--/ Predefined Views -->
          </div>
        </div>
      </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Daftar Approval Task</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body card-dashboard">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Project</th>
                                <th>Task</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Target Selesai</th>
                                <th>Durasi (hari)</th>
                                <th>Project Manager</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('approve.approve')
@stop
@section('script')
    <script type="text/javascript" src="{{ asset('js/pages/_approve.js') }}"></script>
@stop