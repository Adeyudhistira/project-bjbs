@section('content')

      <div class="sidebar-detached sidebar-left"="">
        <div class="sidebar">
          <div class="bug-list-sidebar-content">
            <!-- Predefined Views -->
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Daftar Project</h4>
                <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  </ul>
                </div>
              </div>
              <!-- bug-list search -->
              <div class="card-content">
             
                <!-- /bug-list search -->
                <!-- bug-list view -->
                <div class="card-body ">
                  <div class="list-group">
                        @foreach($project as $pj)
                       
                            <a href="#" onclick="ajaxMenu(event,'approve/task/{{$pj->id}}')" title="PIC ({{$pj->pic}})" class="list-group-item list-group-item-action {{ $pj->id == $id ? 'active' : '' }}">{{$pj->project_name}}</a>          
                            
                        @endforeach
                  </div>
                </div>
              </div>
            </div>
            <!--/ Predefined Views -->
          </div>
        </div>
      </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Daftar Approval Task</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body card-dashboard">
                        <div class="table-responsive">

                        <table class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                      <thead>
                        <tr>
                          <th>&nbsp;</th>
                          <th>Nama Pekerjaan</th>
                          <th>Dates</th>
                          <th>Tanggal Mulai</th>
                          <th>Target Selesai</th>
                          <th>Tanggal Selesai</th>
                          <th>Durasi (Hari)</th>
                          <th>PIC</th>
                          <th>Dokumen Pendukung</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($task as $data)
                         <tr>
                            <td>
                               <i class='icon-note'></i>
                            </td>
                          <td>
                            <p class="text-muted"> {{$data->sub_task_name}}</p>
                          </td>
                          <td>
                             <h6 class="mb-0">
                              <span class="text-bold-600">{{$data->task_name}}</span>
                            </h6>
                          </td>
                          <td>
                            {{date('d-M-Y',strtotime($data->start_date))}}
                          </td>
                          <td>
                            {{date('d-M-Y',strtotime($data->target_date))}}
                          </td>
                          <td>
                            {{date('d-M-Y',strtotime($data->end_date))}}
                          </td>
                          <td>
                            {{$data->duration}}
                          </td>
                          
                          <td class="text-center">
                            {{$data->pic}}
                          </td>
                          <td class="text-center">
                            <a href="{{route('download',$data->url_doc)}}" title="{{$data->url_doc}}">{{$data->url_doc}}</a>
                          </td>
                          
                          <td>
                           <button type="button" class="btn btn-success bg-green" data-toggle="tooltip" data-placement="top" title="" data-original-title="Approve" onclick="rating({{$data->mstid}},'{{$data->target_date}}')">
                                <i class="fa fa-check-square-o"></i>
                            </button>
                            <button type="button" class="btn btn-danger bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Return" onclick="showReturn({{$data->mstid}})">
                                <i class="fa fa-repeat"></i>
                            </button>
                          </td>
                        </tr>
                  @endforeach
                      </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('approve.approve')
    @include('approve.rollback')
@stop
@section('script')
    <script type="text/javascript" src="{{ asset('js/pages/_approve.js') }}"></script>
@stop