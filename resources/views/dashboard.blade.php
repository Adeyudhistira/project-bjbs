<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
  <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
  <meta name="author" content="PIXINVENT">
  <title>PM BJB SYARIAH | Dashboard</title>
  <link href="{{ asset('images/ico/apple-icon-120.png') }}" rel="apple-touch-icon">
  <link href="{{asset('images/bjbsmall.jpg')}}" rel="shortcut icon" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,400,500,700"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link href="{{ asset('css/vendors.css') }}" rel="stylesheet">
  <!-- END VENDOR CSS-->
  <!-- BEGIN ROBUST CSS-->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <link href="{{ asset('css/pages/login-register.css') }}" rel="stylesheet">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
 <!-- END Custom CSS-->
</head>
<body class="vertical-layout vertical-compact-menu 1-column   menu-expanded blank-page blank-page"
data-open="click" data-menu="vertical-compact-menu" data-col="1-column">
  <!-- ///////////////////////////////////////////////////////background-size:contain;/////////////////////-->
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body" style="opacity: 1.2">
        @php
        $query= \DB::select('select * from ref_team where status=1'); 
        @endphp

        @foreach($query as $item)
        @php
        $waitApproval = DB::table('master_project')->select(DB::raw('count(*) as jml'))
             ->leftJoin('users','users.id','=','master_project.pm_id')
             //->where('is_approve','=', false)
             //->where('progress_id','=', 5)
             ->where('master_project.team_id','=', $item->id)
             ->where('status_id',2)
             ->first();

        $onprogress = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            ->leftJoin('users','users.id','=','master_project.pm_id')
            ->whereIn('status_id',[1,2])
            ->where('master_project.team_id','=', $item->id)
            ->first();

        $finish = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            ->leftJoin('users','users.id','=','master_project.pm_id')
            ->where('progress_id',2)
            ->where('master_project.team_id','=', $item->id)
            ->where('status_id',1)
            //->where('is_approve' ,'=', true)
            ->first();

        $jmltask = DB::table('master_project')
            ->select(DB::raw('count(*) as jml'))
            ->leftJoin('users','users.id','=','master_project.pm_id')
            ->where('master_project.team_id','=', $item->id)
            ->where('status_id',1)
            ->where('progress_id',5)
            ->first();

        @endphp

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header bg-blue">
                  <h4 class="card-title text-white">{{$item->definition}}</h4>
                    <a class="heading-elements-toggle"><i class="ft-more-horizontal font-medium-3"></i></a>
                </div>
                  <div class="card-content">
                      
                      <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-blue">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="ft-layers font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'task/detail/onprogress')">
                                                <span class="font-large-2 text-bold-300 white float-right"
                                                      >{{$onprogress->jml}}</span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">Total Project</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-red">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="ft-pause font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'task/detail/waiting')">
                                                <span class="font-large-2 text-bold-300 white default float-right"
                                                      >{{$waitApproval->jml}}</span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">InActive Project</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5 bg-warning">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="ft-clock font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'task/detail/done')">
                                                <span class="font-large-2 text-bold-300  white float-right"
                                                      >{{$finish->jml }}</span>
                                                 </a>     
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">On Progress project</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 bg-green">
                                        <div class="pb-1">
                                            <div class="clearfix mb-1">
                                                <i class="icon-check font-large-1 white float-left mt-1"></i>
                                                <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'task/detail/all')">
                                                <span class="font-large-2 text-bold-300 white float-right"
                                                      >{{$jmltask->jml}}</span>
                                                </a>      
                                            </div>
                                            <div class="clearfix">
                                                <span class="text-white">Finished Project</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            
                  </div>
                </div>
              </div>
            </div>
          @endforeach  

      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <!-- BEGIN VENDOR JS-->

  <!-- END ROBUST JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script type="text/javascript">
    setTimeout(function(){
   window.location.reload(1);
  }, 5000);
  </script>
    <!-- END PAGE LEVEL JS-->
</body>
</html>