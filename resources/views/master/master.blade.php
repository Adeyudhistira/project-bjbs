<!DOCTYPE html>
<!--<html lang="{{ app()->getLocale() }}">-->
<html class="loading" lang="en" data-textdirection="ltr">
<meta name="csrf-token" content="{{ csrf_token() }}">
<head>
    @include('master.head')
</head>
<body class="vertical-layout vertical-compact-menu 2-columns menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-compact-menu" data-col="2-columns">
    @include('master.header')

<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
  @include('master.menu')
</div>
    
  <div class="app-content content">
    <div class="content-wrapper ui dimmable">
      <div class="content-header row">
      </div>
        <div class="ui dimmer" id="mainLoader">
            <div class="ui loader"></div>
        </div>
        <div id="data_content">
            @yield('content')
        </div>
    </div>
  </div>

  
<div id="data_modal">@yield('modal')@include('user.changepassword')</div>

    @include('master.script')
<div id="data_script">@yield('script')</div>
<footer class="footer footer-static footer-dark navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://basys.id" target="_blank">BASYS TEAM</a>, All rights reserved. </span>
    </p>
</footer>

</body>
</html>