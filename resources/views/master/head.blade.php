<link href="{{ asset('css/vendors.css') }}" rel="stylesheet">

<link href="{{ asset('vendors/css/charts/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/css/charts/morris.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/css/extensions/unslider.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/css/weather-icons/climacons.min.css') }}" rel="stylesheet">
{{--<link href="{{ asset('vendors/css/forms/selects/select2.min.css') }}" rel="stylesheet">--}}
<link href="{{ asset('vendors/css/forms/selects/selectize.css') }}"/>
<link href="{{ asset('vendors/css/forms/selects/selectize.default.css') }}"/>

<link href="{{ asset('css/plugins/animate/animate.css') }}" rel="stylesheet">

<!-- END VENDOR CSS-->
<!-- BEGIN ROBUST CSS-->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/forms/selectize/selectize.css') }}" rel="stylesheet">
<!-- END ROBUST CSS-->
<!-- BEGIN Page Level CSS-->
<link href="{{ asset('css/core/menu/menu-types/vertical-compact-menu.css') }}" rel="stylesheet">
<link href="{{ asset('css/core/colors/palette-gradient.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/calendars/clndr.css') }}" rel="stylesheet">
<link href="{{ asset('css/core/colors/palette-climacon.css') }}" rel="stylesheet">
<link href="{{ asset('css/pages/users.css') }}" rel="stylesheet">
<link href="{{ asset('fonts/feather/style.css') }}" rel="stylesheet">

<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/css/tables/datatable/datatables.min.css') }}" rel="stylesheet">

<link href="{{ asset('assets/vendor/toastr-master/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/dimmer.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/loader.min.css') }}" rel="stylesheet">

<link href="{{ asset('assets/vendor/m-custom-scrollbar/jquery.mCustomScrollbar.css') }}">
<!-- END Custom CSS-->

<link href="{{ asset('vendors/css/pickers/pickadate/pickadate.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/css/pickers/daterange/daterangepicker.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/css/extensions/raty/jquery.raty.css') }}" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/extensions/bootstrap-treeview.min.css') }}">
<link href="{{ asset('css/pages/project.css') }}" rel="stylesheet">
<link href="{{ asset('css/treeview.css') }}" rel="stylesheet">
