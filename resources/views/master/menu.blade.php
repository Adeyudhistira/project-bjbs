<div id="myDIV">
<div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        <li class="nav-item"><a onclick="ajaxMenu(event,'home')" href="#"><i class="icon-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a>
        </li>
        <li class="nav-item"><a href="#"><i class="icon-notebook"></i><span class="menu-title" data-i18n="nav.templates.main">Master</span></a>
         <ul class="menu-content">
            <li><a class="menu-item" href="#" data-i18n="nav.support_raise_support.main" onclick="ajaxMenu(event,'projects')">Project</a>
            </li>
            <li>
              <a class="menu-item" onclick="ajaxMenu(event,'task')" href="#">Task</a>
            </li>
             @hasrole('SPV')
             <li>
                 <a class="menu-item" onclick="ajaxMenu(event,'approve')" href="#">Approval Task</a>
             </li>
             @endhasrole
          </ul>
        </li>

          @hasrole('Admin')
        <li class="nav-item"><a href="#"><i class="icon-folder-alt"></i><span class="menu-title" data-i18n="nav.templates.main">References</span></a>
        <ul class="menu-content">
            <li>
              <a class="menu-item" onclick="ajaxMenu(event,'model')" href="#">Model</a>
            </li>
            <li>
              <a class="menu-item" onclick="ajaxMenu(event,'refTask')" href="#">Task</a>
            </li>
            <li>
              <a class="menu-item" onclick="ajaxMenu(event,'refSubtask')" href="#">Sub Task</a>
            </li>
            <li>
              <a class="menu-item" onclick="ajaxMenu(event,'client')" href="#">Client</a>
            </li>
            <li>
              <a class="menu-item" onclick="ajaxMenu(event,'company')" href="#">Company</a>
            </li>
            <li>
              <a class="menu-item" onclick="ajaxMenu(event,'paralel')" href="#">Paralel</a>
            </li>
            <li>
            <a class="menu-item" onclick="ajaxMenu(event,'performance')" href="#">Performance</a>
            </li>
            <li>
             <a class="menu-item" onclick="ajaxMenu(event,'progressstatus')" href="#">Progress Status</a>
            </li>
            <li>
            <a class="menu-item" onclick="ajaxMenu(event,'projecttype')" href="#">Project Type</a>
            </li>
            <li>
               <a class="menu-item" onclick="ajaxMenu(event,'quicklevel')" href="#">Quick Level</a>
            </li>
            <li>
               <a class="menu-item" onclick="ajaxMenu(event,'status')" href="#">Status</a>
            </li>
            <li>
              <a class="menu-item" onclick="ajaxMenu(event,'tasklevel')" href="#">Task Level</a>
            </li>
            <li>
              <a class="menu-item" onclick="ajaxMenu(event,'team')" href="#">Team</a>
            </li>
            <li>
             <a class="menu-item" onclick="ajaxMenu(event,'user')" href="#">Users</a>
            </li>
            <li>
               <a class="menu-item" onclick="ajaxMenu(event,'workload')" href="#">Workload</a>
            </li>
          </ul>
        </li>
         <li class="nav-item"><a href="#"><i class="icon-printer"></i><span class="menu-title" data-i18n="nav.templates.main">Report</span></a>
        <ul class="menu-content">
           <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">Rekap</a>
              <ul class="menu-content">
                <li><a class="menu-item" href="#" onclick="ajaxMenu(event,'laporanperunit')" data-i18n="nav.templates.vert.classic_menu">Progress Program Kerja Per Unit</a>
                </li>
                <li><a class="menu-item" href="#" onclick="ajaxMenu(event,'laporanperpic')" data-i18n="nav.templates.vert.compact_menu">Progress Program Kerja Per PIC</a>
                </li>
                <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.content_menu">Progress (Consolidation)</a>
                  <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('excel4')}}" data-i18n="nav.templates.vert.content_menu">Excel</a></li>

                    <li><a class="menu-item" href="{{route('pdf4')}}" data-i18n="nav.templates.vert.content_menu">Pdf</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">Detail Unit</a>
              <ul class="menu-content">
                <li><a class="menu-item" href="#" onclick="ajaxMenu(event,'laporanproker')" data-i18n="nav.templates.vert.classic_menu">Progress Program Kerja</a>
                </li>
              </ul>
            </li>
            <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">Detail Proker</a>
              <ul class="menu-content">
                <li><a class="menu-item" href="#" onclick="ajaxMenu(event,'laporandetail')" data-i18n="nav.templates.vert.classic_menu">Laporan Program Kerja</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
          @endhasrole
      </ul>
    </div>
</div>    
