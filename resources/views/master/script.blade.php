 <!-- BEGIN VENDOR JS-->
 <script src="{{ asset('vendors/js/vendors.min.js') }}"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{ asset('vendors/js/extensions/jquery.knob.min.js') }}"></script>
  <script src="{{ asset('vendors/js/charts/raphael-min.js') }}"></script>
  <script src="{{ asset('vendors/js/charts/chartist.min.js') }}"></script>
  <script src="{{ asset('vendors/js/charts/chartist-plugin-tooltip.js') }}"></script>
  <script src="{{ asset('vendors/js/charts/chart.min.js') }}"></script>
  <script src="{{ asset('vendors/js/charts/jquery.sparkline.min.js') }}"></script>

  <script src="{{ asset('vendors/js/extensions/moment.min.js') }}"></script>
  <script src="{{ asset('vendors/js/extensions/underscore-min.js') }}"></script>
  <script src="{{ asset('vendors/js/extensions/clndr.min.js') }}"></script>
  <script src="{{ asset('vendors/js/extensions/unslider-min.js') }}"></script>
  
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN ROBUST JS-->
  <script src="{{ asset('js/core/app-menu.js') }}"></script>
  <script src="{{ asset('js/core/app.js') }}"></script>
  <script src="{{ asset('js/scripts/customizer.js') }}"></script>

  <!-- END ROBUST JS-->
  <!-- BEGIN PAGE LEVEL JS-->
 {{--<script src="{{ asset('vendors/js/charts/morris.min.js') }}"></script>
 <script src="{{ asset('js/scripts/pages/dashboard-project.js') }}"></script>--}}
  <!-- END PAGE LEVEL JS


  <!-- BEGIN PAGE VENDOR JS-->
   <script src="{{ asset('vendors/js/tables/datatable/datatables.min.js') }}"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{ asset('js/scripts/tables/datatables-extensions/datatables-sources.js') }}"></script>

  <!-- END PAGE LEVEL JS-->
<!--basic scripts initialization-->
<script src="{{ asset('assets/vendor/toastr-master/toastr.js') }}"></script>

<!-- Parsley Validation-->
<script src="{{ asset('assets/vendor/parsley/parsley.min.js') }}"></script>
<script src="{{ asset('assets/vendor/parsley/id.js') }}"></script>


<!-- GLobal Function PM-->
<script src="{{ asset('assets/js/_global.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script src="{{ asset('assets/js/scripts.min.js') }}"></script>

<!--Palette JS-->
<script src="{{ asset('assets/vendor/js-init/palette.js') }}"></script>
<script src="{{ asset('assets/js/dimmer.min.js') }}"></script>

<script src="{{ asset('assets/vendor/data-tables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/data-tables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('js/scripts/modal/components-modal.js') }}"></script>

{{--<script src="{{ asset('vendors/js/forms/select/select2.full.min.js') }}"></script>--}}

 {{--<script src="{{ asset('js/scripts/forms/select/form-select2.js') }}"></script>--}}
 <script src="{{ asset('vendors/js/forms/select/selectize.min.js') }}"></script>
 <script src="{{ asset('vendors/js/pickers/daterange/daterangepicker.js') }}"></script>

 <script src="{{ asset('js/scripts/modal/components-modal.js') }}"></script>

<script src="{{ asset('vendors/js/extensions/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/scripts/pages/project-task-list.js') }}"></script>

 <script src="{{ asset('vendors/js/pickers/pickadate/picker.js') }}"></script>
 <script src="{{ asset('vendors/js/pickers/pickadate/picker.date.js') }}"></script>
 <script src="{{ asset('vendors/js/pickers/pickadate/picker.time.js') }}"></script>
 <script src="{{ asset('vendors/js/pickers/pickadate/legacy.js') }}"></script>
 <script src="{{ asset('vendors/js/extensions/jquery.raty.js') }}"></script>
 <script src="{{ asset('assets/vendor/jquery-masking/jquery.mask.min.js') }}"></script>
 <script src="{{ asset('assets/vendor/m-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

  <script src="{{ asset('vendors/js/charts/echarts/echarts.js') }}"></script>
  <script src="{{ asset('vendors/js/charts/echarts/chart/pie.js') }}"></script>
  <script src="{{ asset('vendors/js/charts/echarts/chart/funnel.js') }}"></script>
  <script src="{{ asset('js/scripts/ui/scrollable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/extensions/bootstrap-treeview.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/extensions/listjs/list.pagination.min.js') }}" type="text/javascript"></script>

  <script src="{{ asset('vendors/js/charts/echarts/echarts.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/charts/echarts/chart/pie.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/charts/echarts/chart/funnel.js') }}" type="text/javascript"></script>
 