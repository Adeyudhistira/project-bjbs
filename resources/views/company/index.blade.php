@section('content')
 <div class="row">
     <div class="col-12">
         <div class="page-title mb-4 d-flex align-items-center">
                <div class="choose-form-tab d-inline-block">
                <ul class="nav nav-form-custom nav-form-sm float-right" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <button type="button" class="btn btn-blue" data-toggle="modal"
                         onclick="tambah()">Tambah Company</button>
                      
                    </li>
                </ul>
            </div>
        </div>
        <div class="card">
           <div class="card-header">
                <h4 class="card-title">Management Company</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
        <div class="card-content collpase show">
            <div class="card-body card-dashboard">
                  
                    <table id="t_user" class="table table-striped table-bordered source-data">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Company Name</th>
                          <th>Address</th>
                          <th>Phone</th>
                          <th>City</th>
                          <th>Province</th>
                          <th>Seq</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('company.add')
@include('company.edit')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('assets/js/_company.js') }}"></script>
@endsection