 <div class="modal animated slideInRight text-left" id="_create" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-blue">
                                  <h4 class="modal-title white" id="myModalLabel76">Tambah Company</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  
                                   <form id="_create_form">
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Company Name</label>
                                          <input type="text" class="form-control" id="company_name" name="company_name" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Address</label>
                                          <input type="text" class="form-control" id="address" name="address" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Phone</label>
                                          <input type="text" maxlength="12" class="form-control" onkeypress="return hanyaAngka(event)" id="phone" name="phone" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">City</label>
                                          <input type="text" class="form-control" id="city" name="city" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Province</label>
                                          <input type="text" class="form-control" id="province" name="province" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Seq</label>
                                          <input type="text" class="form-control" id="seq" name="seq" data-parsley-required>
                                      </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
                                  <button type="button" class="btn btn-outline-blue"  onclick="processInsert()">Simpan</button>
                                </div>
                              </div>
                            </div>
                          </div>