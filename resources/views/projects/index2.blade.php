@section('content')
      <div class="sidebar-detached sidebar-left"="">
        <div class="sidebar">
          <div class="bug-list-sidebar-content">
            <!-- Predefined Views -->
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Daftar Project</h4>
                <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    @hasrole('SPV')
                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                     @endhasrole
                  </ul>
                </div>
              </div>
              <!-- bug-list search -->
              <div class="card-content">
             
                <!-- /bug-list search -->
                <!-- bug-list view -->
                <div class="card-body ">
                  
                    <div class="list-group">
                        @foreach($project as $pj)
                       
                            <a href="#" onclick="ajaxMenu(event,'projects/detail/{{$pj->id}}')" class="list-group-item list-group-item-action {{ $pj->id === $id ? 'active' : '' }}">{{$pj->project_name}}</a>          
                            
                        @endforeach

                       <nav aria-label="Page navigation">
                            <ul class="pagination justify-content-center pagination-separate pagination-round pagination-flat pagination-sm">
                              <?php echo $project->render(); ?>
                            </ul>
                          </nav>
                  </div>
                </div>
              </div>
            </div>
            <!--/ Predefined Views -->
            </div>
        </div>
    </div>

    <div class="row" style="display: {{ $dis }}">
        <div class="col-12">
           <div class="card">
              <div class="text-center">
                <div class="card-body">
                  <img src="{{asset('images/portrait/medium/avatar-m-4.png')}}" class="rounded-circle  height-150"
                  alt="Card image">
                </div>
                <div class="card-body">
                  <h4 class="card-title">Frances Butler</h4>
                  <h6 class="card-subtitle text-muted">Project Manager</h6>
                </div>
              </div>
              <div class="list-group list-group-flush">
                <a href="#" class="list-group-item"><i class="fa fa-briefcase"></i> Project Name</a>
                <a href="#" class="list-group-item"><i class="ft-calendar"></i> Tgl Mulai</a>
                <a href="#" class="list-group-item"><i class="ft-calendar"></i> target Selesai</a>
              </div>
            </div>
        </div>
    </div>

    @include('projects.add')
    @include('projects.edit')
    @include('projects.listtask')
@stop
@section('script')
    <script type="text/javascript" src="{{ asset('js/pages/_projects.js') }}"></script>
@stop