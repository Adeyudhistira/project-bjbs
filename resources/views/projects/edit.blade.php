<div class="modal animated slideInRight text-left" id="_edit"
     role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
    <div class="modal-lg modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <h4 class="modal-title white" id="myModalLabel76">Edit Projects</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="wait1" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:40%;padding:2px;"><img src="{{asset('images/loader.gif')}}" width="110" height="110" /></div>
                <form id="_edit_form" enctype="multipart/form-data">
                    <table width="100%">
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                           <input type="hidden" class="form-control" id="id" name="id">
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Nama Project</label>
                                                <input type="text" class="form-control" required data-validation-required-message="Tidak Boleh Kosong" id="e_project_name" name="e_project_name">
                                            </div> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <div class="form-group">
                                            <label for="message-text" class="col-form-label">Tanggal Mulai</label>
                                            <input type="date" class="form-control" id="e_start_date" name="e_start_date" data-parsley-required>
                                        </div>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                           <div class="form-group">
                                                <label for="message-text" class="col-form-label">Tanggal Target</label>
                                                <input type="date" class="form-control" id="e_target_date" name="e_target_date" data-parsley-required>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                            <table width="100%">
                                <tr valign="top">
                                    <td>&nbsp;</td>
                                    <td>
                                        <div style="background-color: #6495ED; width: 0px;  height: 400px; border: 0.5px #6495ED solid;">
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                             <div class="form-group">
                                                <label for="message-text" class="col-form-label">Dokumen Pendukung</label>
                                                <input type="file"  class="form-control-file" id="e_url_doc" name="e_url_doc" multiple>
                                                <output id="list"></output>
                                            </div>
 
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            
                                                <div class="form-group">
                                                    <label for="message-text" class="col-form-label">Project Manager</label>
                                                    <select name="e_pm_id" required data-validation-required-message="Tidak Boleh Kosong" class="form-control" id="e_pm_id1">
                                                    </select>
                                                </div>
                                        </td>
                                    </tr>
                                       <tr>
                                        <td>
                                                              
                                       
                                        <div class="form-group">
                                             <label for="message-text" class="col-form-label">Status Project</label>
                                            <select name="e_status_id" required data-validation-required-message="Tidak Boleh Kosong" class="form-control"  id="e_status_id1">
                                            </select>
                                        </div>
                                         <input type='checkbox' id='is_paralel1' name='is_paralel'>&nbsp;Is Paralel</input>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-info" onclick="processUpdate()">Update</button>
            </div>
        </div>
    </div>
</div>
