<div class="modal animated slideInRight text-left" id="_create" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
    <div class="modal-lg modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <h4 class="modal-title white" id="myModalLabel76">Register Project</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

               <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src="{{asset('images/loader.gif')}}" width="110" height="110" /></div>
                <form id="_create_form" enctype="multipart/form-data">
                <table align="center"  width="100%" >
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                         <div class="form-group">
                                            <label for="message-text" class="col-form-label">Nama Project</label>
                                            <input type="text" class="form-control" required data-validation-required-message="Tidak Boleh Kosong" id="project_name" name="project_name">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                                <label>Tanggal Mulai s/d Tanggal Selesai</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                      <span class="fa fa-calendar-o"></span>
                                                    </span>
                                                    </div>
                                                    <input type='text' class="form-control" placeholder=" " name="range_date" id="range_date" readonly/>
                                                </div>
                                        </div>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                         <div class="form-group">
                                            <label >Jenis Project</label>
                                            <select name="project_type_id" class="form-control" required data-validation-required-message="Tidak Boleh Kosong"  id="project_type_id">
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                            <label for="message-text" class="col-form-label">Nama Client</label>
                                            <select name="client_id" class="form-control" required data-validation-required-message="Tidak Boleh Kosong"  id="client_id">
                                            </select>
                                        </div>
                                        <br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table width="100%">
                                <tr valign="top">
                                    <td>&nbsp;</td>
                                    <td>
                                        <div style="background-color: #6495ED; width: 0px;  height: 400px; border: 0.5px #6495ED solid;">
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                           <table width="100%">
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Team</label>
                                            <select name="team_id" class="form-control" required data-validation-required-message="Tidak Boleh Kosong"  id="team_id">
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="{{asset('images/loading.gif')}}" width="20" id="imgLoad1" style="display:none;" />
                                       <div class="form-group">
                                            <label for="message-text" class="col-form-label">Project Manager</label>
                                            <select name="pm_id" id="pm_id" class="form-control" required data-validation-required-message="Tidak Boleh Kosong">

                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Dokumen Pendukung</label>
                                            <input type="file" required data-validation-required-message="Tidak Boleh Kosong"  class="form-control-file" id="url_doc" name="file" >
                                        </div>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                            <label for="message-text" class="col-form-label">Anggaran Biaya</label>
                                            <input type="text" class="form-control money" required data-validation-required-message="Tidak Boleh Kosong" id="project_budget" name="project_budget">
                                        </div>
                                        <input type='checkbox' value="1"  id='is_paralel' name='is_paralel'  checked>&nbsp;Is Paralel</input>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-blue" onclick="processInsert()">Simpan</button>
            </div>
        </div>
    </div>
</div>