<div class="modal animated slideInRight text-left" id="_generate" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
    <div class="modal-lg modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <h4 class="modal-title white" id="myModalLabel76">Generate Task</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label >Model</label>
                        <select name="model_id" class="form-control" required data-validation-required-message="Tidak Boleh Kosong"  id="model_id">
                           
                        </select>
                </div>
               <div id="gen" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:40%;padding:2px;"><img src="{{asset('images/loader.gif')}}" width="110" height="110" /></div>
            <form id="_generate_form" enctype="multipart/form-data">
               <input type="hidden" class="form-control" id="id_project" name="id_project">
                <div id="task" class="acidjs-css3-treeview">
                 

            </div>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-blue" onclick="processGenerate()">Generate</button>
            </div>
        </div>
    </div>
</div>
