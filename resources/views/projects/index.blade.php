@section('content')

      <div class="sidebar-detached sidebar-left">
        <div class="sidebar">
          <div class="bug-list-sidebar-content">
            <!-- Predefined Views -->
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Daftar Project</h4>
                <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                      @role('SPV')
                    <li><a onclick="tambah()" data-toggle="modal"><i class="ft-plus"></i></a></li>
                      @endrole
                  </ul>
                </div>
              </div>
              <!-- bug-list search -->
              <div class="card-content">
             
                <!-- /bug-list search -->
                <!-- bug-list view -->
                <div class="card-body ">
                  
                    <div class="list-group">
                        @foreach($project as $pj)
                       
                            <a href="#" onclick="ajaxMenu(event,'projects/detail/{{$pj->id}}')" class="list-group-item list-group-item-action {{ $pj->id == $id ? 'active' : '' }}">{{$pj->project_name}}</a>          
                            
                        @endforeach

                       <nav aria-label="Page navigation">
                            <ul class="pagination justify-content-center pagination-separate pagination-round pagination-flat pagination-sm">
                              <?php echo $project->render(); ?>
                            </ul>
                          </nav>
                  </div>
                </div>
              </div>
            </div>
            <!--/ Predefined Views -->
            </div>
        </div>
    </div>

    <div class="row" style="display: {{ $dis }}">
        <div class="col-12">
           <div class="card">
              <div class="text-center">
                <div class="card-body">
                    @if($user==null)
                    @elseif($user[0]->photo==null)
                    <img src="{{asset('images/portrait/medium/avatar-m-4.png')}}" class="rounded-circle  height-150"
                  alt="Card image">
                    @else

                  <img  src="{{URL::asset('/uploadfoto/'.$user[0]->photo)}}" class="rounded-circle  height-150"
                  alt="Card image">
                    @endif
                </div>
                <div class="card-body">
                  <h4 class="card-title">{{$user==null ? '' : $user[0]->full_name}}</h4>
                  <h6 class="card-subtitle text-muted">Project Manager</h6>
                </div>
              </div>
              <div class="list-group list-group-flush">
                <a class="text-info list-group-item" onclick="showdet()"><i class="fa fa-briefcase"></i> {{$user==null ? '' : $user[0]->project_name}}</a>
                <p class="list-group-item"><i class="ft-calendar"></i> Tanggal Mulai : {{$user==null ? '' : date('d-M-Y',strtotime($user[0]->start_date))}}</p>
                <p class="list-group-item"><i class="ft-calendar"></i> Target Selesai : {{$user==null ? '' : date('d-M-Y',strtotime($user[0]->target_date))}}</p>
                @if($user)
                @php
                 $tasklast = \DB::select("select mp.id,mp.close,mp.project_name,mp.start_date,mp.target_date,u.full_name,u.photo,mst.sub_task_name from 
            master_project mp join users u on u.id=mp.pm_id 
            left join master_task mt on mt.id_project=mp.id
            left join master_sub_task mst on mst.id_task=mt.id
            where mp.id=$id and mst.progress_id=5 and is_approve=true  
            order by mst.updated_at desc");
                @endphp
                
                <p class="text-success list-group-item"><i class="ft-list"></i> Task Selesai : {{$tasklast==null ? '-' : $tasklast[0]->sub_task_name}}</p>
                @endif
                @role('SPV')
                 @if($user!=null)
                 @if($user[0]->close==0) 
                 <a class="text-info list-group-item" onclick="generate({{$user==null ? '' : $user[0]->id}})"><i class="fa fa-sitemap"></i> Generate Task</a>
                 @endif
                 @endif
                  
                 @if($user!=null)
                 @if($user[0]->close==0)
                 <a class="text-danger list-group-item" onclick="closeproject({{$user[0]->id}},'{{$user[0]->target_date}}')"><i class="fa fa-expeditedssl"></i> Close Project</a>
                 @endif
                 @endif
                 @if($user!=null)
                 @if($user[0]->close==1)
                 <a class="text-success list-group-item" onclick="openproject({{$user[0]->id}},base_url +'/projects/open','{{$user[0]->project_name}}')"><i class="fa fa-expeditedssl"></i> Open Project</a> 
                 @endif
                 @endif
                 @endrole
              </div>
            </div>
        </div>
    </div>

 <div class="row" style="display: {{ $dis }}">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Detail</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body card-dashboard">
                       <div class="table-responsive">
                         @if($user!=null)
                         @if($user[0]->close==1)
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>No SK</th>
                                <th>Tanggal SK</th>
                                <th>Perihal SK</th>
                                <th>Dokumen SK</th>
                              </tr>
                            </thead>
                            <tbody>
                              @if(!empty($det))
                                @foreach($det as $close)
                              <tr>
                                <td>
                                  {{$close->no_sk}}
                                </td>
                                <td>
                                  {{date('d-M-Y',strtotime($close->tgl_sk))}}
                                </td>
                                <td>
                                  {{$close->perihal_sk}}
                                </td>
                                <td>
                                  <a href="{{route('downloadp',$close->upload_doc)}}" title="{{$close->upload_doc}}">{{$close->upload_doc}}</a> 
                                  
                                </td>
                              </tr>
                              @endforeach
                              @endif
                            </tbody>
                          </table>
                         @endif
                         @endif
                          <br>

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Tanggal Selesai</th>
                                <th>Progress</th>
                                <th>Durasi (hari)</th>
                                <th>Status</th>
                                <th>Dokumen</th>
                                <th>Action</th>
                                <th colspan="2"></th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(!empty($det))
                                @foreach($det as $dt)
                                <tr>
                                    <td>
                                        {{ $dt->end_date==null ? 'Belum Selesai' : date('d-M-Y',strtotime($dt->end_date))}}
                                    </td>
                                    <td>
                                        <div class="progress progress-sm">
                                          <div aria-valuemin="{{$dt->progress_pct}}" aria-valuemax="100" class="progress-bar bg-gradient-x-success"
                                          role="progressbar" title=" {{round($dt->progress_pct,2)}}%" style="width: {{$dt->progress_pct}}%" aria-valuenow="{{$dt->progress_pct}}"></div>
                                        </div>
                                       
                                    </td>
                                    <td>
                                        {{$dt->duration}}
                                    </td>
                                    <td>
                                        @if($dt->status_id==1)
                                        <div class="badge badge-success">Active</div>
                                        @elseif($dt->status_id==2)
                                        <div class="badge badge-info">Inactive</div>
                                        @else
                                        <div class="badge badge-danger">Dropped</div>
                                        @endif
                                    </td>
                                    <td>
                                      <a href="{{route('downloadp',$dt->url_doc)}}" title="{{$dt->url_doc}}">{{$dt->url_doc}}</a>  
                                    </td>
                                    <td colspan="2">
                                        @role('SPV')
                                        @if($user!=null)
                                        @if($user[0]->close==0)
                                        
                                        <button type="button" class="btn btn-info bg-blue" data-toggle="tooltip" data-placement="top" title=""  data-original-title="Edit Project" onclick="editshow({{$dt->id_project}},{{$dt->iduser}},{{$dt->status_id}})">
                                         <i class="fa fa-pencil"></i>
                                        </button>
                                        @endif
                                        @endif
                                        @endrole
                                        
                                        <button type="button" class="btn btn-success bg-green" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detail Task" onclick="ajaxMenu(event,'projects/detailtask/{{$dt->id_project}}')">
                                            <i class="fa fa-th-list"></i>
                                        </button>
                                        
                                        
                                        @role('SPV')
                                        @if($user!=null)
                                        @if($user[0]->close==0)
                                        <button type="button" class="btn btn-danger bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Project" onclick="hapus({{$dt->id_project}},base_url+'/projects/delete')">
                                            <i class="fa fa-trash"></i>
                                        </button>

                                        @endif  
                                        @endif
                                        @endrole
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('projects.add')
    @include('projects.edit')
    @include('projects.generate')
    @include('projects.close')
    @include('projects.listtask')
@stop
@section('script')

<script type="text/javascript" src="{{ asset('js/pages/_projects.js') }}"></script>
@stop