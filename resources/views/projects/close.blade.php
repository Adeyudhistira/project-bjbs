<div class="modal animated slideInRight text-left" id="_close" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <h4 class="modal-title white" id="myModalLabel76">Close Project</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

               <div id="close" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src="{{asset('images/loader.gif')}}" width="110" height="110" /></div>
                <form id="_close_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">No SK</label>
                        <input type="hidden" class="form-control" required data-validation-required-message="Tidak Boleh Kosong" id="idproject" name="idproject">
                        <input type="hidden" class="form-control" required data-validation-required-message="Tidak Boleh Kosong" id="duration1" name="duration">
                        <input type="hidden" class="form-control" required data-validation-required-message="Tidak Boleh Kosong" id="end_date1" name="end_date">
                        
                        <input type="text" class="form-control" required data-validation-required-message="Tidak Boleh Kosong" id="no_sk" maxlength="20" name="no_sk">
                    </div> 
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Tanggal SK</label>
                        <input type="date" class="form-control" required data-validation-required-message="Tidak Boleh Kosong" id="tgl_sk" name="tgl_sk">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Perihal SK</label>
                        <textarea id="perihal_sk"  maxlength="200" name="perihal_sk" class="form-control" required data-validation-required-message="Tidak Boleh Kosong"></textarea> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Upload Dokumen</label>
                        <input type="file" required data-validation-required-message="Tidak Boleh Kosong"  class="form-control-file" id="upload_doc" name="upload_doc" >
                    </div>

            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-blue" onclick="processClose()">Close project</button>
            </div>
        </div>
    </div>
</div>