@section('content')
 <div class="sidebar-detached sidebar-left">
        <div class="sidebar">
          <div class="bug-list-sidebar-content">
            <!-- Predefined Views -->
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Daftar Project</h4>
                <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    
                     @role('SPV')
                    <li><a onclick="tambah()" data-toggle="modal"><i class="ft-plus"></i></a></li>
                      @endrole
                  </ul>
                </div>
              </div>
              <!-- bug-list search -->
              <div class="card-content">
             
                <!-- /bug-list search -->
                <!-- bug-list view -->
                <div class="card-body ">
                  
                    <div class="list-group">
                        @foreach($project as $pj)
                       
                            <a href="#" onclick="ajaxMenu(event,'projects/detail/{{$pj->id}}')" class="list-group-item list-group-item-action {{ $pj->id == $id ? 'active' : '' }}">{{$pj->project_name}}</a>          
                            
                        @endforeach

                       <nav aria-label="Page navigation">
                            <ul class="pagination justify-content-center pagination-separate pagination-round pagination-flat pagination-sm">
                              <?php echo $project->render(); ?>
                            </ul>
                          </nav>
                  </div>
                </div>
              </div>
            </div>
            <!--/ Predefined Views -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                    <h4 class="card-title">{{$userp==null ? '-' : $userp[0]->project_name}}</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                    @if(!empty($userp))                 
                        @if($userp[0]->status_id==1)
                            <span class="badge badge-default badge-success">{{$userp[0]->definition}}</span>  
                        @elseif($userp[0]->status_id==2)
                            <span class="badge badge-default badge-warning">{{$userp[0]->definition}}</span>   
                        @else
                            <span class="badge badge-default badge-danger">{{$userp[0]->definition}}</span>         
                        @endif
                    @else
                        <span class="badge badge-default badge-danger">-</span>
                    @endif    
                    </div>
                  </div>
                  <div class="px-1">
                    <ul class="list-inline list-inline-pipe text-center p-1 border-bottom-grey border-bottom-lighten-3">
                      <li>Project Owner:
                        <span class="text-muted">{{$userp==null ? '-' : $userp[0]->full_name}}</span>
                      </li>
                      <li>Start:
                        <span class="text-muted">{{$userp==null ? '-' : date('d/M/Y',strtotime($userp[0]->start_date))}}</span>
                      </li>
                      <li>Target:
                        <span class="text-muted">{{$userp==null ? '-' : date('d/M/Y',strtotime($userp[0]->target_date))}}</span>
                      </li>
                      <li>Finish:
                        @if(empty($userp))
                        <span class="text-muted">-</span>
                        @else
                        <span class="text-muted">{{ $userp[0]->end_date == null ? 'Belum Selesai' : date('d/M/Y',strtotime($userp[0]->end_date)) }}</span>
                        @endif
                      </li>
                      @if(!empty($detailtask))
                      <li><a href="{{route('excel',$detailtask==null ? '-' : $detailtask[0]->id_project)}}" class="text-muted" data-toggle="tooltip" data-placement="bottom"
                        title="Export as Excel"><i class="fa fa-file-excel-o"></i></a></li>
                      @endif  
                    </ul>
                  </div>
                </div>
                <!-- project-info -->
                <div id="project-info" class="card-body row">
                  <div class="project-info-count col-lg-4 col-md-12">
                    <div class="project-info-icon">
                      <h2>{{count($onprogress)}}</h2>
                      <div class="project-info-sub-icon">
                        <span class="fa fa-tasks"></span>
                      </div>
                    </div>
                    <div class="project-info-text pt-1">
                      <h5>On Progress Task</h5>
                    </div>
                  </div>
                  <div class="project-info-count col-lg-4 col-md-12">
                    <div class="project-info-icon">
                      <h2>{{count($done)}}</h2>
                      <div class="project-info-sub-icon">
                        <span class="fa fa-check-square-o"></span>
                      </div>
                    </div>
                    <div class="project-info-text pt-1">
                      <h5>Done Task</h5>
                    </div>
                  </div>
                  <div class="project-info-count col-lg-4 col-md-12">
                    <div class="project-info-icon">
                      <h2>{{count($total)}}</h2>
                      <div class="project-info-sub-icon">
                        <span class="fa fa-th"></span>
                      </div>
                    </div>
                    <div class="project-info-text pt-1">
                      <h5>Total Task</h5>
                    </div>
                  </div>
                </div>
                <!-- project-info -->
                <div class="card-body">
                  <div class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                    <span>SLA Monitoring</span>
                  </div>
                  <div class="row py-2">
                   <table class="table">
                       <thead>
                        <tr>
                           <th>
                              Nama Task
                           </th>
                           <th>
                              Durasi (hari) 
                           </th>
                           <th>
                              Status 
                           </th>
                       </tr>    
                       </thead>

                       <tbody>
                       @foreach($task as $ts)
                           <tr>
                               <td>
                                   {{$ts->task_name}}
                               </td>
                                <td>
                                   {{$ts->duration}}
                               </td>
                                <td>
                                   {{$ts->definition}}
                               </td>
                           </tr>
                         @endforeach  
                       </tbody>
                       
                   </table>
                  </div>
                  <div class="row py-2">
                     <table class="table table-striped ">
                       <thead>
                        <tr>
                           <th>
                              Nama Sub Task
                           </th>
                           <th>
                              Nama 
                           </th>
                           <th align="center">
                              Durasi (hari) 
                           </th>
                           <th>
                              Status 
                           </th>
                           <th>
                              Progress 
                           </th>
                       </tr>    
                       </thead>
                       <tbody>
                        @foreach($task as $ts)
                           <tr>
                             <td colspan="5"><b>{{$ts->task_name}}</b></td>
                           </tr>
                           @php
                            $subtask = \DB::select("SELECT 
                              mst.sub_task_name,
                              mst.start_date,
                              rp.definition as progress,
                              (CASE WHEN mst.duration IS NULL THEN
                              (SELECT 
                              count(*) 
                              FROM   
                                generate_series(mst.start_date , now(), interval  '1 day') the_day
                              WHERE  
                              extract('ISODOW' FROM the_day) < 6) ELSE mst.duration END) AS duration,
                              mst.status_id,
                              rs.definition,u.full_name 
                              from master_sub_task mst
                              join master_task mt on mt.id=mst.id_task
                              join users u on u.id=mst.pic_id
                              join ref_status rs on rs.id=mst.status_id
                              join ref_progress_status rp on rp.id=mst.progress_id
                              where mt.id_project=$ts->id_project and mst.id_task=$ts->id");
                           @endphp
                           @foreach($subtask as $sts)
                           <tr>
                               <td>
                                   {{$sts->sub_task_name}}
                               </td>
                               <td>
                                   {{$sts->full_name}}
                               </td>
                                <td>
                                   {{$sts->duration}}
                               </td>
                                <td>
                                   {{$sts->definition}}
                               </td>
                               <td>
                                   {{$sts->progress}}
                               </td>
                           </tr>
                         @endforeach  
                       @endforeach  
                       </tbody>
                       
                   </table>

                  </div>
                </div>
              </div>
            </div>
    </div>
     @include('projects.add')
    @include('projects.edit')
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/pages/_projects.js') }}"></script>
@stop
