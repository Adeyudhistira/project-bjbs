<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
          content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
    <meta name="keywords"
          content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Error 403 - Akses di Tolak</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,400,500,700"
          rel="stylesheet">

    <link href="{{ asset('css/vendors.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pages/error.css') }}" rel="stylesheet">
</head>
<body class="vertical-layout vertical-content-menu 1-column   menu-expanded blank-page blank-page"
      data-open="click" data-menu="vertical-content-menu" data-col="1-column">
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-md-4 col-10 p-0">
                        <div class="card-header bg-transparent border-0">
                            <h2 class="error-code text-center mb-2">403</h2>
                            <h3 class="text-uppercase text-center">Akses Ditolak !</h3>
                        </div>
                        <div class="card-content">
                            <h4 align="center">Anda tidak berhak untuk mengakses halaman ini, info lebih lanjut silahkan kontak Web Administrator</h4>
                            <div class="row py-2">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <a href="{{ route('home') }}" class="btn btn-primary btn-block"><i
                                                class="ft-home"></i> Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
</body>
</html>