@section('content')
 <section class="row">
          <div class="col-12">
      
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Daftar Project {{$status}}</h4>
                <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
              </div>
              <div class="card-content">
                <div class="card-body">
                 <table class="table table-striped ">
                       <thead>
                        <tr>
                           <th>
                              Nama Sub Task
                           </th>
                           <th>
                              Nama PIC
                           </th>
                           <th align="center">
                              Durasi (hari) 
                           </th>
                           <th>
                              Status 
                           </th>
                           <th>
                              Progress 
                           </th>
                       </tr>    
                       </thead>
                       <tbody>
                        @foreach($task as $pm)
                         <tr>
                          <td style="font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;" colspan="5"><b>{{ $pm->project_name }}</b></td>
                        </tr>
                        @php
                           $mtask=DB::table('master_task')
                           ->select('master_task.id','master_task.task_name','master_task.id_task')
                           ->join('master_sub_task','master_sub_task.id_task','=','master_task.id')
                           ->where('master_task.status_id',1)
                           ->where('id_project',$pm->id)
                           ->paginate(2);
                        @endphp
                        @foreach($mtask as $ts)
                        <tr>
                          <td style="font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;" colspan="5"> &nbsp;&nbsp;&nbsp;&nbsp;{{$ts->task_name}}</td>
                        </tr>

                        @php
                        if($idstatus==1){

                          $subtask=\DB::select("select mst.sub_task_name,u.full_name,mst.duration,ps.definition as progress,rs.definition as status 
                          from master_sub_task mst
                          join users u on u.id=mst.pic_id
                          join ref_progress_status ps on ps.id=mst.progress_id 
                          join ref_status rs on rs.id=mst.status_id
                          WHERE mst.progress_id=2 and id_task=$ts->id ");

                          }elseif($idstatus==2){

                          $subtask=\DB::select("select mst.sub_task_name,u.full_name,mst.duration,ps.definition as progress,rs.definition as status 
                          from master_sub_task mst
                          join users u on u.id=mst.pic_id
                          join ref_progress_status ps on ps.id=mst.progress_id 
                          join ref_status rs on rs.id=mst.status_id
                          WHERE mst.progress_id=5 and id_task=$ts->id ");
  
                        }elseif($idstatus==3){

                          $subtask=\DB::select("select mst.sub_task_name,u.full_name,mst.duration,ps.definition as progress,rs.definition as status 
                          from master_sub_task mst
                          join users u on u.id=mst.pic_id
                          join ref_progress_status ps on ps.id=mst.progress_id 
                          join ref_status rs on rs.id=mst.status_id
                          WHERE mst.progress_id=5 and mst.is_approve=false and id_task=$ts->id ");
  
                      }else{

                          $subtask=\DB::select("select mst.sub_task_name,u.full_name,mst.duration,ps.definition as progress,rs.definition as status 
                          from master_sub_task mst
                          join users u on u.id=mst.pic_id
                          join ref_progress_status ps on ps.id=mst.progress_id 
                          join ref_status rs on rs.id=mst.status_id
                          WHERE id_task=$ts->id ");
  
                      }
                        

                        @endphp


                        @foreach($subtask as $mst)
                         <tr>
                           <td>
                             {{$mst->sub_task_name}}
                           </td>
                           <td>
                             {{$mst->full_name}}
                           </td>
                           <td>
                             {{$mst->duration}}
                           </td>
                           <td>
                             {{$mst->status}}
                           </td>
                           <td>
                             {{$mst->progress}}
                           </td>
                         </tr>
                        @endforeach

                        @endforeach
                        
                        @endforeach
                        
                       </tbody>
                   </table>
                   <nav aria-label="Page navigation">
                      <ul class="pagination justify-content-center pagination-separate pagination-round pagination-flat pagination-sm">
                              <?php echo $mtask->render(); ?>
                      </ul>
                    </nav>
                </div>
              </div>
            </div>
          </div>
        </section>

@endsection
