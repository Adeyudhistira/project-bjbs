 <div class="modal animated slideInRight text-left" id="_edit" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-blue">
                                  <h4 class="modal-title white" id="myModalLabel76">Edit Task</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <div id="tasku" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:40%;padding:2px;"><img src="{{asset('images/loader.gif')}}" width="110" height="110" /></div>
                                   <form id="form_edit_lm"  name="formEdit" role="form" class="form-validation" data-parsley-validate="">
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Nama Task</label>
                                          <input type="text" readonly class="form-control" id="task_name1" name="task_name" data-parsley-required>
                                          <input type="hidden" class="form-control" id="id" name="id" data-parsley-required>
                                          <input type="hidden" class="form-control" id="id_project1" name="id_project" data-parsley-required>
                                          <input type="hidden" class="form-control" id="end_date1" name="end_date" data-parsley-required>
                                          <input type="hidden" class="form-control" id="duration1" name="duration" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Progress Task</label>
                                          
                                          <select style="width:470px" name="progressstatus_id" class=" form-control" data-parsley-required id="progressstatus_id1">
                                             
                                          </select>
                                     
                                      </div>
                                       
                                      <div class="form-group">
                                        <label for="message-text" class="col-form-label">Dokumen Pendukung</label>
                                      
                                          <input type="file" class="form-control-file" id="url_doc1" name="url_doc">
                                        
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Keterangan</label>
                                         
                                         <textarea class="form-control" id="notes1" name="notes" rows="3"></textarea>
                                      </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-outline-info"  onclick="processUpdate()">Update</button>
                                </div>
                              </div>
                            </div>
                          </div>