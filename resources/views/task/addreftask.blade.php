 <div class="modal animated slideInRight text-left" id="_create1" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel76">Tambah Task</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  
                                   <form id="_create_form1">
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Definition</label>
                                          <input type="text" class="form-control" id="definition" name="definition" data-parsley-required>
                                      </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-outline-primary"  onclick="processInsert1()">Save</button>
                                </div>
                              </div>
                            </div>
                          </div>