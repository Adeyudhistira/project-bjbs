@section('content')
 <section class="row">
          <div class="col-12">
            <div class="page-title mb-4 d-flex align-items-center">
                <div class="choose-form-tab d-inline-block">
                <ul class="nav nav-form-custom nav-form-sm float-right" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        @role('SPV')
                        <button type="button" class="btn btn-info" data-toggle="modal"
                         onclick="tambah()">Tambah Task</button>
                        @endrole
                    </li>
                </ul>
            </div>
        </div>
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Daftar Task</h4>
                <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <!-- Task List table -->
                  <div class="table-responsive">
                    <table id="t_user" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle table-striped dataTable">
                      <thead>
                        <tr>
                          <th>&nbsp;</th>
                          <th>Nama Pekerjaan</th>
                          <th>Dates</th>
                          <th>Status Terakhir</th>
                          <th>Progress</th>
                          <th>PIC</th>
                          <th>Dokumen Pendukung</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                        <tbody>
                        @foreach($task as $data)
                         <tr>
                          <td>
                           <i class='icon-note'></i>
                          </td>
                          <td>
                                                    
                          <a href="#" class="text-bold-600" onclick="ajaxMenu(event,'subtask/table/{{$data->id}}')">{{$data->task_name}}</a>

                          @php
                          $st=DB::select("SELECT sub_task_name
                              FROM master_sub_task st join master_task mt on mt.id=st.id_task where st.status_id in (1,2) and st.id_task=$data->id and mt.id_project=$data->id_project");
                          @endphp
                          
                         <p class="text-muted"> 
                          @if(count($st))
                          @foreach($st as $dt)
                          <div style="word-wrap: break-word; width:auto;">
                            {{$dt->sub_task_name}},

                          </div> 
                          @endforeach
                          @endif
                          </p>
                          </td>
                          <td>
                             <h6 class="mb-0">
                              <span class="text-bold-600">{{$data->project_name}}</span>
                            </h6>
                          </td>
                          <td>
                            @if($data->progress_id==1)
                              <span class="badge badge-default badge-danger">{{$data->definition}}</span>
                            @elseif($data->progress_id==2)
                              <span class="badge badge-default badge-warning">{{$data->definition}}</span>
                            @else
                              <span class="badge badge-default badge-success">{{$data->definition}}</span>
                            @endif
                            
                          </td>
                          <td>
                            <div class="progress progress-sm">
                              <div aria-valuemin="{{$data->progress_pct}}" aria-valuemax="100" class="progress-bar bg-gradient-x-success"
                              role="progressbar" title="{{$data->progress_pct}}%" style="width:{{$data->progress_pct}}%" aria-valuenow="{{$data->progress_pct}}"></div>
                            </div>
                          </td>
                          <td class="text-center">
                            {{$data->full_name}}
                          </td>
                          <td class="text-center">
                            <a href="{{route('downloadtask',$data->url_doc)}}" title="{{$data->url_doc}}">{{$data->url_doc}}</a>
                          </td>
                          <td>
                            @role('Admin')
                           <i class='fa fa-pencil' style='color:blue;' title='Edit'  onclick='editshow({{$data->id}},{{$data->is_parallel}})'></i>
                           @endrole
                              @role('SPV')
                            <i style='color:red;' title='Hapus' onclick="hapus({{$data->id}},'task/delete');" class='fa fa-trash'></i>
                              @endrole
                          </td>
                        </tr>
                  @endforeach
                      </tbody>
                    </table>
                  </div>
                  <!--/ Task List table -->
                </div>
              </div>
            </div>
          </div>
        </section>
@include('task.add')
@include('task.edit')
@include('task.addreftask')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('assets/js/_task.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {

       $.ajax({
        type: 'GET',
        url: 'select/progress',
        success: function (res) {
            var data = $.parseJSON(res);
            _items = "<option value=''>Pilih Progress Task</option>";
            $.each(data, function (k,v) {
                _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
            });

            $('#progressstatus_id1').html(_items);
        }
    });

   }); 
</script>
@endsection