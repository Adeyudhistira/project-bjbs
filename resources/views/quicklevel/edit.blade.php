 <div class="modal animated slideInRight text-left" id="_edit" tabindex="-1"
                          role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-blue">
                                  <h4 class="modal-title white" id="myModalLabel76">Edit Quick Level</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  
                                   <form id="form_edit_lm"  name="formEdit" role="form" class="form-validation" data-parsley-validate="">
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Definition</label>
                                          <input type="text" class="form-control" id="definition1" name="definition" data-parsley-required>
                                          <input type="hidden" class="form-control" id="id" name="id" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Point</label>
                                          <input type="text" onkeypress="return hanyaAngka(event)" class="form-control" id="point1" name="point" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Selisih</label>
                                          <input type="text" class="form-control" id="selisih1" name="selisih" data-parsley-required>
                                      </div>
                                      <div class="form-group">
                                          <label for="message-text" class="col-form-label">Seq</label>
                                          <input type="number" onkeypress="return hanyaAngka(event)" class="form-control" id="seq1" name="seq" data-parsley-required>
                                      </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-outline-info"  onclick="processUpdate()">Update</button>
                                </div>
                              </div>
                            </div>
                          </div>