@section('content')
 <div class="row">
     <div class="col-12">
        
        <div class="card">
           <div class="card-header bg-blue">
                <h4 class="card-title text-white">Laporan : Progress Program Kerja Per Unit (Rekap)</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
        <div class="card-content collpase show">
              <div class="card-body card-dashboard">
                <p style="color: red;">*Pilih Salah Satu Team</p>
                
                  <form class="form-horizontal" id="form-filter" method="GET" action="{{route('excel1')}}">
                    {{ csrf_field() }}
                    <table>
                @foreach($team as $item)
                <tr>
                  <td>
                <div class="form-group {{ $errors->has('customCheck[]') ? 'has-error' : ''}}">
                        
                <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" value='{{$item->id}}' name="customCheck[]" id="customCheck_{{$item->id}}" type="checkbox">
                        <label class="custom-control-label" for="customCheck_{{$item->id}}">{{$item->definition}}</label>
                      </div>
                    </div>
                  <td>
                </tr>
                @endforeach
                </table>
                    <div class="modal-footer">
                       <button type="submit" value="pdf" name="pdf" id="pdf" title="Export as PDF" class="btn btn-danger"> <i class="fa fa-file-pdf-o"></i> PDF</button>
                       <button type="submit" value="excel" name="excel" id="excel" title="Export as Excel" class="btn btn-success"> <i class="fa fa-file-excel-o"></i> Excel</button>
                    </div>
                 </form>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">   
function processexcel() {
    var _generate_form = $('#lap');
    _generate_form.parsley().validate();
    var _form_data = new FormData(_generate_form[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
    if(_generate_form.parsley().isValid()){

        //loader.html('<image src="{{ url('/images/loading.gif') }}">');
       // loader.src='/images/loading.gif';
//document.getElementById("gen").style.display = "block";
        $.ajax({
        type: 'get',
        url: base_url + '/laporan/excel1',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                $('#lap').modal('hide');
                //hideLoading(true);
                //document.getElementById("gen").style.display = "none";
                //swal("Berhasil!", "Generate Berhasil", "success");
                swal({
                    title: "Berhasil!",
                    text: "Generate Berhasil",
                    icon: "warning",
                    showCancelButton: true,
                    buttons: {
                        confirm: {
                            text: "Tutup",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {
                    if (isConfirm) {
                          var loader = $('.ui.dimmable #mainLoader');    
                            loader.dimmer({closable: false}).dimmer('show');
                             location.reload();
                      } 
                });
               // showInfo(parse.msg);
                _create_form[0].reset();
            }else if(parse.code == 2){
                //hideLoadingPartial(true);
               $('#_generate').modal('hide');
                document.getElementById("gen").style.display = "none";
               // swal("Error!", "Pilih Salah Satu Task", "info");
                swal({
                    title: "Info!",
                    text: "Pilih Salah Satu Task",
                    icon: "info",
                    showCancelButton: true,
                    buttons: {
                        confirm: {
                            text: "Tutup",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {
                    if (isConfirm) {
                          var loader = $('.ui.dimmable #mainLoader');    
                            loader.dimmer({closable: false}).dimmer('show');
                             location.reload();
                      } 
                });
                //showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                //hideLoadingPartial(true);
                showError(li);
            }

        }

    }).always(function () {
     var loader = $('.ui.dimmable #mainLoader');    
    loader.dimmer({closable: false}).dimmer('show');
  //   location.reload();
    });

        
    } 
    
}
</script>
@stop