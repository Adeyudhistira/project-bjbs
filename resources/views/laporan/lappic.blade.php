@section('content')
 <div class="row">
     <div class="col-12">
        
        <div class="card">
           <div class="card-header bg-blue">
                <h4 class="card-title text-white">Laporan : Progress Program Kerja Per PIC (Rekap)</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
        <div class="card-content collpase show">
              <div class="card-body card-dashboard">
                <p style="color: red;">*Pilih Salah Satu PIC</p>
              <form class="form-horizontal" id="form-filter" method="GET" action="{{route('excel2')}}">
                {{ csrf_field() }}
                <table>
                @foreach($team as $item)

                @php
                 $user = \DB::select("select * from users where status=1 and team_id=$item->id");
                @endphp
                <tr>
                <td>
                <p><b>{{$item->definition}}</b></p>
                </td>
                </tr>
                @foreach($user as $users)
                <tr>
                  <td>
                
                <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" value='{{$users->id}}' name="customCheck[]" id="customCheck_{{$users->id}}" type="checkbox">
                        <label class="custom-control-label" for="customCheck_{{$users->id}}">{{$users->full_name}}</label>
                      </div>
                  <td>
                </tr>
                @endforeach
                @endforeach
                </table>
                    <div class="modal-footer">
                       <button type="submit" value="pdf" name="pdf" id="pdf" title="Export as PDF" class="btn btn-danger"> <i class="fa fa-file-pdf-o"></i> PDF</button>
                       <button type="submit" value="excel" name="excel" id="excel" title="Export as Excel" class="btn btn-success"> <i class="fa fa-file-excel-o"></i> Excel</button>
                    </div>
                 </form>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
