<style type="text/css">
.page-break {
    page-break-after: always;
}
                .tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #000 1;width: 100%; }
                .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
            </style>
@if(!empty($module))
    @foreach($module as $dt)
<table  class="tg" border="1">
<tr>
    <th>
        Nama Program Kerja
    </th>
    <td>
        {{$dt->project_name}}
    </td>
</tr>
<tr>
    <th>jenis</th>
    <td>{{$dt->definition}}</td>
</tr>
<tr>
    <th>Unit</th>
    <td>{{$dt->team}}</td>
</tr>
<tr>
    <th>PIC</th>
    <td>{{$dt->full_name}}</td>
</tr>
<tr>
    <th>Jumlah Task</th>
    <td>{{$dt->taskprog}}</td>
</tr>
<tr>
    <th>Jumlah Sub Task</th>
    <td>{{$dt->subtask}}</td>
</tr>
</table>
@endforeach
@endif


<table  class="tg" border="1">
    <thead>
        <tr><th colspan="5"><h3></h3>Detail Sub Task</th></tr>
        <tr>
            <th>No</th><th>Sub Task</th><th>Tanggal Input</th><th>Status</th><th>Tanggal Dokumen (Keterangan)</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($module))
            @php $no=0; @endphp
            @foreach($module as $dt)
            @php
            $query = \DB::select("select mst.sub_task_name,mst.created_at,rps.definition as status,mst.end_date,mst.notes from master_sub_task mst 
            join master_task mt on mt.id=mst.id_task
            join ref_progress_status rps on rps.id=mst.progress_id
            where mt.id_project=$dt->id");
            @endphp
            @foreach($query as $item)
            @php $no++; @endphp
            <tr>
                <td>{{$no}}</td>
                <td>{{$item->sub_task_name}}</td>
                <td>{{date('d M Y',strtotime($item->created_at))}}</td>
                <td>{{$item->status}}</td>
                <td>{{isset($item->end_date) ? date('d m Y',strtotime($item->end_date)) : '-'}}({{isset($item->notes) ? $item->notes : '-'}})</td>
            </tr>
            @endforeach
            @endforeach
        @endif
    </tbody>
    
</table>