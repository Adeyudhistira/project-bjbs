<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>

<table  class="tg" border="1">
	<thead>
		<tr><th colspan="9"><h3></h3>{{$title}}</th></tr>
		<tr>
			<th>No</th><th>Unit</th><th>Program Kerja</th><th>Status</th><th>Task Prog</th>
			<th>Sub Task</th><th>Sub Task Done</th><th>%Sub Task Done</th><th>PIC</th>
		</tr>
	</thead>
	<tbody>
		@if(!empty($module))
			@php $no=0; @endphp
			@foreach($module as $dt)
			@php $no++; @endphp
			<tr>
				<td>{{$no}}</td>
				<td>{{$dt->definition}}</td>
				<td>{{$dt->project_name}}</td>
				<td>{{$dt->status}}</td>
				<td>{{$dt->taskprog}}</td>
				<td>{{$dt->subtask}}</td>
				<td>{{$dt->subtaskprog}}</td>
				<td>{{$dt->pdone}}%</td>
				<td>{{$dt->full_name}}</td>
			</tr>
			@endforeach
		@endif
	</tbody>
	
</table>