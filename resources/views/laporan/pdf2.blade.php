<style type="text/css">
.page-break {
    page-break-after: always;
}
                .tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #000 1;width: 100%; }
                .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
            </style>
<table  class="tg" border="1">
    <thead>
        <tr><th colspan="9"><h3></h3>{{$title}}</th></tr>
        <tr>
            <th>No</th><th>Unit</th><th>Program Kerja</th><th>Status</th><th>Task Prog</th>
            <th>Sub Task</th><th>Sub Task Done</th><th>%Sub Task Done</th><th>PIC</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($module))
            @php $no=0; @endphp
            @foreach($module as $dt)
            @php $no++; @endphp
            <tr>
                <td>{{$no}}</td>
                <td>{{$dt->definition}}</td>
                <td>{{$dt->project_name}}</td>
                <td>{{$dt->status}}</td>
                <td>{{$dt->taskprog}}</td>
                <td>{{$dt->subtask}}</td>
                <td>{{$dt->subtaskprog}}</td>
                <td>{{$dt->pdone}}%</td>
                <td>{{$dt->full_name}}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
    
</table>
