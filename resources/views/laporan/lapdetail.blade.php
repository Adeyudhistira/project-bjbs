@section('content')
 <div class="row">
     <div class="col-12">
        
        <div class="card">
           <div class="card-header bg-blue">
                <h4 class="card-title text-white">Detail Program Kerja</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
        <div class="card-content collpase show">
              <div class="card-body card-dashboard">
              <table id='lap' class="table table-striped table-bordered source-data">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>Program Kerja</th>
                    <th>Jenis</th>
                    <th>Unit</th>
                    <th>PIC</th>
                    <th>Jumlah Task</th>
                    <th>Jumlah Sub Task</th>
                    <th>Export</th>
                  </tr>
                </thead>
                <tbody>
                  @php $no=0; @endphp
                @foreach($team as $item)
                  @php $no++; @endphp
                  <tr>
                    <td>{{$no}}</td>
                    <td>{{$item->project_name}}</td>
                    <td>{{$item->definition}}</td>
                    <td>{{$item->team}}</td>
                    <td>{{$item->full_name}}</td>
                    <td>{{$item->taskprog}}</td>
                    <td>{{$item->subtask}}</td>
                    <td>
                      <a href="{{route('excel5',$item->id)}}" class="text-success" data-toggle="tooltip" data-placement="bottom"
                        title="Export as Excel"><i class="fa fa-file-excel-o"></i></a>
                        <a href="{{route('pdf5',$item->id)}}" class="text-danger" data-toggle="tooltip" data-placement="bottom"
                        title="Export as Pdf"><i class="fa fa-file-pdf-o"></i></a>
                      </td>
                  </tr>
                @endforeach
                  
                </tbody>
                </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$('#lap').DataTable({
});
</script>
@stop