<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>

<table  class="tg" border="1">
	<thead>
		<tr><th colspan="9"><h3></h3>{{$title}}</th></tr>
		<tr>
			<th>No</th><th>Unit</th><th>Jml_Proker</th><th>Done</th><th>On Progress</th>
			<th>InActive</th><th>%Done</th><th>%On Progress</th><th>%InActive</th>
		</tr>
	</thead>
	<tbody>
		@if(!empty($module))
			@php $no=0; @endphp
			@foreach($module as $dt)
			@php $no++; @endphp
			<tr>
				<td>{{$no}}</td>
				<td>{{$dt->unit}}</td>
				<td>{{$dt->jml_proker}}</td>
				<td>{{$dt->done}}</td>
				<td>{{$dt->onprogress}}</td>
				<td>{{$dt->inactive}}</td>
				<td>{{$dt->pdone}}%</td>
				<td>{{$dt->ponprogress}}%</td>
				<td>{{$dt->pinactive}}%</td>
			</tr>
			@endforeach
		@endif
	</tbody>
	
</table>