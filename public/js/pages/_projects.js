$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


        //$('#start_date').pickadate();
        $('#range_date').daterangepicker({
            locale:{
                format: 'YYYY-MM-DD'
            }
        });
        $('.money').mask('000.000.000.000.000', {reverse: true});
       // $('#e_start_date').pickadate();
        $('#end_date').pickadate();
       // $('#e_target_date').pickadate();


        $('.selectize-select').selectize({
            create: true,
            sortField: 'text'
        });

        var _create_form = $('#_create_form');
        var modal_upd= $('#_edit');
        var modal_task = $('#_list');

        var t_project = $('#t_project').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: 'projects/table'
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'project', name: 'project', width: "300px"},
                {data: 'start_date', name: 'start_date'},
                {data: 'target_date', name: 'target_date'},
                {data: 'end_date', name: 'end_date'},
                {data: 'progress_pct', name: 'progress_pct'},
                {data: 'pmo', name: 'pmo'},
                {data: 'duration', name: 'duration', width: "50px"},
                {data: 'status', name: 'status', width: "50px"},
                {data: 'action', name: 'action', sortable: false, searchable: false, width: "150px"}
            ]
        });
        var _create_modal = $('#_create');


function tambah() {
$("#wait").css("display", "block");
            var _items = "";
            $.ajax({
                type: 'GET',
                url: base_url+'/select/projecttype',
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Jenis Project...</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
                    });
                    $('#project_type_id').html(_items);

                }
            });

            $.ajax({
                type: 'GET',
                url: base_url+'/select/client',
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Client...</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
                    });
                    $('#client_id').html(_items);
                    $("#wait").css("display", "none");
                }
            });

            $('#_create').modal('show');
        }

$('#client_id').on('change', function (v) {
    var _items = "";
  $("#wait").css("display", "block");
   $.ajax({
                type: 'GET',
                url: base_url+'/select/teamid1',
                success: function (res) {

                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Team...</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
                    });
                    $('#team_id').html(_items);

                 $("#wait").css("display", "none"); 
                }
        });
});

$('#team_id').on('change', function (v) {
    var _items = "";
    $("#wait").css("display", "block");
            $.ajax({
                type: 'GET',
                url: base_url+'/select/userid/'+ this.value,
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Project Manager...</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.id+"'>"+v.full_name+"</option>";
                    });
                    $('#pm_id').html(_items);
                     $("#wait").css("display", "none"); 
                }
            });        
});

function processInsert() {
     _create_form.parsley().validate();
    var _form_data = new FormData(_create_form[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
    if(_create_form.parsley().isValid()){

        
       
        //loader.html('<image src="{{ url('/images/loading.gif') }}">');
       // loader.src='/images/loading.gif';
document.getElementById("wait").style.display = "block";
        $.ajax({
        type: 'POST',
        url: base_url + '/projects',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                _create_modal.modal('hide');
                hideLoading(true);
                showInfo(parse.msg);
                _create_form[0].reset();
            }else if(parse.code == 2){
                hideLoadingPartial(true);
                showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }

        }

    }).always(function () {
     var loader = $('.ui.dimmable #mainLoader');    
    loader.dimmer({closable: false}).dimmer('show');
         location.reload();
    });



        /*submitdata('task',_form_data,t_user,_create_modal,_create_form[0])
        .always(function (dataOrjqXHR, textStatus, jqXHRorErrorThrown) { 
            location.reload();

        });*/
        
    } 
    
}

 

function editshow(id,iduser,idstatus) {
    var _items = "";
    var _items1 = "";
    $("#wait1").css("display", "block");
      $.ajax({
                type: 'GET',
                url: base_url+'/select/userteam/'+id,
                success: function (res) {
                var data = $.parseJSON(res);
                _items = "<option value=''>Pilih PIC</option>";
                $.each(data, function (k,v) {
                 var cek;  
                    if(v.id==iduser){
                        cek='selected';
                    }else{
                        cek=false
                    }
                    _items += "<option value='"+v.id+"' "+ cek +">"+v.full_name+"</option>";
                });
                $('#e_pm_id1').html(_items);
                $("#wait1").css("display", "none");
                }
            });



            $.ajax({
            type: 'GET',
            url: base_url+'/select/status',
            success: function (res) {
            var data = $.parseJSON(res);
            _items1 = "<option value=''>Pilih Status</option>";
            $.each(data, function (k,v) {
             var cek;  
                    if(v.id==idstatus){
                        cek='selected';
                    }else{
                        cek=false
                    }   
            _items1 += "<option value='"+v.id+"' "+ cek +">"+v.definition+"</option>";
            });

            $('#e_status_id1').html(_items1);
            }
            });     

    $.ajax({
        type: 'GET',
        url: base_url+'/projects/edit/'+id,
        success: function (res) {
        var data = $.parseJSON(res);
            $.each(data, function (k,v) {
                        
                       // document.form1.e_url_doc.value =v.url_doc;
                         //var input = document.getElementById ("e_url_doc").val(v.url_doc);
                         //console.log(input);
                          //$('input[type=file]').change(function () {
                               // });
                       
                       
                        //console.log(base_url+"/public/support_docs/"+v.url_doc);   
                       
                        $('#id').val(v.id);
                        $('#e_project_name').val(v.project_name);
                        $('#e_start_date').val(v.start_date);
                        $('#e_target_date').val(v.target_date);
                        $('#e_url_doc').val(v.url_doc); 
                        console.log("aa");
                        console.log(v.is_parallel);
                        if(v.is_parallel) {
                        radiobtn = document.getElementById("is_paralel1");
                        radiobtn.val(v.is_parallel);
                        radiobtn.checked = true;
                        console.log('1');
                        }else{
                        console.log('0');
                        radiobtn = document.getElementById("is_paralel1");
                        radiobtn.val(v.is_parallel);
                        radiobtn.checked = false;
                        }

 
                    });

                        
                  
        }

    });

  
             

modal_upd.modal('show');
}

function processUpdate() {
    var form = $('#_edit_form'),
        formData = new FormData(form[0]);

    form.parsley().validate();
     //alert('ab');
    if (form.parsley().isValid()) {
        document.getElementById("wait1").style.display = "block";
       $.ajax({
        type: 'POST',
        url: base_url + '/projects/update',
        data: formData,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                modal_upd.modal('hide');
             //   hideLoading(true);
                showInfo(parse.msg);
                form[0].reset();
            }else if(parse.code == 2){
                hideLoadingPartial(true);
                showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    }).always(function () {
       var loader = $('.ui.dimmable #mainLoader');    
    loader.dimmer({closable: false}).dimmer('show');
        location.reload();
    });
       // submitdata('task/update',formData,t_user,modal_upd,form[0]);
    }
}

        function showtask(id) {
            var body = $('#list_body');

            $.ajax({
                type: 'GET',
                url: base_url + '/projects/task/'+id,
                success: function (res) {
                    var data = $.parseJSON(res);
                    var content = "", status = "";
                    if(data.length != 0){
                        content = '<ul class="list-group">';
                        $.each(data, function (k,v) {
                            switch(v.status_id){
                                case 1:
                                    status = '<span class="badge badge-success float-right">Active</span>';
                                    break;
                                case 2:
                                    status = '<span class="badge badge-secondary float-right">Inactive</span>';
                                    break;
                                case 3:
                                    status = '<span class="badge badge-danger float-right">Dropped</span>';
                                    break;
                            }
                            content += '<li class="list-group-item">'+status+' '+v.task_name+'</li>';
                        });
                        content += '</ul>';
                    }else {
                        content = '<div class="alert alert-info border-0 alert-dismissible mb-2" role="alert">\n' +
                            '                          <h4 class="alert-heading mb-2">Informasi !</h4>\n' +
                            '                          <p>Project ini belum memiliki Task. Untuk menambahkan Task, silahkan klik menu <strong>Master</strong> <i class="fa fa-arrow-circle-right"></i> <strong>Task</strong> </p>\n' +
                            '                        </div>';
                    }
                    body.html(content);
                }
            });

            modal_task.modal('show');
        }


function showdet(){
    $('#aa').show();
}

var _detail = $('#_detail');
function detail() {
_detail.modal('show');
}

$('#e_target_date').on('change', function (v) {

var tglawal = this.value;
//var awal = str.split('/',3);
//hasil variable awal akan kyk begini : 14,04,2010

var tglakhir = $('#e_start_date').val();
//var selesai = str2.split('/',3);
//hasil variable selesai akan kyk begini : 13,04,2010

//udah gitu sambung lagi bagian2 yang udah di split tadi ke format yyyy-mm-dd,
//var mulai = awal[2] + '-' +awal[1] + '-' + awal[0];
//var akhir = selesai[2] + '-' +selesai[1] + '-' + selesai[0];

//udah gitu rubah ke bentuk date() sekaligus bandingin,
//console.log(str);
//console.log(str2);
if( tglakhir > tglawal) {
alert('Tanggal target harus lebih besar dari tanggal mulai.');
$('#e_target_date').val(tglakhir);
}else if(!tglakhir){
alert('Tanggal Mulai Masih Kosong');
$('#e_start_date').val(tglakhir);
}

}); 

$('#e_start_date').on('change', function (v) {

var tglawal =$('#e_target_date').val();
//var awal = str.split('/',3);
//hasil variable awal akan kyk begini : 14,04,2010

var tglakhir =  this.value;
//var selesai = str2.split('/',3);
//hasil variable selesai akan kyk begini : 13,04,2010

//udah gitu sambung lagi bagian2 yang udah di split tadi ke format yyyy-mm-dd,
//var mulai = awal[2] + '-' +awal[1] + '-' + awal[0];
//var akhir = selesai[2] + '-' +selesai[1] + '-' + selesai[0];

//udah gitu rubah ke bentuk date() sekaligus bandingin,
//console.log(str);
//console.log(str2);
if(!tglawal){
$('#e_target_date').val(tglakhir);
}else if(tglakhir > tglawal){
alert('Tanggal mulai harus lebih kecil dari tanggal target.');
$('#e_target_date').val(tglakhir);    
}

});    


function hapus(id,url) {
 var loader = $('.ui.dimmable #mainLoader');    
    loader.dimmer({closable: false}).dimmer('show');
 $.ajax({
                type: 'GET',
                url: base_url+'/projects/cektask/'+id,
                success: function (res) {
                    var data = $.parseJSON(res);
                    $.each(data, function (k,v) {


                        //task ada
                      if(v.id_project != null){
                        loader.dimmer({closable: false}).dimmer('hide');
                      swal({
                            title: "Project Telah Ada Task!",
                            text: "Yakin Hapus Data Project ?",
                            icon: "warning",
                            showCancelButton: true,
                            buttons: {
                                cancel: {
                                    text: "Cancel",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                                confirm: {
                                    text: "Delete",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                        }).then(isConfirm => {
                            if (isConfirm) {
                                  del(id,url);
                              } else {
                                swal("Cancelled", "Your imaginary file is safe", "error");
                            }
                        });

                        }
                    });
                //task kosng
                loader.dimmer({closable: false}).dimmer('hide');
                  if(data.length==0){
                    swal({
                            title: "Anda Yakin?",
                            text: "Data Akan Dihapus!",
                            icon: "warning",
                            showCancelButton: true,
                            buttons: {
                                cancel: {
                                    text: "Cancel",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                                confirm: {
                                    text: "Delete",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                        }).then(isConfirm => {
                            if (isConfirm) {
                                  del(id,url);
                              } else {
                                swal("Cancelled", "Your imaginary file is safe", "error");
                            }
                        });
                    }

                }


            });


}

function del(id,url) {
    url = url + '/' + id;
    $('.modal').each(function () {
        $(this).modal('hide');
    });
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        if (response.rc == 0) {
            // dataTable.ajax.reload();
            //t_user.draw();

            swal("Berhasil!", "Data Sudah Dihapus", "success");
             var loader = $('.ui.dimmable #mainLoader');    
        loader.dimmer({closable: false}).dimmer('show');
            location.reload()
        } else {
            swal("Gagal", response.rm, "error");
        }
    }).fail(function (response) {
        hideLoading(false);
        swal("Error", "Terjadi Kesalahan", "error");
    });
}


function generate(){
    alert("aa");
}

  var defaultData = [{
        text: 'Parent 1',
        href: '#parent1',
        tags: ['4'],
        nodes: [{
            text: 'Child 1',
            href: '#child1',
            tags: ['2']
        }, {
            text: 'Child 2',
            href: '#child2',
            tags: ['0']
        }]
    }];

      // Check / Uncheck All
    var $checkableTree = $('#checkable-tree').treeview({
        data: defaultData,
        showIcon: false,
        showCheckbox: true,
    });

    // Check/uncheck all
    $('#btn-check-all').on('click', function(e) {
        $checkableTree.treeview('checkAll', {
            silent: $('#chk-check-silent').is(':checked')
        });
    });

    $('#btn-uncheck-all').on('click', function(e) {
        $checkableTree.treeview('uncheckAll', {
            silent: $('#chk-check-silent').is(':checked')
        });
    });


    $(".acidjs-css3-treeview").delegate("label input:checkbox", "change", function() {
    var
        checkbox = $(this),
        nestedList = checkbox.parent().next().next(),
        selectNestedListCheckbox = nestedList.find("label:not([for]) input:checkbox");
 
    if(checkbox.is(":checked")) {
        return selectNestedListCheckbox.prop("checked", true);
    }
    selectNestedListCheckbox.prop("checked", false);
});

$('#model_id').on('change', function (v) {
    var _items = "";
      document.getElementById("gen").style.display = "block";
      console.log(this.value);
if(this.value!=0){
 $.ajax({
                type: 'GET',
                url: base_url+'/select/taskid/'+ this.value +'/'+ $('#id_project').val(),
                success: function (res) {

                    var data = $.parseJSON(res);
           
                    //_items = "<ul>";
                    //_items += "<li>";
                    var no=0;
                   _items += "<ul>";
                   _items +=" <select  id=pic_id1_"+v.idtask+" required name='pic_id'>"; 
                        _items +="<option value=''>Pilih PIC</option>";
                        $.each(data[0].user, function (i,v) {
                            _items +="<option value='"+ v.id +"'>"+ v.full_name +"</option>";
                            });
                            _items +="</select>";
                           
                            _items +=" <select required id=sub_task_level_id1_"+v.idtask+" name='sub_task_level_id'>"; 
                             _items +="<option value=''>Pilih Level Task</option>";
                            $.each(data[0].leveltask, function (i,v) {
                            _items +="<option value='"+ v.id +"'>"+ v.definition +"</option>";
                            });
                            _items +="</select>";   

                    
                    $.each(data, function (k,v) { 
                        console.log(v.task);
                       var taskid=v.idtask;
               // for(var i =0 ; i<no; i++){   
                     

                       _items += "<li>";    
                        _items += "<input type='checkbox' id=node-"+no+" /><label><input type='checkbox' onclick=cek("+v.idtask+"); value="+v.idtask+"  id='idtask"+v.idtask+"' name='idtask[]' /><span></span></label><label for=node-"+no+">"+ v.task +"</label>";
                        _items +=" <input type='date' id=target_date1_"+v.idtask+"  min='"+ data[0].startdate +"' max='"+ data[0].targetdate +"' name='target_date1[]'>";
                            
                        

                          var no1=0;
                        _items +="<ul>";
                            
                        $.each(v.branches, function (i,v) {
                          // for(var a=0;a<no1;a++)  {
                            _items +="<li>";
                            _items += "<input type='checkbox' id=node-"+no+"-"+ no1 +" /><label><input type='checkbox' onclick=cek2("+v.id+"); value="+v.id+" id='idsubtask"+v.id+"' name=idsubtask_"+taskid+"[] /><span></span></label><label for=node-"+no+"-"+ no1 +">"+ v.definition +"</label>" ;
                            _items +=" <input   type='date' id=target_date_"+v.id+" min='"+ data[0].startdate +"' max='"+ data[0].targetdate +"' name=target_date_"+taskid+"_"+v.id+">";
                            _items +="</li>";
                           // }
                            no1++;
                        });   
                      //  }
                        _items +="</ul>";
                        no++;
                    _items += "</li>";
                    });
                   _items += "</ul>";
                    //_items += "</li>";
                    //_items += "</ul>";
                    $('#task').html(_items);
                      document.getElementById("gen").style.display = "none";

                
                }

        });    
}else{
  _items="<p style=color:red;><b>Silahkan Pilih Model</b></p>";
  $('#task').html(_items);
  document.getElementById("gen").style.display = "none";  
}


});    


function cek(id){
    document.getElementById("idtask"+id).addEventListener('change', function(){
      
    if(this.checked)
    {
    document.getElementById("target_date1_"+id).required =  true;
    }
     else
     {
     document.getElementById("target_date1_"+id).required =  false;
    }
  })
}

function cek2(id){
    document.getElementById("idsubtask"+id).addEventListener('change', function(){
      
    if(this.checked)
    {
    document.getElementById("target_date_"+id).required =  true;
    }
     else
     {
     document.getElementById("target_date_"+id).required =  false;
    }
  })
}



function generate(id) {
   document.getElementById("gen").style.display = "block";
     $.ajax({
                type: 'GET',
                url: base_url+'/select/model',
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value='0'>Pilih Model...</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.id+"'>"+v.name_model+"</option>";
                    });
                    $('#model_id').html(_items);
                    document.getElementById("gen").style.display = "none";
                }
            });
     $('#id_project').val(id);
     $('#_generate').modal('show');

}

function processGenerate() {
    var _generate_form = $('#_generate_form');
    _generate_form.parsley().validate();
    var _form_data = new FormData(_generate_form[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
    if(_generate_form.parsley().isValid()){

        
       
        //loader.html('<image src="{{ url('/images/loading.gif') }}">');
       // loader.src='/images/loading.gif';
document.getElementById("gen").style.display = "block";
        $.ajax({
        type: 'POST',
        url: base_url + '/projects/insertGenerate',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                $('#_generate').modal('hide');
                //hideLoading(true);
                document.getElementById("gen").style.display = "none";
                //swal("Berhasil!", "Generate Berhasil", "success");
                swal({
                    title: "Berhasil!",
                    text: "Generate Berhasil",
                    icon: "warning",
                    showCancelButton: true,
                    buttons: {
                        confirm: {
                            text: "Tutup",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {
                    if (isConfirm) {
                          var loader = $('.ui.dimmable #mainLoader');    
                            loader.dimmer({closable: false}).dimmer('show');
                             location.reload();
                      } 
                });
               // showInfo(parse.msg);
                _create_form[0].reset();
            }else if(parse.code == 2){
                //hideLoadingPartial(true);
               $('#_generate').modal('hide');
                document.getElementById("gen").style.display = "none";
               // swal("Error!", "Pilih Salah Satu Task", "info");
                swal({
                    title: "Info!",
                    text: "Pilih Salah Satu Task",
                    icon: "info",
                    showCancelButton: true,
                    buttons: {
                        confirm: {
                            text: "Tutup",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {
                    if (isConfirm) {
                          var loader = $('.ui.dimmable #mainLoader');    
                            loader.dimmer({closable: false}).dimmer('show');
                             location.reload();
                      } 
                });
                //showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                //hideLoadingPartial(true);
                showError(li);
            }

        }

    }).always(function () {
     var loader = $('.ui.dimmable #mainLoader');    
    loader.dimmer({closable: false}).dimmer('show');
  //   location.reload();
    });



        /*submitdata('task',_form_data,t_user,_create_modal,_create_form[0])
        .always(function (dataOrjqXHR, textStatus, jqXHRorErrorThrown) { 
            location.reload();

        });*/
        
    } 
    
}


function closeproject(id,tgl) {
    $('#idproject').val(id);
     var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();

                if(dd<10) {
                    dd = '0'+dd
                } 

                if(mm<10) {
                    mm = '0'+mm
                } 

                today = yyyy + '-' + mm + '-' + dd;


                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date(tgl);
                var secondDate = new Date(today);
                

               var wd = workingDaysBetweenDates(secondDate,firstDate);
                var diff1 = secondDate - firstDate;
                var millisecondsPerDay1 = 86400 * 1000;
                var days1 = diff1 / millisecondsPerDay1;
                var startDay1 = secondDate.getDay();
                var endDay1 = firstDate.getDay();
                    
                    // Remove weekend not previously removed.   
                    if (startDay1 - endDay1 > 1)         
                        days1 = days1 - 2;      
                    
                    // Remove start day if span starts on Sunday but ends before Saturday
                    if (startDay1 == 0 && endDay1 != 6)
                        days1 = days1 - 1  
                            
                    // Remove end day if span ends on Saturday but starts after Sunday
                    if (endDay1 == 6 && startDay1 != 0)
                        days1 = days1 - 1  

                console.log(days1);
                console.log(firstDate);
                console.log(secondDate);
                $('#end_date1').val(today);
                $('#duration1').val(days1);
               function workingDaysBetweenDates(startDate, endDate) {
  
                    // Validate input
                    //if (endDate < startDate)
                      //  return 0;
                    
                    // Calculate days between dates
                    var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
                    //startDate.setHours(0,0,0,1);  // Start just after midnight
                   // endDate.setHours(23,59,59,999);  // End just before midnight
                    var diff = endDate - startDate;  // Milliseconds between datetime objects    
                    //var days = Math.ceil(diff / millisecondsPerDay);
                    var days = diff / millisecondsPerDay;
                    // Subtract two weekend days for every week in between
                    //var weeks = Math.floor(days / 7);
                    //days = days - (weeks * 2);

                    // Handle special cases
                    var startDay = startDate.getDay();
                    var endDay = endDate.getDay();
                    
                    // Remove weekend not previously removed.   
                    if (startDay - endDay > 1)         
                        days = days - 2;      
                    
                    // Remove start day if span starts on Sunday but ends before Saturday
                    if (startDay == 0 && endDay != 6)
                        days = days - 1  
                            
                    // Remove end day if span ends on Saturday but starts after Sunday
                    if (endDay == 6 && startDay != 0)
                        days = days - 1  
                    
                    return days;
                }
$('#_close').modal('show');
}
function processClose() {
    var _generate_form = $('#_close_form');
    _generate_form.parsley().validate();
    var _form_data = new FormData(_generate_form[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
    if(_generate_form.parsley().isValid()){

        document.getElementById("close").style.display = "block";
        $.ajax({
        type: 'POST',
        url: base_url + '/projects/insertClose',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                //$('#_close').modal('hide');
                //hideLoading(true);
                $('#_close').modal('hide');
                document.getElementById("close").style.display = "none";
                swal("Berhasil!", "Project Telah Diclose", "success");
                //showInfo(parse.msg);
                _create_form[0].reset();
            }else if(parse.code == 2){
               // hideLoadingPartial(true);
                //showError(parse.msg);
                $('#_close').modal('hide');
                swal("Gagal Close Project!", "Task Belum Done", "error");
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }

        }

    }).always(function () {
    var loader = $('.ui.dimmable #mainLoader');    
    loader.dimmer({closable: false}).dimmer('show');
    location.reload();
    });

    }
}

function openproject(id,url,name) {

    swal({
            title: "Anda Yakin?",
            text: "Open "+ name,
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Tidak",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Ya",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                  open(id,url);
              } else {
                swal("Cancelled", "Open Project Dibatalkan", "error");
            }
        });
}

function open(id,url) {
    url = url + '/' + id;
    $('.modal').each(function () {
        $(this).modal('hide');
    });
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        if (response.rc == 0) {
            // dataTable.ajax.reload();
            //t_user.draw();
            swal("Berhasil!", "Open Project Berhasil", "success");
            var loader = $('.ui.dimmable #mainLoader');    
            loader.dimmer({closable: false}).dimmer('show');
            location.reload();
        } else {
            swal("Gagal", response.rm, "error");
        }
    }).fail(function (response) {
        hideLoading(false);
        swal("Error", "Terjadi Kesalahan", "error");
    });
}

$(".acidjs-css3-treeview").delegate("label input:checkbox", "change", function() {
    var
        checkbox = $(this),
        nestedList = checkbox.parent().next().next(),
        selectNestedListCheckbox = nestedList.find("label:not([for]) input:checkbox");
 
    console.log(nestedList);
    console.log(selectNestedListCheckbox);
    if(checkbox.is(":checked")) {
        return selectNestedListCheckbox.prop("checked", true);
    }
    selectNestedListCheckbox.prop("checked", false);
});

$(".treeview").delegate("label input:checkbox", "change", function() {
    var
        checkbox = $(this),
        nestedList = checkbox.parent().next().next(),
        selectNestedListCheckbox = nestedList.find("label:not([for]) input:checkbox");
 
     console.log(nestedList);
    console.log(selectNestedListCheckbox);
    if(checkbox.is(":checked")) {
        return selectNestedListCheckbox.prop("checked", true);
    }
    selectNestedListCheckbox.prop("checked", false);
});