/**** End Chart Section 5 ***/
var modal_upd=$('#exampleModal4');
var T_home=$('#_home');
function change(id) {
    $.ajax({
        url: 'user/edit/'+id,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {

                $('#id').val(v.id);
                $('#user_name1').val(v.user_name);
              $('#password1').val(v.password);
            });
        }
    });
   $('#exampleModal4').modal('show');
}

function processUpdate() {
    var form = $('#form_edit_lm'),
        formData = new FormData(form[0]);

    form.parsley().validate();
    if (form.parsley().isValid()) {

        submitdata('user/update1',formData,T_home,modal_upd,form[0]);

    }
}

$(document).ready(function () {
   $("#cpasswordnew").keyup(checkPasswordMatch);
   $("#passwordnew").keyup(reset);
   
   function reset() {
    $("#divCheckPasswordMatch").html("");
    }

   function checkPasswordMatch() {
    var password = $("#passwordnew").val();
    var confirmPassword = $("#cpasswordnew").val();

    if (password != confirmPassword)
        $("#divCheckPasswordMatch").html("Passwords do not match!");
     
    else
        $("#divCheckPasswordMatch").html("Passwords match.");

}
});

$.ajax({
    type: 'GET',
    url: 'chart/assigned',
    success: function (res) {
        var data = $.parseJSON(res);
        var jmlassigned;
        $.each(data, function (k,v) {
            
            jmlassigned = v.jml;
        });
        $('#jmlassigned').html(jmlassigned);
        
    }
});
$.ajax({
    type: 'GET',
    url: 'chart/onprogress',
    success: function (res) {
        var data = $.parseJSON(res);
        var jmlonprogress;
        $.each(data, function (k,v) {
            
            jmlonprogress = v.jml;
        });
        $('#jmlonprogress').html(jmlonprogress);
        
    }
});
$.ajax({
    type: 'GET',
    url: 'chart/finish',
    success: function (res) {
        var data = $.parseJSON(res);
        var jmlfinish;
        $.each(data, function (k,v) {
            
            jmlfinish = v.jml;
        });
        $('#jmlfinish').html(jmlfinish);
        
    }
});
$.ajax({
    type: 'GET',
    url: 'chart/jmltask',
    success: function (res) {
        var data = $.parseJSON(res);
        var jmltask;
        $.each(data, function (k,v) {
            
            jmltask = v.jml;
        });
        $('#jmltask').html(jmltask);
        
    }
});

$(window).on("load", function(){

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: 'vendors/js/charts/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],


        // Charts setup
        function (ec) {
            // Initialize chart
            // ------------------------------
            var myChart = ec.init(document.getElementById('basic-pie'));

            // Chart Options
            // ------------------------------
            chartOptions = {

                // Add title
                title: {
                    text: 'Browser popularity',
                    subtext: 'Open source information',
                    x: 'center'
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: ['IE', 'Opera', 'Safari', 'Firefox', 'Chrome']
                },

                // Add custom colors
                color: ['#00A5A8', '#626E82', '#FF7D4D','#FF4558', '#28D094'],

                // Display toolbox
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    feature: {
                        mark: {
                            show: true,
                            title: {
                                mark: 'Markline switch',
                                markUndo: 'Undo markline',
                                markClear: 'Clear markline'
                            }
                        },
                        dataView: {
                            show: true,
                            readOnly: false,
                            title: 'View data',
                            lang: ['View chart data', 'Close', 'Update']
                        },
                        magicType: {
                            show: true,
                            title: {
                                pie: 'Switch to pies',
                                funnel: 'Switch to funnel',
                            },
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    y: '20%',
                                    width: '50%',
                                    height: '70%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore: {
                            show: true,
                            title: 'Restore'
                        },
                        saveAsImage: {
                            show: true,
                            title: 'Same as image',
                            lang: ['Save']
                        }
                    }
                },

                // Enable drag recalculate
                calculable: true,

                // Add series
                series: [{
                    name: 'Browsers',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '57.5%'],
                    data: [
                        {value: 335, name: 'IE'},
                        {value: 310, name: 'Opera'},
                        {value: 234, name: 'Safari'},
                        {value: 135, name: 'Firefox'},
                        {value: 1548, name: 'Chrome'}
                    ]
                }]
            };

            // Apply options
            // ------------------------------

            myChart.setOption(chartOptions);


            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        myChart.resize();
                    }, 200);
                }
            });
        }
    );
});