$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
/**** End Chart Section 5 ***/
$('#team').perfectScrollbar({
        suppressScrollX : true,
        theme: 'dark',
        wheelPropagation: true
    });
$('#workload').perfectScrollbar({
        suppressScrollX : true,
        theme: 'dark',
        wheelPropagation: true
    });
$('#rbbteam').perfectScrollbar({
        suppressScrollX : true,
        theme: 'dark',
        wheelPropagation: true
    });
$('#nrbbteam').perfectScrollbar({
        suppressScrollX : true,
        theme: 'dark',
        wheelPropagation: true
    });

var modal_upd=$('#exampleModal4');
var T_home=$('#_home');
function change(id) {
   $("#wait").css("display", "block");
    $.ajax({
        url: base_url+'/user/edit/'+id,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {

                $('#id').val(v.id);
                $('#user_name1').val(v.user_name);
              $('#password1').val(v.password);
            });

    $("#wait").css("display", "none");
        }
    });
   $('#exampleModal4').modal('show');
}

function processUpdate() {
    var form = $('#form_edit_lm'),
        formData = new FormData(form[0]);

    form.parsley().validate();
    if (form.parsley().isValid()) {

        submitdata(base_url+'/user/update1',formData,T_home,modal_upd,form[0]);

    }
}



$.ajax({
    type: 'GET',
    url: 'chart/jmlfinish',
    success: function (res) {
        var data = $.parseJSON(res);
        var jmlfinish;
        $.each(data, function (k,v) {
            
            jmlfinish = v.jml;
        });
        $('#jmlfinish').html(jmlfinish);
        
    }
});
$.ajax({
    type: 'GET',
    url: 'chart/jmlonprogress',
    success: function (res) {
        var data = $.parseJSON(res);
        var jmlonprogress;
        $.each(data, function (k,v) {
            
            jmlonprogress = v.jml;
        });//
        $('#jmlonprogress').html(jmlonprogress);
        
    }
});
$.ajax({
    type: 'GET',
    url: 'chart/jmlactive',
    success: function (res) {
        var data = $.parseJSON(res);
        var jmlactive;
        $.each(data, function (k,v) {
            
            jmlactive = v.jml;
        });
        $('#jmlactive').html(jmlactive);
        
    }
});

$.ajax({
    type: 'GET',
    url: 'chart/jmlinactive',
    success: function (res) {
        var data = $.parseJSON(res);
        var jmlinactive;
        $.each(data, function (k,v) {

            jmlinactive = v.jml;
        });
        $('#jmlinactive').html(jmlinactive);

    }
});

$.ajax({
    type: 'GET',
    url: 'chart/jmlproject',
    success: function (res) {
        var data = $.parseJSON(res);
        var jmlproject;
        $.each(data, function (k,v) {
            
            jmlproject = v.jml;
        });
        $('#jmlproject').html(jmlproject);
        
    }
});


$.ajax({
    type: 'GET',
    url: 'chart/task',
    success: function (res) {
        var data = $.parseJSON(res);
        var jml = [];
        var definition  = [];
        var done=[];
        var onprogress=[];
        $.each(data, function (k,v) {
           
            done.push(v.done);
            onprogress.push(v.onprogress);
            //definition.push(v.definition);
        });
        console.log(jml);

 
    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: '/vendors/js/charts/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],


        // Charts setup
        function (ec) {
            // Initialize chart
            // ------------------------------
            var myChart = ec.init(document.getElementById('basic-pie'));

            // Chart Options
            // ------------------------------
            chartOptions = {

                // Add title
               

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: ['On Progress','Done']
                },

                // Add custom colors
                color: ['#FF4558', '#28D094','#FF7D4D'],

                // Display toolbox
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    feature: {
                       
                        magicType: {
                            show: true,
                            title: {
                                pie: 'Switch to pies',
                                funnel: 'Switch to funnel',
                            },
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    y: '20%',
                                    width: '50%',
                                    height: '70%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore: {
                            show: true,
                            title: 'Restore'
                        },
                        saveAsImage: {
                            show: true,
                            title: 'Same as image',
                            lang: ['Save']
                        }
                    }
                },

                // Enable drag recalculate
                calculable: true,

                // Add series
                series: [{
                    name: 'Sub Taks',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '57.5%'],
                    data: [

                        {value: onprogress[0], name: 'On Progress'},
                        {value: done[0], name: 'Done'}
                    ]
                }]
            };

            // Apply options
            // ------------------------------

            myChart.setOption(chartOptions);


            // Resize chart
            // ------------------------------

        }
    );


    }
 });


$.fn.raty.defaults.path =base_url+'/images/raty/';
$('#read-only-stars').raty({
        readOnly: true,
        score: 3
    });