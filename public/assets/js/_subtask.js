$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var _create_form = $('#_create_form');
var _create_form1 = $('#_create_form1');
var _create_modal = $('#_create');
var _create_modal1 = $('#_create1');
var _change_modal = $('#_change');
var modal_upd= $('#_edit');
var t_user = $('#t_user');
/*
var t_user = $('#t_user').DataTable({
    processing: true,
    serverSide: true,
    scrollX: true,
    ajax: {
        url: 'task/table'
    },
   
    columns: [
        {data: 'nomor_urut', name: 'nomor_urut', id:'nomor_urut'},
        {data: 'id_project', name: 'id_project'},
        {data: 'task_name', name: 'task_name'},
        {data: 'progress_id', name: 'progress_id'},
        {data: 'progress_pct', name: 'progress_pct'},
        {data: 'pic_id', name: 'pic_id'},
        {data: 'url_doc', name: 'url_doc'},
        {data: 'action', name: 'action', sortable: false, searchable: false}
    ]
    
});
*/


function tambah(id,id1,idproject) {
    $('#id_task').val(id);
    $('#id_task1').val(id1);
    console.log(id);
    console.log(id1);
var _items = "";
   document.getElementById("subtask").style.display = "block";
     $.ajax({
        type: 'GET',
        url: base_url+'/select/userteam/'+ idproject,
        success: function (res) {
            var data = $.parseJSON(res);
            _items = "<option value=''>Pilih PIC</option>";
            $.each(data, function (k,v) {
                _items += "<option value='"+v.id+"'>"+v.full_name+"</option>";
            });

            $('#pic_id').html(_items);
        }
    });
    $.ajax({
        type: 'GET',
        url: base_url+'/select/projecttype',
        success: function (res) {
            var data = $.parseJSON(res);
            _items = "<option value=''>Pilih Task Type</option>";
            $.each(data, function (k,v) {
                _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
            });

            $('#tasktype_id').html(_items);
        }
    });
    $.ajax({
        type: 'GET',
        url: base_url+'/select/tasklevel',
        success: function (res) {
            var data = $.parseJSON(res);
            _items = "<option value=''>Pilih Task Level</option>";
            $.each(data, function (k,v) {
                _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
            });

            $('#tasklevel_id').html(_items);
        }
    }); 
 
   
     $.ajax({
        type: 'GET',
        url: base_url+'/select/RefSubtask/' + id1,
        success: function (res) {
            var data = $.parseJSON(res);
            _items = "<option value=''>Pilih Sub Task</option>";
            _items += "<option value='add'><b>Tambah Sub Task. . .</b></option>";
            $.each(data, function (k,v) {
                _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
            });

            $('#id_sub_task').html(_items);
        }
    });  

     $.ajax({
        url: base_url+'/select/projectid/'+idproject,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {

                 var mulai = document.getElementById("start_date");
                    mulai.setAttribute("min", v.start_date);
                    mulai.setAttribute("max", v.target_date);

                 var target = document.getElementById("target_date");
                    target.setAttribute("min", v.start_date);
                    target.setAttribute("max", v.target_date);    
           
            });    
        document.getElementById("subtask").style.display = "none";    
        }
    });


    _create_modal.modal('show');
}

$('select[name=id_sub_task]').change(function() {
    
    if ($(this).val() == 'add')
    {
       
        //alert($('#id_task').val());
    
        $('#definition1').val($('#id_task1').val());
         _create_modal1.modal('show');
         _create_modal.modal('hide');
    }
});
$('#id_sub_task').on('change', function (v) {
  $('#sub_task_name').val($(this).find('option:selected').text()); 
});

function processInsert1() {
     _create_form1.parsley().validate();
    var _form_data = new FormData(_create_form1[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
    if(_create_form1.parsley().isValid()){
        submitdata(base_url+'/refSubtask',_form_data,_create_modal,_create_modal1,_create_form1[0]);
    }
    
}

function processInsert() {
     _create_form.parsley().validate();
    var _form_data = new FormData(_create_form[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
    if(_create_form.parsley().isValid()){
          document.getElementById("subtask").style.display = "block";   
             $.ajax({
        type: 'POST',
        url: base_url+'/subtask',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                _create_modal.modal('hide');
               // hideLoading(true);
                  
                showInfo(parse.msg);
                _create_form[0].reset();
                var loader = $('.ui.dimmable #mainLoader');    
                               loader.dimmer({closable: false}).dimmer('show');
                                     location.reload();
            }else if(parse.code == 2){
                //hideLoadingPartial(true);
                //showError(parse.msg);
                _create_modal.modal('hide');
                _create_form[0].reset();
                // swal("Info!", "Sub Task Untuk "+ parse.msg +" Sudah Ada", "info");

                swal({
                        title: "Info!",
                        text: "Sub Task Untuk "+ parse.msg +" Sudah Ada",
                        icon: "info",
                        showCancelButton: true,
                        buttons: {
                            
                            confirm: {
                                text: "Tutup",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: false
                            }
                        }
                    }).then(isConfirm => {
                        if (isConfirm) {
                              //del(id,url);
                               var loader = $('.ui.dimmable #mainLoader');    
                               loader.dimmer({closable: false}).dimmer('show');
                                     location.reload();
                          } 
                    });

            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
               document.getElementById("subtask").style.display = "none";
        }
    }).always(function () {
     //var loader = $('.ui.dimmable #mainLoader');    
   //loader.dimmer({closable: false}).dimmer('show');
        // location.reload();
    });
     //   submitdata(base_url+'/subtask',_form_data,t_user,_create_modal,_create_form[0]);
    }
}

function editshow(id,parallel) {
    var _items = "";
     

     $.ajax({
        type: 'GET',
        url: base_url+'/select/status1',
        success: function (res) {
            var data = $.parseJSON(res);
            //_items = "<option value=''>Pilih Progress Task</option>";
            $.each(data, function (k,v) {
                _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
            });

            $('#status_id1').html(_items);
        }
    });  
    
    $.ajax({
        url: base_url+'/subtask/edit/'+id,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {

                $('#id').val(v.id);
                $('#id_task1').val(v.id_task);
                $('#idtask').val(v.id_task);
                $('#sub_task_name1').val(v.sub_task_name);
                $('#notes1').val(v.notes);
              //  $('#url_doc1').val(v.url_doc);
                $('#progressstatus_id1').val(v.progress_id);
                //$('#progressstatus_id1 option[value="' + v.progress_id +'"]').attr('selected', 'selected').text();
                

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();

                if(dd<10) {
                    dd = '0'+dd
                } 

                if(mm<10) {
                    mm = '0'+mm
                } 

                today = yyyy + '-' + mm + '-' + dd;


                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date(v.target_date);
                var secondDate = new Date(today);
                

               var wd = workingDaysBetweenDates(firstDate,secondDate);
             
                $('#end_date1').val(today);
                $('#duration1').val(wd);
               function workingDaysBetweenDates(startDate, endDate) {
  
                    // Validate input
                    //if (endDate < startDate)
                      //  return 0;
                    
                    // Calculate days between dates
                    var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
                    startDate.setHours(0,0,0,1);  // Start just after midnight
                    endDate.setHours(23,59,59,999);  // End just before midnight
                    var diff = endDate - startDate;  // Milliseconds between datetime objects    
                    var days = Math.ceil(diff / millisecondsPerDay);
                    
                    // Subtract two weekend days for every week in between
                    var weeks = Math.floor(days / 7);
                    days = days - (weeks * 2);

                    // Handle special cases
                    var startDay = startDate.getDay();
                    var endDay = endDate.getDay();
                    
                    // Remove weekend not previously removed.   
                    if (startDay - endDay > 1)         
                        days = days - 2;      
                    
                    // Remove start day if span starts on Sunday but ends before Saturday
                    if (startDay == 0 && endDay != 6)
                        days = days - 1  
                            
                    // Remove end day if span ends on Saturday but starts after Sunday
                    if (endDay == 6 && startDay != 0)
                        days = days - 1  
                    
                    return days;
                }

            });    
        }
    });
    var loader = $('.ui.dimmable #mainLoader');    
    loader.dimmer({closable: false}).dimmer('show');

    if(parallel==1){

     $.ajax({

            url: base_url+'/subtask/seq/'+id,
            type: 'get',
             success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {
               
               console.log(v.progress_id);
               console.log(v.is_approve);

               if(v.progress_id!=5){

                loader.dimmer({closable: false}).dimmer('hide');
                alert("Sub Task Sebelumnya Belum Done");


               }else if(v.is_approve!=true){
                loader.dimmer({closable: false}).dimmer('hide');
                alert("Sub Task Sebelumnya Belum Done");

               }else{

            loader.dimmer({closable: false}).dimmer('hide');
            modal_upd.modal('show');

               }

            });
            if(data.length==0){


             loader.dimmer({closable: false}).dimmer('hide');
            modal_upd.modal('show');
   
            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
 }else{
    loader.dimmer({closable: false}).dimmer('hide');
    modal_upd.modal('show');
    
 }



    

}

function processUpdate() {
    var form = $('#form_edit_lm'),
        formData = new FormData(form[0]);

    form.parsley().validate();
     //alert('ab');
    if (form.parsley().isValid()) {
       document.getElementById("subtasku").style.display = "block";
       $.ajax({
        type: 'POST',
        url: base_url+'/subtask/update',
        data: formData,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                modal_upd.modal('hide');
                //hideLoading(true);
                document.getElementById("subtasku").style.display = "none";
                showInfo(parse.msg);
                form[0].reset();
            }else if(parse.code == 2){
                hideLoadingPartial(true);
                showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    }).always(function () {
        var loader = $('.ui.dimmable #mainLoader');    
    loader.dimmer({closable: false}).dimmer('show');
     location.reload();
    });
        //submitdata(base_url+'/subtask/update',formData,t_user,modal_upd,form[0]);
    }
}

function hapus(id,url) {

    swal({
            title: "Anda Yakin?",
            text: "Data Akan Dihapus!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                  del(id,url);
              } else {
                swal("Cancelled", "Your imaginary file is safe", "error");
            }
        });
}

function del(id,url) {
    url = url + '/' + id;
    $('.modal').each(function () {
        $(this).modal('hide');
    });
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        if (response.rc == 0) {
            // dataTable.ajax.reload();

           // t_user.draw();
         
            swal("Berhasil!", "Data Sudah Dihapus", "success");
              var loader = $('.ui.dimmable #mainLoader');    
        loader.dimmer({closable: false}).dimmer('show');
            location.reload();
        } else {
            swal("Gagal", response.rm, "error");
        }
    }).fail(function (response) {
        hideLoading(false);
        swal("Error", "Terjadi Kesalahan", "error");
    });
}



function filter() {
    t_user.draw();
}
$('#target_date').on('change', function (v) {

var tglawal = this.value;
//var awal = str.split('/',3);
//hasil variable awal akan kyk begini : 14,04,2010

var tglakhir = $('#start_date').val();
//var selesai = str2.split('/',3);
//hasil variable selesai akan kyk begini : 13,04,2010

//udah gitu sambung lagi bagian2 yang udah di split tadi ke format yyyy-mm-dd,
//var mulai = awal[2] + '-' +awal[1] + '-' + awal[0];
//var akhir = selesai[2] + '-' +selesai[1] + '-' + selesai[0];

//udah gitu rubah ke bentuk date() sekaligus bandingin,
//console.log(str);
//console.log(str2);
if( tglakhir > tglawal) {
alert('Tanggal target harus lebih besar dari tanggal mulai.');
$('#target_date').val(tglakhir);
}else if(!tglakhir){
alert('Tanggal Mulai Masih Kosong');
$('#start_date').val(tglakhir);
}

}); 

$('#start_date').on('change', function (v) {

var tglawal =$('#target_date').val();
//var awal = str.split('/',3);
//hasil variable awal akan kyk begini : 14,04,2010

var tglakhir =  this.value;
//var selesai = str2.split('/',3);
//hasil variable selesai akan kyk begini : 13,04,2010

//udah gitu sambung lagi bagian2 yang udah di split tadi ke format yyyy-mm-dd,
//var mulai = awal[2] + '-' +awal[1] + '-' + awal[0];
//var akhir = selesai[2] + '-' +selesai[1] + '-' + selesai[0];

//udah gitu rubah ke bentuk date() sekaligus bandingin,
//console.log(str);
//console.log(str2);
if(!tglawal){
$('#target_date').val(tglakhir);
}else if(tglakhir > tglawal){
alert('Tanggal mulai harus lebih kecil dari tanggal target.');
$('#target_date').val(tglakhir);    
}

});    
