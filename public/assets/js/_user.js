$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var _create_form = $('#_create_form');
var _create_modal = $('#_create');
var _change_modal = $('#_change');
var modal_upd= $('#_edit');

var t_user = $('#t_user').DataTable({
    processing: true,
    serverSide: true,
    scrollX: true,
    ajax: {
        url: 'user/table'
    },
    columns: [
        {data: 'nomor_urut', name: 'nomor_urut'},
        {data: 'full_name', name: 'full_name'},
        {data: 'user_name', name: 'user_name'},
        {data: 'team_id', name: 'team_id'},
        {data: 'name', name: 'name'},
        {data: 'action', name: 'action', sortable: false, searchable: false}
    ]
});

function tambah() {
 var _items = "";
  $.ajax({
        type: 'GET',
        url: 'select/team',
        success: function (res) {
            var data = $.parseJSON(res);
            _items = "<option value=''>Pilih Team</option>";
            $.each(data, function (k,v) {
                _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
            });

            $('#team_id').html(_items);
        }
    }); 
    _create_modal.modal('show');
}


function processInsert() {
     _create_form.parsley().validate();
    var _form_data = new FormData(_create_form[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
    if(_create_form.parsley().isValid()){
        //document.getElementById("us").style.display = "block";
        //document.getElementById("us").style.display = "none";
        submitdatachange('user',_form_data,t_user,_create_modal,_create_form[0]);
        
    }
    
}

function editshow(id) {

var _items = "";
  $.ajax({
        type: 'GET',
        url: 'select/team',
        success: function (res) {
            var data = $.parseJSON(res);
            _items = "<option value=''>Pilih Team</option>";
            $.each(data, function (k,v) {
                _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
            });

            $('#team_id1').html(_items);
        }
    }); 

$.ajax({
        url: 'user/edit/'+id,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {

                $('#id').val(v.id);
                $('#full_name1').val(v.full_name);
                $('#user_name1').val(v.user_name);
                //$('#team_id1').val(v.team_id);
                $('#team_id1 option[value="' + v.team_id +'"]').attr('selected', 'selected').text();
               
      
            });
        }
    });
modal_upd.modal('show');
}

function processUpdate() {
    var form = $('#form_edit_lm'),
        formData = new FormData(form[0]);

    form.parsley().validate();
     //alert('ab');
   // if (form.parsley().isValid()) {
      //  alert('a'); 
        submitdata('user/update',formData,t_user,modal_upd,form[0]);
    //}
}

function hapus(id,url) {

    swal({
            title: "Anda Yakin?",
            text: "Data Akan Dihapus!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                  del(id,url);
              } else {
                swal("Cancelled", "Your imaginary file is safe", "error");
            }
        });
}

function del(id,url) {
    url = url + '/' + id;
    $('.modal').each(function () {
        $(this).modal('hide');
    });
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        if (response.rc == 0) {
            // dataTable.ajax.reload();
            t_user.draw();
            swal("Berhasil!", "Data Sudah Dihapus", "success");
        } else {
            swal("Gagal", response.rm, "error");
        }
    }).fail(function (response) {
        hideLoading(false);
        swal("Error", "Terjadi Kesalahan", "error");
    });
}

function resetPassword(id) {

     swal({
            title: "Anda Yakin?",
            text: "Reset Password",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Reset",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                   $.ajax({
                        url: 'user/reset/'+id,
                        type: 'GET',
                        success: function (res) {
                            var data = $.parseJSON(res);

                            if(data.code == 1){
                                swal("Berhasil!", "Password Sudah direset", "success");
                            }else{
                                swal("Gagal", data.msg, "error");
                            }
                        }
                    });
              } else {
                swal("Cancelled", "Your imaginary file is safe", "error");
            }
        });
}

function filter() {
    t_user.draw();
}
